import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'model/movies_model.dart';
import 'model/people_model.dart';
import 'model/tv_shows_model.dart';
import 'strings.dart';
import 'colors.dart';
import 'main_screen.dart';

void main() {
  final moviesModel = MoviesModel();
  final peopleModel = PeopleModel();
  final tvShowModel = TvShowsModel();

  runApp(ScopedModel<MoviesModel>(
    model: moviesModel,
    child: ScopedModel<PeopleModel>(
        model: peopleModel,
        child: ScopedModel<TvShowsModel>(
          model: tvShowModel,
          child: MyApp(),
        )),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: Strings.title,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: AppColors.primaryColor, fontFamily: "OpenSans"),
        home: MainScreen());
  }
}
