class Strings {
  static const String title = "Filmographer";

  //Navigation
  static const String filterSettings = "Настройки фильтра";
  static const String searchResults = "Результаты поиска";
  static const String favourites = "Избранное";
  static const String wantToWatch = "Хочу посмотреть";
  static const String settings = "Настройки";
  static const String search = "Поиск";
  static const String movies = "Фильмы";
  static const String tvShows = "Сериалы";
  static const String people = "Люди";

  //Movies
  static const String searchMovies = "Поиск фильмов";
  static const String discoverMovies = "Подбор фильмов";
  static const String aboutMovie = "О фильме";
  static const String findMovies = "НАЙТИ ФИЛЬМЫ";
  static const String trailers = "Трейлеры";
  static const String recommendations = "Рекомендации";

  //TvShows
  static const String searchTvShows = "Поиск сериалов";
  static const String discoverTvShows = "Подбор сериалов";
  static const String aboutTvShow = "О сериале";
  static const String aboutSeason = "О сезоне";
  static const String aboutEpisode = "О серии";
  static const String guestStars = "В серии снимались";
  static const String seasons = "Сезоны";
  static const String season = "сезон";
  static const String episodes = "Серии";
  static const String episode = "серия";
  static const String stills = "Кадры";
  static const String airDate = "Дата выхода";
  static const String findTvShows = "НАЙТИ СЕРИАЛЫ";
  static const String inProduction = "Снимается";
  static const String finished = "Завершен";

  //People
  static const String searchPeople = "Поиск актеров";
  static const String cast = "В ролях";
  static const String director = "Режиссер";
  static const String writer = "Сценарист";
  static const String producer = "Продюсер";
  static const String actor = "Актер";
  static const String actress = "Актриса";
  static const String biography = "Биография";
  static const String knownForMoviesMale = "Снимался в фильмах";
  static const String knownForMoviesFemale = "Снималась в фильмах";
  static const String knownForMoviesNeutral = "Снимался(-ась) в фильмах";
  static const String knownForTvShowsMale = "Снимался в сериалах";
  static const String knownForTvShowsFemale = "Снималась в сериалах";
  static const String knownForTvShowsNeutral = "Снимался(-ась) в сериалах";
  static const String deletedMale = "удален";
  static const String deletedFemale = "удалена";
  static const String photo = "Фото";

  //UI
  static const String genre = "Жанр:";
  static const String details = "Подробнее";
  static const String detailsCaps = "ПОДРОБНЕЕ";
  static const String cancelCaps = "ОТМЕНА";
  static const String popular = "Популярные";
  static const String addFavourites = "В избранное";
  static const String addFavouritesCaps = "В ИЗБРАННОЕ";
  static const String inFavourites = "В избранном";
  static const String links = "Ссылки";
  static const String moreOverview = "Развернуть";
  static const String lessOverview = "Свернуть";
  static const String photoUI = " фото ";
  static const String unknown = "Неизвестно";
  static const String loading = "Загрузка...";
  static const String retry = "ПОВТОРИТЬ";

  //Messages
  static const String noElements = "Здесь пока ничего нет";
  static const String nothingFound = "К сожалению, ничего не нашлось";
  static const String errorMessage = "Возникла ошибка";
  static const String checkConnection = "Проверьте ваше Интернет-соединение";
  static const String tryLater = "Попробуйте повторить запрос позже";
}