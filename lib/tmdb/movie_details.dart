import 'package:filmographer/tmdb/external_ids.dart';
import 'package:json_annotation/json_annotation.dart';
import 'genre.dart';
import 'discover_results.dart';
import 'image_results.dart';
import 'credits.dart';
import 'video_results.dart';

part 'movie_details.g.dart';

@JsonSerializable()
class MovieDetails {
  MovieDetails({
    this.id,
    this.credits,
    this.runtime,
    this.tagline,
    this.homepage,
    this.images,
    this.imdbId,
    this.genres,
    this.recommendations,
    this.videos,
    this.externalIds,
  });

  final List<Genre> genres;
  final String homepage;
  final int id;

  @JsonKey(name: 'imdb_id')
  final String imdbId;

  final int runtime;
  final String tagline;
  final ImageResults images;
  final Credits credits;
  final DiscoverResults recommendations;
  final VideoResults videos;
  @JsonKey(name: 'external_ids')
  final ExternalIds externalIds;

  factory MovieDetails.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$MovieDetailsToJson(this);
}
