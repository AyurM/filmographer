// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoResults _$VideoResultsFromJson(Map<String, dynamic> json) {
  return VideoResults((json['results'] as List)
      ?.map((e) => e == null ? null : Video.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

Map<String, dynamic> _$VideoResultsToJson(VideoResults instance) =>
    <String, dynamic>{'results': instance.results};
