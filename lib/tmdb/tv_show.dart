import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/string_utils.dart';
import 'tmdb_video.dart';

part 'tv_show.g.dart';

@JsonSerializable()
class TvShow extends TmdbVideo {
  TvShow(
      {int voteCount,
      int id,
      double voteAverage,
      this.name,
      double popularity,
      String posterPath,
      String originalLanguage,
      this.originalName,
      List<int> genreIds,
      String backdropPath,
      String overview,
      this.firstAirDate})
      : super(voteCount, id, voteAverage, popularity, posterPath,
            originalLanguage, backdropPath, overview, genreIds);

  final String name;

  @JsonKey(name: 'first_air_date')
  final String firstAirDate;

  @JsonKey(name: 'original_name')
  final String originalName;

  String get title => name;
  String get releaseDate => firstAirDate;
  String get originalTitle => originalName;

  factory TvShow.fromJson(Map<String, dynamic> json) => _$TvShowFromJson(json);

  Map<String, dynamic> toJson() => _$TvShowToJson(this);

  //для БД SQLite
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'voteCount': voteCount,
      'voteAverage': voteAverage,
      'name': name,
      'popularity': popularity,
      'posterPath': posterPath,
      'originalLanguage': originalLanguage,
      'originalName': originalName,
      'genreIds': StringUtils.getGenreIdsString(genreIds),
      'backdropPath': backdropPath,
      'overview': overview,
      'firstAirDate': firstAirDate
    };
  }

  //для БД SQLite
  factory TvShow.fromMap(Map<String, dynamic> dbMap) => new TvShow(
      voteCount: dbMap['voteCount'] as int,
      id: dbMap['id'] as int,
      voteAverage: (dbMap['voteAverage'] as num)?.toDouble(),
      name: dbMap['name'] as String,
      popularity: (dbMap['popularity'] as num)?.toDouble(),
      posterPath: dbMap['posterPath'] as String,
      originalLanguage: dbMap['originalLanguage'] as String,
      originalName: dbMap['originalName'] as String,
      genreIds: (dbMap['genreIds'] as String)
          .split(',')
          .map((x) => int.parse(x))
          .toList(),
      backdropPath: dbMap['backdropPath'] as String,
      overview: dbMap['overview'] as String,
      firstAirDate: dbMap['firstAirDate'] as String);

  @override
  bool operator ==(o) => o is TvShow && o.id == id;

  @override
  int get hashCode => id.hashCode;
}
