// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_episode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TvEpisode _$TvEpisodeFromJson(Map<String, dynamic> json) {
  return TvEpisode(
      json['id'] as int,
      json['season_number'] as int,
      json['air_date'] as String,
      json['name'] as String,
      json['overview'] as String,
      json['episode_number'] as int,
      json['vote_count'] as int,
      json['still_path'] as String,
      (json['vote_average'] as num)?.toDouble(),
      (json['guest_stars'] as List)
          ?.map((e) =>
              e == null ? null : Cast.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TvEpisodeToJson(TvEpisode instance) => <String, dynamic>{
      'id': instance.id,
      'episode_number': instance.episodeNumber,
      'season_number': instance.seasonNumber,
      'vote_count': instance.voteCount,
      'air_date': instance.airDate,
      'name': instance.name,
      'overview': instance.overview,
      'still_path': instance.stillPath,
      'vote_average': instance.voteAverage,
      'guest_stars': instance.guestStars
    };
