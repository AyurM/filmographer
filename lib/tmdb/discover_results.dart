import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/tmdb/movie.dart';

part 'discover_results.g.dart';

@JsonSerializable()
class DiscoverResults {
  DiscoverResults(
      {this.page, this.totalResults, this.totalPages, this.results});

  final int page;
  @JsonKey(name: 'total_results')
  final int totalResults;
  @JsonKey(name: 'total_pages')
  final int totalPages;
  final List<Movie> results;

  factory DiscoverResults.fromJson(Map<String, dynamic> json) =>
      _$DiscoverResultsFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverResultsToJson(this);
}
