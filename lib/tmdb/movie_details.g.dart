// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDetails _$MovieDetailsFromJson(Map<String, dynamic> json) {
  return MovieDetails(
      id: json['id'] as int,
      credits: json['credits'] == null
          ? null
          : Credits.fromJson(json['credits'] as Map<String, dynamic>),
      runtime: json['runtime'] as int,
      tagline: json['tagline'] as String,
      homepage: json['homepage'] as String,
      images: json['images'] == null
          ? null
          : ImageResults.fromJson(json['images'] as Map<String, dynamic>),
      imdbId: json['imdb_id'] as String,
      genres: (json['genres'] as List)
          ?.map((e) =>
              e == null ? null : Genre.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      recommendations: json['recommendations'] == null
          ? null
          : DiscoverResults.fromJson(
              json['recommendations'] as Map<String, dynamic>),
      videos: json['videos'] == null
          ? null
          : VideoResults.fromJson(json['videos'] as Map<String, dynamic>),
      externalIds: json['external_ids'] == null
          ? null
          : ExternalIds.fromJson(json['external_ids'] as Map<String, dynamic>));
}

Map<String, dynamic> _$MovieDetailsToJson(MovieDetails instance) =>
    <String, dynamic>{
      'genres': instance.genres,
      'homepage': instance.homepage,
      'id': instance.id,
      'imdb_id': instance.imdbId,
      'runtime': instance.runtime,
      'tagline': instance.tagline,
      'images': instance.images,
      'credits': instance.credits,
      'recommendations': instance.recommendations,
      'videos': instance.videos,
      'external_ids': instance.externalIds
    };
