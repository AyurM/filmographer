import 'package:json_annotation/json_annotation.dart';
import 'tmdb_parameters.dart';

part 'backdrop.g.dart';

@JsonSerializable()
class Backdrop{
  Backdrop(
      this.aspectRatio,
      this.filePath,
      this.height,
      this.width
      );

  @JsonKey(name: 'aspect_ratio')
  final double aspectRatio;

  @JsonKey(name: 'file_path')
  final String filePath;

  final int height;
  final int width;

  String getBackdropPath(){
    return TmdbParams.highResPosterBaseUrl + filePath;
  }

  factory Backdrop.fromJson(Map<String, dynamic> json) => _$BackdropFromJson(json);

  Map<String, dynamic> toJson() => _$BackdropToJson(this);
}