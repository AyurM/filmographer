// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'combined_credits_cast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CombinedCreditsCast _$CombinedCreditsCastFromJson(Map<String, dynamic> json) {
  return CombinedCreditsCast(
      json['character'] as String,
      json['id'] as int,
      json['name'] as String,
      json['overview'] as String,
      (json['popularity'] as num)?.toDouble(),
      json['media_type'] as String,
      json['vote_count'] as int,
      (json['vote_average'] as num)?.toDouble(),
      json['poster_path'] as String,
      (json['genre_ids'] as List)?.map((e) => e as int)?.toList(),
      json['title'] as String,
      json['original_title'] as String,
      json['release_date'] as String,
      json['episode_count'] as int,
      json['original_name'] as String,
      json['first_air_date'] as String,
      json['original_language'] as String,
      json['backdrop_path'] as String);
}

Map<String, dynamic> _$CombinedCreditsCastToJson(
        CombinedCreditsCast instance) =>
    <String, dynamic>{
      'character': instance.character,
      'id': instance.id,
      'overview': instance.overview,
      'popularity': instance.popularity,
      'media_type': instance.mediaType,
      'vote_count': instance.voteCount,
      'vote_average': instance.voteAverage,
      'poster_path': instance.posterPath,
      'backdrop_path': instance.backdropPath,
      'genre_ids': instance.genreIds,
      'original_language': instance.originalLanguage,
      'title': instance.title,
      'original_title': instance.originalTitle,
      'release_date': instance.releaseDate,
      'name': instance.name,
      'episode_count': instance.episodeCount,
      'original_name': instance.originalName,
      'first_air_date': instance.firstAirDate
    };
