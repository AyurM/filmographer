// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_show.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TvShow _$TvShowFromJson(Map<String, dynamic> json) {
  return TvShow(
      voteCount: json['vote_count'] as int,
      id: json['id'] as int,
      voteAverage: (json['vote_average'] as num)?.toDouble(),
      name: json['name'] as String,
      popularity: (json['popularity'] as num)?.toDouble(),
      posterPath: json['poster_path'] as String,
      originalLanguage: json['original_language'] as String,
      originalName: json['original_name'] as String,
      genreIds: (json['genre_ids'] as List)?.map((e) => e as int)?.toList(),
      backdropPath: json['backdrop_path'] as String,
      overview: json['overview'] as String,
      firstAirDate: json['first_air_date'] as String);
}

Map<String, dynamic> _$TvShowToJson(TvShow instance) => <String, dynamic>{
      'vote_count': instance.voteCount,
      'id': instance.id,
      'vote_average': instance.voteAverage,
      'popularity': instance.popularity,
      'poster_path': instance.posterPath,
      'original_language': instance.originalLanguage,
      'backdrop_path': instance.backdropPath,
      'overview': instance.overview,
      'genre_ids': instance.genreIds,
      'name': instance.name,
      'first_air_date': instance.firstAirDate,
      'original_name': instance.originalName
    };
