import 'package:json_annotation/json_annotation.dart';
import 'combined_credits_results.dart';
import 'image_cast_results.dart';
import 'person_summary.dart';
import 'external_ids.dart';

part 'cast_details.g.dart';

@JsonSerializable()
class CastDetails {
  CastDetails(
      this.id,
      this.birthday,
      this.deathday,
      this.name,
      this.gender,
      this.biography,
      this.popularity,
      this.placeOfBirth,
      this.imdbId,
      this.profilePath,
      this.homepage,
      this.combinedCredits,
      this.images,
      this.knownForDepartment,
      this.adult,
      this.externalIds);

  final int id;
  final int gender;
  final double popularity;
  final bool adult;
  final String birthday;
  final String deathday;
  final String name;
  final String homepage;
  final String biography;

  @JsonKey(name: 'known_for_department')
  final String knownForDepartment;

  @JsonKey(name: 'place_of_birth')
  final String placeOfBirth;

  @JsonKey(name: 'imdb_id')
  final String imdbId;

  @JsonKey(name: 'profile_path')
  final String profilePath;

  @JsonKey(name: 'combined_credits')
  final CombinedCreditsResults combinedCredits;

  @JsonKey(name: 'external_ids')
  final ExternalIds externalIds;

  final ImageCastResults images;

  factory CastDetails.fromJson(Map<String, dynamic> json) =>
      _$CastDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$CastDetailsToJson(this);

  PersonSummary getSummary() {
    return PersonSummary(id, gender, name, knownForDepartment, profilePath,
        _getTotalMovies(), _getTotalTvShows(), images.profiles.length);
  }

  int _getTotalMovies() {
    if (knownForDepartment == "Acting")
      return combinedCredits.cast.where((c) => c.isMovie()).length;
    else if (knownForDepartment == "Directing" ||
        knownForDepartment == "Writing")
      return combinedCredits.crew.where((c) => c.isMovie()).length;
    else
      return 0;
  }

  int _getTotalTvShows() {
    if (knownForDepartment == "Acting")
      return combinedCredits.cast.where((c) => !c.isMovie()).length;
    else if (knownForDepartment == "Directing" ||
        knownForDepartment == "Writing")
      return combinedCredits.crew.where((c) => !c.isMovie()).length;
    else
      return 0;
  }
}
