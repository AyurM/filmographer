import 'cast.dart';
import 'crew.dart';

class PersonSummary {
  final int id;
  final int gender;
  final String name;
  final String knownForDepartment;
  final String profilePath;
  final int totalPhotos;
  int totalMovies;
  int totalTvShows;

  PersonSummary(this.id, this.gender, this.name, this.knownForDepartment,
      this.profilePath, this.totalMovies, this.totalTvShows, this.totalPhotos);

  Cast getCast(){
    return Cast(id, gender, name, profilePath, 0);
  }

  Crew getCrew(){
    return Crew(knownForDepartment, id, gender, "", name, profilePath);
  }

  bool isCast(){
    return knownForDepartment == "Acting";
  }

  //для БД SQLite
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'gender': gender,
      'name': name,
      'knownForDepartment': knownForDepartment,
      'profilePath': profilePath,
      'totalMovies': totalMovies,
      'totalTvShows': totalTvShows,
      'totalPhotos': totalPhotos
    };
  }

  //для БД SQLite
  factory PersonSummary.fromMap(Map<String, dynamic> dbMap) =>
      new PersonSummary(
          dbMap['id'] as int,
          dbMap['gender'] as int,
          dbMap['name'] as String,
          dbMap['knownForDepartment'] as String,
          dbMap['profilePath'] as String,
          dbMap['totalMovies'] as int,
          dbMap['totalTvShows'] as int,
          dbMap['totalPhotos'] as int);

  @override
  bool operator == (o) => o is PersonSummary && o.id == id;

  @override
  int get hashCode => id.hashCode;
}
