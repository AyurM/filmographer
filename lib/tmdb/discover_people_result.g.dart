// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'discover_people_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DiscoverPeopleResults _$DiscoverPeopleResultsFromJson(
    Map<String, dynamic> json) {
  return DiscoverPeopleResults(
      page: json['page'] as int,
      totalResults: json['total_results'] as int,
      totalPages: json['total_pages'] as int,
      results: (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : PersonResult.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$DiscoverPeopleResultsToJson(
        DiscoverPeopleResults instance) =>
    <String, dynamic>{
      'page': instance.page,
      'total_results': instance.totalResults,
      'total_pages': instance.totalPages,
      'results': instance.results
    };
