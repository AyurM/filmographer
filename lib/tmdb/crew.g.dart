// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crew.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Crew _$CrewFromJson(Map<String, dynamic> json) {
  return Crew(
      json['department'] as String,
      json['id'] as int,
      json['gender'] as int,
      json['job'] as String,
      json['name'] as String,
      json['profile_path'] as String);
}

Map<String, dynamic> _$CrewToJson(Crew instance) => <String, dynamic>{
      'id': instance.id,
      'gender': instance.gender,
      'name': instance.name,
      'profile_path': instance.profilePath,
      'department': instance.department,
      'job': instance.job
    };
