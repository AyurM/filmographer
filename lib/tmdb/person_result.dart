import 'package:json_annotation/json_annotation.dart';
import 'combined_credits_cast.dart';
import 'cast.dart';

part 'person_result.g.dart';

@JsonSerializable()
class PersonResult {
  PersonResult(
    this.id,
    this.name,
    this.profilePath,
    this.popularity,
    this.knownFor,
    this.adult
  );

  final int id;
  final double popularity;
  final bool adult;
  final String name;
  @JsonKey(name: 'profile_path')
  final String profilePath;
  @JsonKey(name: 'known_for')
  final List<CombinedCreditsCast> knownFor;

  Cast getCast() {
    return Cast(id, 0, name, profilePath, 0);
  }

  factory PersonResult.fromJson(Map<String, dynamic> json) =>
      _$PersonResultFromJson(json);

  Map<String, dynamic> toJson() => _$PersonResultToJson(this);
}
