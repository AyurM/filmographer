// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_season.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TvSeason _$TvSeasonFromJson(Map<String, dynamic> json) {
  return TvSeason(
      json['id'] as int,
      json['episode_count'] as int,
      json['season_number'] as int,
      json['air_date'] as String,
      json['name'] as String,
      json['overview'] as String,
      json['poster_path'] as String);
}

Map<String, dynamic> _$TvSeasonToJson(TvSeason instance) => <String, dynamic>{
      'id': instance.id,
      'episode_count': instance.episodeCount,
      'season_number': instance.seasonNumber,
      'air_date': instance.airDate,
      'name': instance.name,
      'overview': instance.overview,
      'poster_path': instance.posterPath
    };
