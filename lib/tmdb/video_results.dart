import 'package:json_annotation/json_annotation.dart';
import 'video.dart';

part 'video_results.g.dart';

@JsonSerializable()
class VideoResults {
  VideoResults(this.results);

  final List<Video> results;

  factory VideoResults.fromJson(Map<String, dynamic> json) => _$VideoResultsFromJson(json);

  Map<String, dynamic> toJson() => _$VideoResultsToJson(this);
}