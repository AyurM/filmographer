class TmdbParams {
  //URL building
  static const String baseUrl = "https://api.themoviedb.org/3/";
  static const String discoverMovieUrl = "discover/movie?";
  static const String discoverTvUrl = "discover/tv?";
  static const String trendingUrl = "trending/";
  static const String movieUrl = "movie/";
  static const String tvUrl = "tv/";
  static const String seasonUrl = "/season/";
  static const String episodeUrl = "/episode/";
  static const String personUrl = "person/";
  static const String popularUrl = "popular?";
  static const String searchTvUrl = "search/tv?";
  static const String searchMovieUrl = "search/movie?";
  static const String searchPersonUrl = "search/person?";
  static const String imagesUrl = "/images?";
  static const String highResPosterBaseUrl = "http://image.tmdb.org/t/p/w500";
  static const String lowResPosterBaseUrl = "http://image.tmdb.org/t/p/w300";

  //Request options
  static const String apiKeyValue = "4cdbd4367d3bbac1a675ab6e9416c1e6";
  static const String apiKey = "api_key";
  static const String page = "page";
  static const String language = "language";
  static const String query = "query";
  static const String sortBy = "sort_by";
  static const String primaryReleaseDateGte = "primary_release_date.gte";
  static const String primaryReleaseDateLte = "primary_release_date.lte";
  static const String firstAirDateGte = "first_air_date.gte";
  static const String firstAirDateLte = "first_air_date.lte";
  static const String voteCountGte = "vote_count.gte";
  static const String voteAverageGte = "vote_average.gte";
  static const String withGenres = "with_genres";
  static const String defaultMovieVoteCountGte = "50";
  static const String defaultTvVoteCountGte = "30";
  static const String appendToResponse =
      "append_to_response=videos%2Cimages%2Ccredits%2Crecommendations%2Cexternal_ids";
  static const String appendToCastResponse =
      "append_to_response=images%2Ccombined_credits%2Cexternal_ids";
  static const String imageLanguage =
      "&include_image_language=include_image_language%3Den%2Cru%2Cnull";
  static const double defaultVoteAverage = 5.0;

  //YouTube
  static const String youTubeApiKeyValue =
      "AIzaSyDb4iFLmIb23XwjknrJ9hzjtvvvlq1itCY";
  static const String youTubeThumbnailUrl = "https://img.youtube.com/vi/";
  static const String youTubeThumbnailQuality = "/sddefault.jpg";
  static const String youTubeValue = "YouTube";
  static const String typeTrailer = "Trailer";

  static const List<String> movieGenres = <String>[
    'Любой',
    'Боевик',
    'Приключения',
    'Мультфильм',
    'Комедия',
    'Криминал',
    'Документальный',
    'Драма',
    'Семейный',
    'Фэнтези',
    'История',
    'Ужасы',
    'Музыка',
    'Детектив',
    'Мелодрама',
    'Фантастика',
    'ТВ фильм',
    'Триллер',
    'Военный',
    'Вестерн',
  ];

  static const List<String> tvShowGenres = <String>[
    'Любой',
    'Боевик и приключения',
    'Мультфильм',
    'Комедия',
    'Криминал',
    'Документальный',
    'Драма',
    'Семейный',
    'Детский',
    'Детектив',
    'Новости',
    'Реалити-шоу',
    'НФ и фэнтези',
    'Мыльная опера',
    'Ток-шоу',
    'Война и политика',
    'Вестерн',
  ];

  static const List<int> moviesGenreIds = <int>[
    0,
    28,
    12,
    16,
    35,
    80,
    99,
    18,
    10751,
    14,
    36,
    27,
    10402,
    9648,
    10749,
    878,
    10770,
    53,
    10752,
    37
  ];

  static const List<int> tvShowGenreIds = <int>[
    0,
    10759,
    16,
    35,
    80,
    99,
    18,
    10751,
    10762,
    9648,
    10763,
    10764,
    10765,
    10766,
    10767,
    10768,
    37,
  ];

//  static const String testJson = '''
//  {
//  "page": 1,
//  "total_results": 5209,
//  "total_pages": 261,
//  "results": [
//    {
//      "vote_count": 1074,
//      166428,
//      "video": false,
//      "vote_average": 7.8,
//      "title": "Как приручить дракона 3",
//      "popularity": 449.528,
//      "poster_path": "/mIPdhu9f32O3tzyW1wKKUBSFzP8.jpg",
//      "original_language": "en",
//      "original_title": "How to Train Your Dragon: The Hidden World",
//      "genre_ids": [
//        16,
//        10751,
//        12
//      ],
//      "backdrop_path": "/h3KN24PrOheHVYs9ypuOIdFBEpX.jpg",
//      "adult": false,
//      "overview": "Когда-то викинги жили в гармонии с драконами. В те времена они делили радость, горе… и последние штаны. Казалось, что так будет всегда, но появление загадочной Дневной Фурии изменило жизнь острова. И теперь Иккинг и Беззубик столкнутся с безжалостным охотником на драконов, жаждущим уничтожить все, что им дорого.",
//      "release_date": "2019-01-03"
//    },
//    {
//      "vote_count": 2458,
//      299537,
//      "video": false,
//      "vote_average": 7.3,
//      "title": "Капитан Марвел",
//      "popularity": 366.817,
//      "poster_path": "/cl7lFMru1YQs0Yf9vpTFPACoHhP.jpg",
//      "original_language": "en",
//      "original_title": "Captain Marvel",
//      "genre_ids": [
//        28,
//        12,
//        878
//      ],
//      "backdrop_path": "/w2PMyoyLU22YvrGK3smVM9fW1jj.jpg",
//      "adult": false,
//      "overview": "После столкновения с враждующими инопланетными расами пилот военно-воздушных сил Кэрол Дэнверс обретает суперсилу и становится неуязвимой. Героине предстоит совладать со своими новыми способностями, чтобы противостоять могущественному врагу.",
//      "release_date": "2019-03-06"
//    },
//    {
//      "vote_count": 411,
//      399361,
//      "video": false,
//      "vote_average": 6.3,
//      "title": "Тройная граница",
//      "popularity": 197.817,
//      "poster_path": "/aBw8zYuAljVM1FeK5bZKITPH8ZD.jpg",
//      "original_language": "en",
//      "original_title": "Triple Frontier",
//      "genre_ids": [
//        28,
//        53,
//        80
//      ],
//      "backdrop_path": "/s9I2LmQMYCanl6DvC3X1AOHs2r8.jpg",
//      "adult": false,
//      "overview": "Фильм рассказывает о пятерых друзьях, ветеранах военной службы, которые решают провернуть ограбление влиятельного наркобарона из Южной Америки.",
//      "release_date": "2019-03-06"
//    },
//    {
//      "vote_count": 4652,
//      297802,
//      "video": false,
//      "vote_average": 6.8,
//      "title": "Аквамен",
//      "popularity": 165.687,
//      "poster_path": "/fk6y3eWV1vFwZOY5k9IXecEpLyu.jpg",
//      "original_language": "en",
//      "original_title": "Aquaman",
//      "genre_ids": [
//        28,
//        12,
//        14,
//        878
//      ],
//      "backdrop_path": "/9QusGjxcYvfPD1THg6oW3RLeNn7.jpg",
//      "adult": false,
//      "overview": "Действие фильма разворачивается в необъятном и захватывающем подводном мире семи морей, а сюжет знакомит зрителей с историей происхождения получеловека-полуатланта Артура Карри и ключевыми событиями его жизни — теми, что заставят его не только столкнуться с самим собой, но и выяснить, достоин ли он быть тем, кем ему суждено… царем!",
//      "release_date": "2018-12-07"
//    },
//    {
//      "vote_count": 2727,
//      324857,
//      "video": false,
//      "vote_average": 8.5,
//      "title": "Человек-паук: Через вселенные",
//      "popularity": 157.433,
//      "poster_path": "/dEh3ksfCPRp41Q6RPkIjI64Zfr1.jpg",
//      "original_language": "en",
//      "original_title": "Spider-Man: Into the Spider-Verse",
//      "genre_ids": [
//        28,
//        12,
//        16,
//        878,
//        35
//      ],
//      "backdrop_path": "/uUiId6cG32JSRI6RyBQSvQtLjz2.jpg",
//      "adult": false,
//      "overview": "В центре сюжета уникальной и инновационной в визуальном плане картины подросток из Нью-Йорка Майлз Моралес, который живет в мире безграничных возможностей вселенных Человека-паука, где костюм супергероя носит не только он.",
//      "release_date": "2018-12-07"
//    },
//    {
//      "vote_count": 1596,
//      399579,
//      "video": false,
//      "vote_average": 6.7,
//      "title": "Алита: Боевой Ангел",
//      "popularity": 154.874,
//      "poster_path": "/1zw24g2m08p9oBxMtfJrNmSxqbR.jpg",
//      "original_language": "en",
//      "original_title": "Alita: Battle Angel",
//      "genre_ids": [
//        28,
//        878,
//        53,
//        12
//      ],
//      "backdrop_path": "/aQXTw3wIWuFMy0beXRiZ1xVKtcf.jpg",
//      "adult": false,
//      "overview": "XXVI век. На планете Земля богачи живут в Небесном городе, беднота довольствуется жизнью в Нижнем городе, куда сбрасываются все отходы и мусор. Однажды в куче металлолома Нижнего города учёный находит части женщины-киборга и возвращает её к жизни. Придя в сознание, киборг обнаруживает, что из ее памяти стерто все, кроме боевых приемов. Теперь она должна обрести утерянные воспоминания и выяснить, кто её отправил на свалку.",
//      "release_date": "2019-01-31"
//    },
//    {
//      "vote_count": 12182,
//      299536,
//      "video": false,
//      "vote_average": 8.3,
//      "title": "Мстители: Война бесконечности",
//      "popularity": 139.94,
//      "poster_path": "/qIUFg6tzKeK5bUDguonWCAFceNB.jpg",
//      "original_language": "en",
//      "original_title": "Avengers: Infinity War",
//      "genre_ids": [
//        12,
//        28,
//        14
//      ],
//      "backdrop_path": "/bOGkgRGdhrBYJSLpXaxhXVstddV.jpg",
//      "adult": false,
//      "overview": "Пока Мстители и их союзники продолжают защищать мир от различных опасностей, с которыми не смог бы справиться один супергерой, новая угроза возникает из космоса: Танос. Межгалактический тиран преследует цель собрать все шесть Камней Бесконечности — артефакты невероятной силы, с помощью которых можно менять реальность по своему желанию. Всё, с чем Мстители сталкивались ранее, вело к этому моменту — судьба Земли никогда ещё не была столь неопределённой.",
//      "release_date": "2018-04-25"
//    },
//    {
//      "vote_count": 880,
//      504172,
//      "video": false,
//      "vote_average": 6.5,
//      "title": "Наркокурьер",
//      "popularity": 128.205,
//      "poster_path": "/rGXNiLbN9xDKgEkQSqDZNJRENqa.jpg",
//      "original_language": "en",
//      "original_title": "The Mule",
//      "genre_ids": [
//        80,
//        18,
//        53
//      ],
//      "backdrop_path": "/bTeRgkAavyw1eCtSkaww18wLYNP.jpg",
//      "adult": false,
//      "overview": "Эрл Стоун - одинокий старик. Его частный бизнес находится на грани разорения, отношения с женой не ладятся, а дети, кажется, давно забыли о его существовании. Неожиданно ему предлагают не пыльную, но прибыльную работенку - просто водить машину. Стоун поначалу соглашается и вскоре понимает, что стал наркокурьером мексиканского картеля и попал в поле зрения оперативника Агентства по контролю за оборотом наркотиков.",
//      "release_date": "2018-12-14"
//    },
//    {
//      "vote_count": 3990,
//      338952,
//      "video": false,
//      "vote_average": 6.9,
//      "title": "Фантастические твари: Преступления Грин-де-Вальда",
//      "popularity": 110.362,
//      "poster_path": "/4wH16yc3Rmpx8Ob33QTJkFVNf5j.jpg",
//      "original_language": "en",
//      "original_title": "Fantastic Beasts: The Crimes of Grindelwald",
//      "genre_ids": [
//        10751,
//        14,
//        12
//      ],
//      "backdrop_path": "/wDN3FIcQQ1HI7mz1OOKYHSQtaiE.jpg",
//      "adult": false,
//      "overview": "В конце первого фильма могущественный темный волшебник Геллерт Грин-де-Вальд был пойман сотрудниками МАКУСА (Магического Конгресса Управления по Северной Америке), не без помощи Ньюта Саламандера. Выполняя свое обещание, темный маг устраивает грандиозный побег и начинает собирать сторонников, большинство из которых не знают о его истинной цели: добиться превосходства волшебников над всеми немагическими существами на планете. Чтобы сорвать планы Грин-де-Вальда, Альбус Дамблдор обращается к своему бывшему студенту Ньюту Саламандеру, который соглашается помочь, не подозревая, какая опасность ему грозит. В раскалывающемся на части волшебном мире любовь и верность проверяются на прочность, а конфликт разделяет даже настоящих друзей и членов семей.",
//      "release_date": "2018-11-14"
//    },
//    {
//      "vote_count": 1744,
//      450465,
//      "video": false,
//      "vote_average": 6.6,
//      "title": "Стекло",
//      "popularity": 108.167,
//      "poster_path": "/AfIIo5puMKjEfBGvB6RJ70535QE.jpg",
//      "original_language": "en",
//      "original_title": "Glass",
//      "genre_ids": [
//        53,
//        9648,
//        18,
//        14
//      ],
//      "backdrop_path": "/lvjscO8wmpEbIfOEZi92Je8Ktlg.jpg",
//      "adult": false,
//      "overview": "Похититель с множественным расстройством личности и террорист-инвалид «Мистер Стекло» выходят на своих давних противников — травмированную девочку-подростка Кейси и стареющего супергероя Дэвида Данна.",
//      "release_date": "2019-01-16"
//    },
//    {
//      "vote_count": 414,
//      512196,
//      "video": false,
//      "vote_average": 6.2,
//      "title": "Счастливого нового дня смерти",
//      "popularity": 99.748,
//      "poster_path": "/ZstlucYRCRfkZ74O2LUfbbNbyb.jpg",
//      "original_language": "en",
//      "original_title": "Happy Death Day 2U",
//      "genre_ids": [
//        27,
//        9648,
//        53,
//        878,
//        35
//      ],
//      "backdrop_path": "/dhNJHBDacrZjSPtwaiwp3idpzxU.jpg",
//      "adult": false,
//      "overview": "На прошлый день рождения судьба преподнесла ей сомнительный подарок — праздник повторялся снова и снова, а в финале её неизменно убивал маньяк в маске. То ли дело в этом году: какое счастье проснуться с утра пораньше и оказаться в той же самой петле времени! Только теперь с тобой умирают все твои друзья…",
//      "release_date": "2019-02-13"
//    },
//    {
//      "vote_count": 1047,
//      400650,
//      "video": false,
//      "vote_average": 6.7,
//      "title": "Мэри Поппинс возвращается",
//      "popularity": 94.535,
//      "poster_path": "/uUm76WfwQXWhHH2T2ZGLa03tVsZ.jpg",
//      "original_language": "en",
//      "original_title": "Mary Poppins Returns",
//      "genre_ids": [
//        14,
//        10751,
//        35
//      ],
//      "backdrop_path": "/cwiJQXezWz876K3jS57Sq56RYCZ.jpg",
//      "adult": false,
//      "overview": "Фильм рассказывает о новых приключениях Мэри и ее друга Джека, которым предстоит встретиться с представителями следующего поколения семейства Бэнкс.",
//      "release_date": "2018-12-13"
//    },
//    {
//      "vote_count": 7081,
//      920,
//      "video": false,
//      "vote_average": 6.7,
//      "title": "Тачки",
//      "popularity": 93.974,
//      "poster_path": "/p6f6diLLGR27j1glPrsP49hO5H8.jpg",
//      "original_language": "en",
//      "original_title": "Cars",
//      "genre_ids": [
//        16,
//        12,
//        35,
//        10751
//      ],
//      "backdrop_path": "/a1MlbLBk5Sy6YvMbSuKfwGlDVlb.jpg",
//      "adult": false,
//      "overview": "Неукротимый в своем желании всегда и во всем побеждать гоночный автомобиль «Молния» Маккуин вдруг обнаруживает, что сбился с пути и застрял в маленьком захолустном городке Радиатор-Спрингс, что находится где-то на трассе 66 в Калифорнии. Участвуя в гонках на Кубок Поршня, где ему противостояли два очень опытных соперника, Маккуин совершенно не ожидал, что отныне ему придется общаться с персонажами совсем иного рода...",
//      "release_date": "2006-06-08"
//    },
//    {
//      "vote_count": 1419,
//      424783,
//      "video": false,
//      "vote_average": 6.5,
//      "title": "Бамблби",
//      "popularity": 93.533,
//      "poster_path": "/2k9nXxST186qGgvs7CO0yrPI3Yf.jpg",
//      "original_language": "en",
//      "original_title": "Bumblebee",
//      "genre_ids": [
//        28,
//        12,
//        878
//      ],
//      "backdrop_path": "/hMANgfPHR1tRObNp2oPiOi9mMlz.jpg",
//      "adult": false,
//      "overview": "1987 год. Скрываясь от преследования, Бамблби находит убежище на автомобильной свалке в калифорнийском городке, где живет Чарли. Девушке скоро исполнится 18, она стремится найти свое место в жизни. И именно она наталкивается на покореженного и сломленного Бамблби. Приведя его в чувство, Чарли моментально осознает: на ее пути оказался не простой желтый Фольксваген Жук…",
//      "release_date": "2018-12-15"
//    },
//    {
//      "vote_count": 6326,
//      424694,
//      "video": false,
//      "vote_average": 8.1,
//      "title": "Богемская рапсодия",
//      "popularity": 83.447,
//      "poster_path": "/hGuih2gomFOJzzFnAUsfLF87SVR.jpg",
//      "original_language": "en",
//      "original_title": "Bohemian Rhapsody",
//      "genre_ids": [
//        18,
//        10402
//      ],
//      "backdrop_path": "/xcaSYLBhmDzJ6P14bcKe0KTh3QV.jpg",
//      "adult": false,
//      "overview": "«Богемская рапсодия» - это чествование группы Queen, их музыки и их выдающегося вокалиста Фредди Меркьюри, который бросил вызов стереотипам и победил условности, чтобы стать одним из самых любимых артистов на планете. Фильм прослеживает головокружительный путь группы к успеху, благодаря их культовым песням и революционному звуку, практически распад коллектива, поскольку образ жизни Меркьюри выходит из-под контроля, и их триумфальное воссоединение накануне концерта Live Aid, ставшем одним из величайших выступлений в истории рок-музыки. Фильм цементирует наследие группы, отношения внутри которой всегда были больше похожи на семейные, и которая продолжают вдохновлять неудачников, мечтателей и любителей музыки по сей день.",
//      "release_date": "2018-10-24"
//    },
//    {
//      "vote_count": 2445,
//      490132,
//      "video": false,
//      "vote_average": 8.3,
//      "title": "Зелёная книга",
//      "popularity": 82.547,
//      "poster_path": "/aEeTnDzcnGRD5IjSFwVXjusKpu0.jpg",
//      "original_language": "en",
//      "original_title": "Green Book",
//      "genre_ids": [
//        18,
//        35,
//        10402
//      ],
//      "backdrop_path": "/78PjwaykLY2QqhMfWRDvmfbC6EV.jpg",
//      "adult": false,
//      "overview": "1960-е годы. После закрытия нью-йоркского ночного клуба на ремонт вышибала Тони по прозвищу Болтун ищет подработку на пару месяцев. Как раз в это время Дон Ширли, утонченный светский лев, богатый и талантливый чернокожий музыкант, исполняющий классическую музыку, собирается в турне по южным штатам, где ещё сильны расистские убеждения и царит сегрегация. Он нанимает Тони в качестве водителя, телохранителя и человека, способного решать текущие проблемы. У этих двоих так мало общего, и эта поездка навсегда изменит жизнь обоих.",
//      "release_date": "2018-11-16"
//    },
//    {
//      "vote_count": 3345,
//      337167,
//      "video": false,
//      "vote_average": 6.1,
//      "title": "Пятьдесят оттенков свободы",
//      "popularity": 72.751,
//      "poster_path": "/uZ5ERtY6q8GvPZmJmuW6hQRQ1nO.jpg",
//      "original_language": "en",
//      "original_title": "Fifty Shades Freed",
//      "genre_ids": [
//        18,
//        10749
//      ],
//      "backdrop_path": "/9ywA15OAiwjSTvg3cBs9B7kOCBF.jpg",
//      "adult": false,
//      "overview": "Кристиан и Анастейша поженились и живут в своё удовольствие, наслаждаясь обществом друг друга. Но жизнь новоиспечённой миссис Грей находится в опасности, поскольку объявляется недруг, который собирается мстить, используя свою богатую фантазию. Призраки прошлого Кристиана вновь вернулись, а тучи над супругами сгущаются всё сильнее.",
//      "release_date": "2018-01-17"
//    },
//    {
//      "vote_count": 1777,
//      480530,
//      "video": false,
//      "vote_average": 6.6,
//      "title": "Крид 2",
//      "popularity": 68.642,
//      "poster_path": "/1FZuRgUP3fPG5CD9sTNHaWK62qb.jpg",
//      "original_language": "en",
//      "original_title": "Creed II",
//      "genre_ids": [
//        18
//      ],
//      "backdrop_path": "/6JHYYbvoSuQ95ceGx8Oeg8zzAjg.jpg",
//      "adult": false,
//      "overview": "Фильм рассказывает о возвращении на ринг боксера Адониса Крида, тренером которого выступает легендарный Рокки.",
//      "release_date": "2018-11-21"
//    },
//    {
//      "vote_count": 16978,
//      118340,
//      "video": false,
//      "vote_average": 7.9,
//      "title": "Стражи Галактики",
//      "popularity": 67.921,
//      "poster_path": "/L6U6zH3N39toWXIjvfPjxgRXuG.jpg",
//      "original_language": "en",
//      "original_title": "Guardians of the Galaxy",
//      "genre_ids": [
//        28,
//        878,
//        12
//      ],
//      "backdrop_path": "/bHarw8xrmQeqf3t8HpuMY7zoK4x.jpg",
//      "adult": false,
//      "overview": "Отважному путешественнику Питеру Квиллу попадает в руки таинственный артефакт, принадлежащий могущественному и безжалостному злодею Ронану, строящему коварные планы по захвату Вселенной. Питер оказывается в центре межгалактической охоты, где жертва — он сам. Единственный способ спасти свою жизнь — объединиться с четверкой нелюдимых изгоев: воинственным енотом по кличке Ракета, человекоподобным деревом Грутом, смертельно опасной Гаморой и одержимым жаждой мести Драксом, также известным как Разрушитель. Когда Квилл понимает, какой силой обладает украденный артефакт и какую опасность он представляет для вселенной, одиночка пойдет на все, чтобы сплотить случайных союзников для решающей битвы за судьбу галактики.",
//      "release_date": "2014-07-30"
//    },
//    {
//      "vote_count": 1943,
//      404368,
//      "video": false,
//      "vote_average": 7.3,
//      "title": "Ральф против Интернета",
//      "popularity": 66.475,
//      "poster_path": "/tVmomifGhoJfijkOnSGDDZwRSH5.jpg",
//      "original_language": "en",
//      "original_title": "Ralph Breaks the Internet",
//      "genre_ids": [
//        10751,
//        16,
//        35,
//        14
//      ],
//      "backdrop_path": "/88poTBTafMXaz73vYi3c74g0y2k.jpg",
//      "adult": false,
//      "overview": "На этот раз Ральф и Ванилопа фон Кекс выйдут за пределы зала игровых автоматов и отправятся покорять бескрайние просторы Интернета, который может и не выдержать сокрушительного обаяния громилы.",
//      "release_date": "2018-11-20"
//    }
//  ]
//}''';
}
