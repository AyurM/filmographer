import 'package:json_annotation/json_annotation.dart';
import 'tmdb_person.dart';

part 'crew.g.dart';

@JsonSerializable()
class Crew extends TmdbPerson {
  Crew(this.department, int id, int gender, this.job, String name,
      String profilePath)
      : super(id, gender, name, profilePath);

  final String department;
  final String job;

  factory Crew.fromJson(Map<String, dynamic> json) => _$CrewFromJson(json);

  Map<String, dynamic> toJson() => _$CrewToJson(this);
}
