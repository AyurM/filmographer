// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cast_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CastDetails _$CastDetailsFromJson(Map<String, dynamic> json) {
  return CastDetails(
      json['id'] as int,
      json['birthday'] as String,
      json['deathday'] as String,
      json['name'] as String,
      json['gender'] as int,
      json['biography'] as String,
      (json['popularity'] as num)?.toDouble(),
      json['place_of_birth'] as String,
      json['imdb_id'] as String,
      json['profile_path'] as String,
      json['homepage'] as String,
      json['combined_credits'] == null
          ? null
          : CombinedCreditsResults.fromJson(
              json['combined_credits'] as Map<String, dynamic>),
      json['images'] == null
          ? null
          : ImageCastResults.fromJson(json['images'] as Map<String, dynamic>),
      json['known_for_department'] as String,
      json['adult'] as bool,
      json['external_ids'] == null
          ? null
          : ExternalIds.fromJson(json['external_ids'] as Map<String, dynamic>));
}

Map<String, dynamic> _$CastDetailsToJson(CastDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'gender': instance.gender,
      'popularity': instance.popularity,
      'adult': instance.adult,
      'birthday': instance.birthday,
      'deathday': instance.deathday,
      'name': instance.name,
      'homepage': instance.homepage,
      'biography': instance.biography,
      'known_for_department': instance.knownForDepartment,
      'place_of_birth': instance.placeOfBirth,
      'imdb_id': instance.imdbId,
      'profile_path': instance.profilePath,
      'combined_credits': instance.combinedCredits,
      'external_ids': instance.externalIds,
      'images': instance.images
    };
