import 'package:json_annotation/json_annotation.dart';
import 'tmdb_person.dart';

part 'cast.g.dart';

@JsonSerializable()
class Cast extends TmdbPerson {
  Cast(int id, int gender, String name, String profilePath, this.order)
      : super(id, gender, name, profilePath);

  final int order;

  factory Cast.fromJson(Map<String, dynamic> json) => _$CastFromJson(json);

  Map<String, dynamic> toJson() => _$CastToJson(this);
}
