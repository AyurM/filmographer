import 'package:json_annotation/json_annotation.dart';

part 'external_ids.g.dart';

@JsonSerializable()
class ExternalIds {

  @JsonKey(name: 'instagram_id')
  final String instagram;
  @JsonKey(name: 'twitter_id')
  final String twitter;
  @JsonKey(name: 'facebook_id')
  final String facebook;
  @JsonKey(name: 'imdb_id')
  final String imdb;

  ExternalIds(this.instagram, this.twitter, this.facebook, this.imdb);

  bool isEmpty(){
    return instagram == null && twitter == null && facebook == null && imdb == null;
  }

  factory ExternalIds.fromJson(Map<String, dynamic> json) => _$ExternalIdsFromJson(json);

  Map<String, dynamic> toJson() => _$ExternalIdsToJson(this);
}