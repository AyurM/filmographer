import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/tmdb/tv_show.dart';

part 'discover_tv_results.g.dart';

@JsonSerializable()
class DiscoverTvResults {
  DiscoverTvResults(
      {this.page, this.totalResults, this.totalPages, this.results});

  final int page;
  @JsonKey(name: 'total_results')
  final int totalResults;
  @JsonKey(name: 'total_pages')
  final int totalPages;
  final List<TvShow> results;

  factory DiscoverTvResults.fromJson(Map<String, dynamic> json) =>
      _$DiscoverTvResultsFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverTvResultsToJson(this);
}