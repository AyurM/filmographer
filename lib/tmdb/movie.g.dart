// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return Movie(
      voteCount: json['vote_count'] as int,
      id: json['id'] as int,
      voteAverage: (json['vote_average'] as num)?.toDouble(),
      title: json['title'] as String,
      popularity: (json['popularity'] as num)?.toDouble(),
      posterPath: json['poster_path'] as String,
      originalLanguage: json['original_language'] as String,
      originalTitle: json['original_title'] as String,
      genreIds: (json['genre_ids'] as List)?.map((e) => e as int)?.toList(),
      backdropPath: json['backdrop_path'] as String,
      overview: json['overview'] as String,
      releaseDate: json['release_date'] as String);
}

Map<String, dynamic> _$MovieToJson(Movie instance) => <String, dynamic>{
      'vote_count': instance.voteCount,
      'id': instance.id,
      'vote_average': instance.voteAverage,
      'popularity': instance.popularity,
      'poster_path': instance.posterPath,
      'original_language': instance.originalLanguage,
      'backdrop_path': instance.backdropPath,
      'overview': instance.overview,
      'genre_ids': instance.genreIds,
      'title': instance.title,
      'original_title': instance.originalTitle,
      'release_date': instance.releaseDate
    };
