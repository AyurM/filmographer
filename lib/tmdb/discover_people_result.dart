import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/tmdb/person_result.dart';

part 'discover_people_result.g.dart';

@JsonSerializable()
class DiscoverPeopleResults {
  DiscoverPeopleResults(
      {this.page, this.totalResults, this.totalPages, this.results});

  final int page;
  @JsonKey(name: 'total_results')
  final int totalResults;
  @JsonKey(name: 'total_pages')
  final int totalPages;
  final List<PersonResult> results;

  factory DiscoverPeopleResults.fromJson(Map<String, dynamic> json) =>
      _$DiscoverPeopleResultsFromJson(json);

  Map<String, dynamic> toJson() => _$DiscoverPeopleResultsToJson(this);
}
