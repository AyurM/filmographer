import 'package:json_annotation/json_annotation.dart';
import 'movie.dart';
import 'tv_show.dart';

part 'combined_credits_crew.g.dart';

@JsonSerializable()
class CombinedCreditsCrew {
  CombinedCreditsCrew(
      this.id,
      this.name,
      this.overview,
      this.popularity,
      this.mediaType,
      this.voteCount,
      this.voteAverage,
      this.posterPath,
      this.genreIds,
      this.title,
      this.originalTitle,
      this.releaseDate,
      this.episodeCount,
      this.originalName,
      this.firstAirDate,
      this.originalLanguage,
      this.backdropPath,
      this.department,
      this.job);

  //Shared attributes
  final int id;
  final String overview;
  final String department;
  final String job;
  final double popularity;

  @JsonKey(name: 'media_type')
  final String mediaType;

  @JsonKey(name: 'vote_count')
  final int voteCount;

  @JsonKey(name: 'vote_average')
  final double voteAverage;

  @JsonKey(name: 'poster_path')
  final String posterPath;

  @JsonKey(name: 'backdrop_path')
  final String backdropPath;

  @JsonKey(name: 'genre_ids')
  final List<int> genreIds;

  @JsonKey(name: 'original_language')
  final String originalLanguage;

  //Movie attributes
  final String title;

  @JsonKey(name: 'original_title')
  final String originalTitle;

  @JsonKey(name: 'release_date')
  final String releaseDate;

  //TvShow attributes
  final String name;

  @JsonKey(name: 'episode_count')
  final int episodeCount;

  @JsonKey(name: 'original_name')
  final String originalName;

  @JsonKey(name: 'first_air_date')
  final String firstAirDate;

  Movie getMovie() {
    if (isMovie())
      return Movie(
          voteCount: voteCount,
          id: id,
          voteAverage: voteAverage,
          title: title,
          popularity: popularity,
          posterPath: posterPath,
          originalLanguage: originalLanguage,
          originalTitle: originalTitle,
          genreIds: genreIds,
          backdropPath: backdropPath,
          overview: overview,
          releaseDate: releaseDate);
    else
      return null;
  }

  TvShow getTvShow() {
    if (!isMovie())
      return TvShow(
          voteCount: voteCount,
          id: id,
          voteAverage: voteAverage,
          name: name,
          popularity: popularity,
          posterPath: posterPath,
          originalLanguage: originalLanguage,
          originalName: originalName,
          genreIds: genreIds,
          backdropPath: backdropPath,
          overview: overview,
          firstAirDate: firstAirDate);
    else
      return null;
  }

  bool isMovie() {
    return mediaType == "movie";
  }

  factory CombinedCreditsCrew.fromJson(Map<String, dynamic> json) =>
      _$CombinedCreditsCrewFromJson(json);

  Map<String, dynamic> toJson() => _$CombinedCreditsCrewToJson(this);
}
