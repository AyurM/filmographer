import 'package:json_annotation/json_annotation.dart';

abstract class TmdbPerson {
  final int id;
  final int gender;
  final String name;
  @JsonKey(name: 'profile_path')
  final String profilePath;

  TmdbPerson(this.id, this.gender, this.name, this.profilePath);
}
