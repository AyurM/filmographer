// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_show_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TvShowDetails _$TvShowDetailsFromJson(Map<String, dynamic> json) {
  return TvShowDetails(
      id: json['id'] as int,
      credits: json['credits'] == null
          ? null
          : Credits.fromJson(json['credits'] as Map<String, dynamic>),
      numberOfEpisodes: json['number_of_episodes'] as int,
      numberOfSeasons: json['number_of_seasons'] as int,
      homepage: json['homepage'] as String,
      images: json['images'] == null
          ? null
          : ImageResults.fromJson(json['images'] as Map<String, dynamic>),
      episodeRunTime:
          (json['episode_run_time'] as List)?.map((e) => e as int)?.toList(),
      inProduction: json['in_production'] as bool,
      genres: (json['genres'] as List)
          ?.map((e) =>
              e == null ? null : Genre.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      recommendations: json['recommendations'] == null
          ? null
          : DiscoverTvResults.fromJson(
              json['recommendations'] as Map<String, dynamic>),
      videos: json['videos'] == null
          ? null
          : VideoResults.fromJson(json['videos'] as Map<String, dynamic>),
      seasons: (json['seasons'] as List)
          ?.map((e) =>
              e == null ? null : TvSeason.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      externalIds: json['external_ids'] == null
          ? null
          : ExternalIds.fromJson(json['external_ids'] as Map<String, dynamic>));
}

Map<String, dynamic> _$TvShowDetailsToJson(TvShowDetails instance) =>
    <String, dynamic>{
      'genres': instance.genres,
      'homepage': instance.homepage,
      'id': instance.id,
      'in_production': instance.inProduction,
      'episode_run_time': instance.episodeRunTime,
      'number_of_episodes': instance.numberOfEpisodes,
      'number_of_seasons': instance.numberOfSeasons,
      'external_ids': instance.externalIds,
      'images': instance.images,
      'credits': instance.credits,
      'recommendations': instance.recommendations,
      'videos': instance.videos,
      'seasons': instance.seasons
    };
