// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'discover_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DiscoverResults _$DiscoverResultsFromJson(Map<String, dynamic> json) {
  return DiscoverResults(
      page: json['page'] as int,
      totalResults: json['total_results'] as int,
      totalPages: json['total_pages'] as int,
      results: (json['results'] as List)
          ?.map((e) =>
              e == null ? null : Movie.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$DiscoverResultsToJson(DiscoverResults instance) =>
    <String, dynamic>{
      'page': instance.page,
      'total_results': instance.totalResults,
      'total_pages': instance.totalPages,
      'results': instance.results
    };
