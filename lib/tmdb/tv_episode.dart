import 'package:json_annotation/json_annotation.dart';
import 'cast.dart';

part 'tv_episode.g.dart';

@JsonSerializable()
class TvEpisode {
  TvEpisode(
      this.id,
      this.seasonNumber,
      this.airDate,
      this.name,
      this.overview,
      this.episodeNumber,
      this.voteCount,
      this.stillPath,
      this.voteAverage,
      this.guestStars);

  final int id;
  @JsonKey(name: 'episode_number')
  final int episodeNumber;
  @JsonKey(name: 'season_number')
  final int seasonNumber;
  @JsonKey(name: 'vote_count')
  final int voteCount;
  @JsonKey(name: 'air_date')
  final String airDate;
  final String name;
  final String overview;
  @JsonKey(name: 'still_path')
  final String stillPath;
  @JsonKey(name: 'vote_average')
  final double voteAverage;

  @JsonKey(name: 'guest_stars')
  final List<Cast> guestStars;

  factory TvEpisode.fromJson(Map<String, dynamic> json) =>
      _$TvEpisodeFromJson(json);

  Map<String, dynamic> toJson() => _$TvEpisodeToJson(this);
}
