// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_cast_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageCastResults _$ImageCastResultsFromJson(Map<String, dynamic> json) {
  return ImageCastResults((json['profiles'] as List)
      ?.map((e) =>
          e == null ? null : Backdrop.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

Map<String, dynamic> _$ImageCastResultsToJson(ImageCastResults instance) =>
    <String, dynamic>{'profiles': instance.profiles};
