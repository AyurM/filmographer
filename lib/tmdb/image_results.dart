import 'package:json_annotation/json_annotation.dart';
import 'backdrop.dart';

part 'image_results.g.dart';

@JsonSerializable()
class ImageResults{

  ImageResults(this.id, this.backdrops, this.stills);

  final int id;
  final List<Backdrop> backdrops;
  final List<Backdrop> stills;

  factory ImageResults.fromJson(Map<String, dynamic> json) =>
      _$ImageResultsFromJson(json);

  Map<String, dynamic> toJson() => _$ImageResultsToJson(this);
}