import 'package:json_annotation/json_annotation.dart';

abstract class TmdbVideo {
  @JsonKey(name: 'vote_count')
  final int voteCount;
  final int id;

  @JsonKey(name: 'vote_average')
  final double voteAverage;

  final double popularity;

  @JsonKey(name: 'poster_path')
  final String posterPath;

  @JsonKey(name: 'original_language')
  final String originalLanguage;

  @JsonKey(name: 'backdrop_path')
  final String backdropPath;

  final String overview;

  @JsonKey(name: 'genre_ids')
  final List<int> genreIds;

  TmdbVideo(
      this.voteCount,
      this.id,
      this.voteAverage,
      this.popularity,
      this.posterPath,
      this.originalLanguage,
      this.backdropPath,
      this.overview,
      this.genreIds);

  String get title;
  String get releaseDate;
  String get originalTitle;
}
