// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Video _$VideoFromJson(Map<String, dynamic> json) {
  return Video(json['key'] as String, json['name'] as String,
      json['type'] as String, json['site'] as String, json['size'] as int);
}

Map<String, dynamic> _$VideoToJson(Video instance) => <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'type': instance.type,
      'site': instance.site,
      'size': instance.size
    };
