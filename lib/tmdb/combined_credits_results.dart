import 'package:json_annotation/json_annotation.dart';
import 'combined_credits_cast.dart';
import 'combined_credits_crew.dart';

part 'combined_credits_results.g.dart';

@JsonSerializable()
class CombinedCreditsResults {
  CombinedCreditsResults(this.cast, this.crew);

  final List<CombinedCreditsCast> cast;
  final List<CombinedCreditsCrew> crew;

  factory CombinedCreditsResults.fromJson(Map<String, dynamic> json) =>
      _$CombinedCreditsResultsFromJson(json);

  Map<String, dynamic> toJson() => _$CombinedCreditsResultsToJson(this);
}