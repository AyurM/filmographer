// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'backdrop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Backdrop _$BackdropFromJson(Map<String, dynamic> json) {
  return Backdrop((json['aspect_ratio'] as num)?.toDouble(),
      json['file_path'] as String, json['height'] as int, json['width'] as int);
}

Map<String, dynamic> _$BackdropToJson(Backdrop instance) => <String, dynamic>{
      'aspect_ratio': instance.aspectRatio,
      'file_path': instance.filePath,
      'height': instance.height,
      'width': instance.width
    };
