import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/tmdb/external_ids.dart';
import 'genre.dart';
import 'discover_tv_results.dart';
import 'image_results.dart';
import 'video_results.dart';
import 'credits.dart';
import 'tv_season.dart';

part 'tv_show_details.g.dart';

@JsonSerializable()
class TvShowDetails {
  TvShowDetails(
      {this.id,
      this.credits,
      this.numberOfEpisodes,
      this.numberOfSeasons,
      this.homepage,
      this.images,
      this.episodeRunTime,
      this.inProduction,
      this.genres,
      this.recommendations,
      this.videos,
      this.seasons,
      this.externalIds});

  final List<Genre> genres;
  final String homepage;
  final int id;

  @JsonKey(name: 'in_production')
  final bool inProduction;

  @JsonKey(name: 'episode_run_time')
  final List<int> episodeRunTime;

  @JsonKey(name: 'number_of_episodes')
  final int numberOfEpisodes;

  @JsonKey(name: 'number_of_seasons')
  final int numberOfSeasons;

  @JsonKey(name: 'external_ids')
  final ExternalIds externalIds;

  final ImageResults images;
  final Credits credits;
  final DiscoverTvResults recommendations;
  final VideoResults videos;
  final List<TvSeason> seasons;

  factory TvShowDetails.fromJson(Map<String, dynamic> json) =>
      _$TvShowDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TvShowDetailsToJson(this);
}
