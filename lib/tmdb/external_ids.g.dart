// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'external_ids.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExternalIds _$ExternalIdsFromJson(Map<String, dynamic> json) {
  return ExternalIds(
      json['instagram_id'] as String,
      json['twitter_id'] as String,
      json['facebook_id'] as String,
      json['imdb_id'] as String);
}

Map<String, dynamic> _$ExternalIdsToJson(ExternalIds instance) =>
    <String, dynamic>{
      'instagram_id': instance.instagram,
      'twitter_id': instance.twitter,
      'facebook_id': instance.facebook,
      'imdb_id': instance.imdb
    };
