import 'package:json_annotation/json_annotation.dart';
import 'package:filmographer/string_utils.dart';
import 'tmdb_video.dart';

part 'movie.g.dart';

@JsonSerializable()
class Movie extends TmdbVideo {
  Movie(
      {int voteCount,
      int id,
      double voteAverage,
      this.title,
      double popularity,
      String posterPath,
      String originalLanguage,
      this.originalTitle,
      List<int> genreIds,
      String backdropPath,
      String overview,
      this.releaseDate})
      : super(voteCount, id, voteAverage, popularity, posterPath,
            originalLanguage, backdropPath, overview, genreIds);

  final String title;

  @JsonKey(name: 'original_title')
  final String originalTitle;

  @JsonKey(name: 'release_date')
  final String releaseDate;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  Map<String, dynamic> toJson() => _$MovieToJson(this);

  //для БД SQLite
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'voteCount': voteCount,
      'voteAverage': voteAverage,
      'title': title,
      'popularity': popularity,
      'posterPath': posterPath,
      'originalLanguage': originalLanguage,
      'originalTitle': originalTitle,
      'genreIds': StringUtils.getGenreIdsString(genreIds),
      'backdropPath': backdropPath,
      'overview': overview,
      'releaseDate': releaseDate
    };
  }

  //для БД SQLite
  factory Movie.fromMap(Map<String, dynamic> dbMap) => new Movie(
      voteCount: dbMap['voteCount'] as int,
      id: dbMap['id'] as int,
      voteAverage: (dbMap['voteAverage'] as num)?.toDouble(),
      title: dbMap['title'] as String,
      popularity: (dbMap['popularity'] as num)?.toDouble(),
      posterPath: dbMap['posterPath'] as String,
      originalLanguage: dbMap['originalLanguage'] as String,
      originalTitle: dbMap['originalTitle'] as String,
      genreIds: (dbMap['genreIds'] as String)
          .split(',')
          .map((x) => int.parse(x))
          .toList(),
      backdropPath: dbMap['backdropPath'] as String,
      overview: dbMap['overview'] as String,
      releaseDate: dbMap['releaseDate'] as String);

  @override
  bool operator ==(o) => o is Movie && o.id == id;

  @override
  int get hashCode => id.hashCode;
}
