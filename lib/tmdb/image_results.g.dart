// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageResults _$ImageResultsFromJson(Map<String, dynamic> json) {
  return ImageResults(
      json['id'] as int,
      (json['backdrops'] as List)
          ?.map((e) =>
              e == null ? null : Backdrop.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      (json['stills'] as List)
          ?.map((e) =>
              e == null ? null : Backdrop.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ImageResultsToJson(ImageResults instance) =>
    <String, dynamic>{
      'id': instance.id,
      'backdrops': instance.backdrops,
      'stills': instance.stills
    };
