// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'combined_credits_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CombinedCreditsResults _$CombinedCreditsResultsFromJson(
    Map<String, dynamic> json) {
  return CombinedCreditsResults(
      (json['cast'] as List)
          ?.map((e) => e == null
              ? null
              : CombinedCreditsCast.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      (json['crew'] as List)
          ?.map((e) => e == null
              ? null
              : CombinedCreditsCrew.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$CombinedCreditsResultsToJson(
        CombinedCreditsResults instance) =>
    <String, dynamic>{'cast': instance.cast, 'crew': instance.crew};
