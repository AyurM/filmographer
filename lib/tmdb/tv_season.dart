import 'package:json_annotation/json_annotation.dart';

part 'tv_season.g.dart';

@JsonSerializable()
class TvSeason {
  TvSeason(this.id, this.episodeCount, this.seasonNumber, this.airDate,
      this.name, this.overview, this.posterPath);

  final int id;
  @JsonKey(name: 'episode_count')
  final int episodeCount;
  @JsonKey(name: 'season_number')
  final int seasonNumber;
  @JsonKey(name: 'air_date')
  final String airDate;
  final String name;
  final String overview;
  @JsonKey(name: 'poster_path')
  final String posterPath;

  factory TvSeason.fromJson(Map<String, dynamic> json) =>
      _$TvSeasonFromJson(json);

  Map<String, dynamic> toJson() => _$TvSeasonToJson(this);
}
