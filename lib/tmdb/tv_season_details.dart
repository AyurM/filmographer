import 'package:json_annotation/json_annotation.dart';
import 'tv_episode.dart';

part 'tv_season_details.g.dart';

@JsonSerializable()
class TvSeasonDetails {
  TvSeasonDetails(this.id, this.episodes, this.seasonNumber, this.airDate,
      this.name, this.overview, this.posterPath);

  final int id;
  @JsonKey(name: 'season_number')
  final int seasonNumber;
  @JsonKey(name: 'air_date')
  final String airDate;
  final String name;
  final String overview;
  @JsonKey(name: 'poster_path')
  final String posterPath;

  final List<TvEpisode> episodes;

  factory TvSeasonDetails.fromJson(Map<String, dynamic> json) =>
      _$TvSeasonDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TvSeasonDetailsToJson(this);
}
