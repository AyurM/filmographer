// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'discover_tv_results.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DiscoverTvResults _$DiscoverTvResultsFromJson(Map<String, dynamic> json) {
  return DiscoverTvResults(
      page: json['page'] as int,
      totalResults: json['total_results'] as int,
      totalPages: json['total_pages'] as int,
      results: (json['results'] as List)
          ?.map((e) =>
              e == null ? null : TvShow.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$DiscoverTvResultsToJson(DiscoverTvResults instance) =>
    <String, dynamic>{
      'page': instance.page,
      'total_results': instance.totalResults,
      'total_pages': instance.totalPages,
      'results': instance.results
    };
