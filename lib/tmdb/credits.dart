import 'package:json_annotation/json_annotation.dart';
import 'cast.dart';
import 'crew.dart';

part 'credits.g.dart';

@JsonSerializable()
class Credits{
  Credits(this.cast, this.crew);

  final List<Cast> cast;
  final List<Crew> crew;

  factory Credits.fromJson(Map<String, dynamic> json) => _$CreditsFromJson(json);

  Map<String, dynamic> toJson() => _$CreditsToJson(this);
}