import 'package:json_annotation/json_annotation.dart';
import 'backdrop.dart';

part 'image_cast_results.g.dart';

@JsonSerializable()
class ImageCastResults {
  ImageCastResults(this.profiles);

  final List<Backdrop> profiles;

  factory ImageCastResults.fromJson(Map<String, dynamic> json) =>
      _$ImageCastResultsFromJson(json);

  Map<String, dynamic> toJson() => _$ImageCastResultsToJson(this);
}