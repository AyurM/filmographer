// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'person_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PersonResult _$PersonResultFromJson(Map<String, dynamic> json) {
  return PersonResult(
      json['id'] as int,
      json['name'] as String,
      json['profile_path'] as String,
      (json['popularity'] as num)?.toDouble(),
      (json['known_for'] as List)
          ?.map((e) => e == null
              ? null
              : CombinedCreditsCast.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['adult'] as bool);
}

Map<String, dynamic> _$PersonResultToJson(PersonResult instance) =>
    <String, dynamic>{
      'id': instance.id,
      'popularity': instance.popularity,
      'adult': instance.adult,
      'name': instance.name,
      'profile_path': instance.profilePath,
      'known_for': instance.knownFor
    };
