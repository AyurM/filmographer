// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tv_season_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TvSeasonDetails _$TvSeasonDetailsFromJson(Map<String, dynamic> json) {
  return TvSeasonDetails(
      json['id'] as int,
      (json['episodes'] as List)
          ?.map((e) =>
              e == null ? null : TvEpisode.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['season_number'] as int,
      json['air_date'] as String,
      json['name'] as String,
      json['overview'] as String,
      json['poster_path'] as String);
}

Map<String, dynamic> _$TvSeasonDetailsToJson(TvSeasonDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'season_number': instance.seasonNumber,
      'air_date': instance.airDate,
      'name': instance.name,
      'overview': instance.overview,
      'poster_path': instance.posterPath,
      'episodes': instance.episodes
    };
