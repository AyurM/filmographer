import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/tmdb/person_summary.dart';
import 'package:filmographer/db/db_provider.dart';

class PeopleModel extends Model {
  PeopleModel() {
    DBProvider.db.getAllPeople().then((people) {
      _people = people;
      _isLoading = false;
      notifyListeners();
    });
  }

  List<PersonSummary> _people = [];
  bool _isLoading = true;

  List<PersonSummary> get people => _people;
  bool get isLoading => _isLoading;

  void add(PersonSummary person) {
    _people.add(person);
    _people.sort((p1, p2) => p1.name.compareTo(p2.name));
    DBProvider.db.insertPerson(person);
    notifyListeners();
  }

  void getPeople() async{
    _people = await DBProvider.db.getAllPeople();
    notifyListeners();
  }

  void addAll(List<PersonSummary> persons) {
    _people.addAll(persons);
    persons.forEach((person) => DBProvider.db.insertPerson(person));
    notifyListeners();
  }

  void remove(PersonSummary person) {
    _people.remove(person);
    DBProvider.db.deletePerson(person.id);
    notifyListeners();
  }

  void clear() {
    _people.clear();
    DBProvider.db.deleteAllPeople();
    notifyListeners();
  }

  void setLoading(bool loadingState){
    _isLoading = loadingState;
    notifyListeners();
  }

  bool contains(int id){
    PersonSummary result = _people.firstWhere((p) => p.id == id, orElse: () => null);
    return result != null;
  }
}
