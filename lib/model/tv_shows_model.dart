import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/tmdb/tv_show.dart';
import 'package:filmographer/db/db_provider.dart';

class TvShowsModel extends Model {
  TvShowsModel() {
    DBProvider.db.getAllFavouriteTvShows().then((tvShows) {
      _favouriteTvShows = tvShows;
      _isLoading = false;
      notifyListeners();
    });

    DBProvider.db.getAllSeenTvShows().then((tvShows) {
      _seenTvShows = tvShows;
      _isLoading = false;
      notifyListeners();
    });
  }

  List<TvShow> _favouriteTvShows = [];
  List<TvShow> _seenTvShows = [];
  bool _isLoading = true;

  List<TvShow> get favouriteTvShows => _favouriteTvShows;
  List<TvShow> get seenTvShows => _seenTvShows;
  bool get isLoading => _isLoading;

  void addToFavourite(TvShow movie) {
    _favouriteTvShows.add(movie);
    DBProvider.db.insertFavouriteTvShow(movie);
    notifyListeners();
  }

  void addToSeen(TvShow movie) {
    _seenTvShows.add(movie);
    DBProvider.db.insertSeenTvShow(movie);
    notifyListeners();
  }

  void getFavouriteTvShows() async{
    _favouriteTvShows = await DBProvider.db.getAllFavouriteTvShows();
    notifyListeners();
  }

  void getSeenTvShows() async{
    _seenTvShows = await DBProvider.db.getAllSeenTvShows();
    notifyListeners();
  }

  void addAll(List<TvShow> tvShows) {
    _favouriteTvShows.addAll(tvShows);
    tvShows.forEach((movie) => DBProvider.db.insertFavouriteTvShow(movie));
    notifyListeners();
  }

  void removeFromFavourites(TvShow movie) {
    _favouriteTvShows.remove(movie);
    DBProvider.db.deleteFavouriteTvShow(movie.id);
    notifyListeners();
  }

  void removeFromSeen(TvShow movie) {
    _seenTvShows.remove(movie);
    DBProvider.db.deleteSeenTvShow(movie.id);
    notifyListeners();
  }

  void clearFavourite() {
    _favouriteTvShows.clear();
    DBProvider.db.deleteAllFavourite();
    notifyListeners();
  }

  void clearSeen() {
    _seenTvShows.clear();
    DBProvider.db.deleteAllSeen();
    notifyListeners();
  }

  void setLoading(bool loadingState){
    _isLoading = loadingState;
    notifyListeners();
  }
}
