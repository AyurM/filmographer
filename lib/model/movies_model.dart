import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/tmdb/movie.dart';
import 'package:filmographer/db/db_provider.dart';

class MoviesModel extends Model {
  MoviesModel() {
    DBProvider.db.getAllFavouriteMovies().then((movies) {
      _favouriteMovies = movies;
      _isLoading = false;
      notifyListeners();
    });

    DBProvider.db.getAllSeenMovies().then((movies) {
      _seenMovies = movies;
      _isLoading = false;
      notifyListeners();
    });
  }

  List<Movie> _favouriteMovies = [];
  List<Movie> _seenMovies = [];
  bool _isLoading = true;

  List<Movie> get favouriteMovies => _favouriteMovies;
  List<Movie> get seenMovies => _seenMovies;
  bool get isLoading => _isLoading;

  void addToFavourite(Movie movie) {
    _favouriteMovies.add(movie);
    DBProvider.db.insertFavouriteMovie(movie);
    notifyListeners();
  }

  void addToSeen(Movie movie) {
    _seenMovies.add(movie);
    DBProvider.db.insertSeenMovie(movie);
    notifyListeners();
  }

  void getFavouriteMovies() async{
    _favouriteMovies = await DBProvider.db.getAllFavouriteMovies();
    notifyListeners();
  }

  void getSeenMovies() async{
    _seenMovies = await DBProvider.db.getAllSeenMovies();
    notifyListeners();
  }

  void addAll(List<Movie> movies) {
    _favouriteMovies.addAll(movies);
    movies.forEach((movie) => DBProvider.db.insertFavouriteMovie(movie));
    notifyListeners();
  }

  void removeFromFavourites(Movie movie) {
    _favouriteMovies.remove(movie);
    DBProvider.db.deleteFavouriteMovie(movie.id);
    notifyListeners();
  }

  void removeFromSeen(Movie movie) {
    _seenMovies.remove(movie);
    DBProvider.db.deleteSeenMovie(movie.id);
    notifyListeners();
  }

  void clearFavourite() {
    _favouriteMovies.clear();
    DBProvider.db.deleteAllFavourite();
    notifyListeners();
  }

  void clearSeen() {
    _seenMovies.clear();
    DBProvider.db.deleteAllSeen();
    notifyListeners();
  }

  void setLoading(bool loadingState){
    _isLoading = loadingState;
    notifyListeners();
  }
}
