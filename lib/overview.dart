import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'strings.dart';
import 'colors.dart';

//Описание фильма/сериала или биография
class Overview extends StatefulWidget {
  final String title;
  final String overview;

  Overview({Key key, this.title, this.overview}) : super(key: key);

  @override
  _OverviewState createState() => _OverviewState();
}

class _OverviewState extends State<Overview> {
  bool _isOverviewExpanded = false;

  @override
  Widget build(BuildContext context) {
    if (widget.overview == null || widget.overview.isEmpty) return SizedBox();

    return Padding(
        padding: EdgeInsets.all(Utils.screenAwareHeight(8.0, context)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildTitle(),
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[_buildOverview(), _buildExpandButton()],
            )
          ],
        ));
  }

  Widget _buildTitle() {
    return Padding(
      padding: EdgeInsets.only(bottom: Utils.screenAwareHeight(4.0, context)),
      child: Text(widget.title,
          style: TextStyle(
              fontSize: Utils.screenAwareHeight(18.0, context),
              color: AppColors.textColor,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget _buildOverview() {
    return Text(widget.overview,
        maxLines: _isOverviewExpanded ? null : 3,
        overflow:
            _isOverviewExpanded ? TextOverflow.clip : TextOverflow.ellipsis,
        textAlign: TextAlign.justify,
        style: TextStyle(
            fontSize: Utils.screenAwareHeight(16.0, context),
            color: AppColors.textColor));
  }

  Widget _buildExpandButton() {
    return GestureDetector(
      child: Text(
          _isOverviewExpanded ? Strings.lessOverview : Strings.moreOverview,
          style: TextStyle(
              fontSize: Utils.screenAwareHeight(14.0, context),
              fontWeight: FontWeight.bold,
              color: AppColors.accentColor)),
      onTap: () {
        setState(() {
          _isOverviewExpanded = !_isOverviewExpanded;
        });
      },
    );
  }
}
