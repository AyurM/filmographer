import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'tmdb/video.dart';
import 'tmdb/tmdb_parameters.dart';
import 'colors.dart';
import 'strings.dart';

class TrailersTab extends StatelessWidget {
  TrailersTab({Key key, this.videos}) : super(key: key);

  final List<Video> videos;

  @override
  Widget build(BuildContext context) {
    if (videos.isEmpty) return SizedBox();

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(Strings.trailers,
                  style: TextStyle(
                      fontSize: 18,
                      color: AppColors.textColor,
                      fontWeight: FontWeight.bold))),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width / 1.333,
            child: DefaultTabController(
                length: videos.length,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    TabBarView(
                      children: videos.map((video) {
                        return GestureDetector(
                            onTap: () => _onTrailerClick(video),
                            child: _buildTrailerPreview(context, video));
                      }).toList(),
                    ),
                    videos.length > 1
                        ? Positioned(
                            bottom: 12,
                            child: TabPageSelector(
                                color: Colors.white, indicatorSize: 6),
                          )
                        : SizedBox()
                  ],
                )),
          )
        ],
      ),
    );
  }

  Widget _buildTrailerPreview(BuildContext context, Video video) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        CachedNetworkImage(
            errorWidget: (context, url, error) =>
                Image.asset("images/poster_placeholder.png"),
            imageUrl: _getYouTubeThumbnailUrl(video)),
        Positioned(
          top: (MediaQuery.of(context).size.width / 1.333) / 2 - 48,
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
            size: 96,
          ),
        ),
        Container(
          padding: const EdgeInsets.all(4),
          color: AppColors.transparentBlack,
          width: double.infinity,
          height: 28,
          child: Text(video.name,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.white)),
        )
      ],
    );
  }

  String _getYouTubeThumbnailUrl(Video video) {
    return TmdbParams.youTubeThumbnailUrl +
        video.key +
        TmdbParams.youTubeThumbnailQuality;
  }

  void _onTrailerClick(Video video) {
    FlutterYoutube.playYoutubeVideoById(
        apiKey: TmdbParams.youTubeApiKeyValue,
        videoId: video.key,
        autoPlay: false,
        fullScreen: true);
  }
}
