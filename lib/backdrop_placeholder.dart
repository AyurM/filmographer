import 'package:flutter/material.dart';

class BackdropPlaceholder extends StatelessWidget {
  final String _loadingPicPath = "images/loading.gif";

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.width / 1.78,
        child: Center(
          child: Image.asset(_loadingPicPath),
        )
//      DecoratedBox(
//          decoration: BoxDecoration(
//            gradient: LinearGradient(
//                colors: [Colors.white, Colors.black12],
//                begin: FractionalOffset.bottomLeft,
//                end: FractionalOffset.topRight),
//          )),
        );
  }
}
