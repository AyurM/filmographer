import 'dart:math';

import 'package:filmographer/connection_error.dart';
import 'package:filmographer/external_links.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/tmdb/movie.dart';
import 'package:filmographer/tmdb/tmdb_video.dart';
import 'package:filmographer/tmdb/movie_details.dart';
import 'package:filmographer/tmdb/backdrop.dart';
import 'package:filmographer/tmdb/tmdb_parameters.dart';
import 'package:filmographer/model/movies_model.dart';
import 'package:filmographer/overview.dart';
import 'package:filmographer/gradient_box.dart';
import 'package:filmographer/trailers_tab.dart';
import 'package:filmographer/loading_placeholder.dart';
import 'package:filmographer/backdrop_placeholder.dart';
import 'package:filmographer/title_box.dart';
import 'package:filmographer/poster_list.dart';
import 'package:filmographer/backrdops_tab.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/colors.dart';
import 'package:filmographer/people/cast_list.dart';

//Страница с детальными сведениями о фильме (фото, описание, актеры, режиссер,
// рекомендации, трейлеры)
class MovieInfo extends StatefulWidget {
  final TmdbVideo video;

  MovieInfo({Key key, @required this.video}) : super(key: key);

  @override
  _MovieInfoState createState() => _MovieInfoState();
}

class _MovieInfoState extends State<MovieInfo> {
  final int _maxBackdrops = 7;
  MovieDetails _details;
  bool _isError = false;

  @override
  Widget build(BuildContext context) {
    if (!_isError && _details == null) _loadDetails();

    if (_isError) return _buildErrorUI();

    return Scaffold(
        appBar: AppBar(title: Text(widget.video.title), centerTitle: true),
        body: _buildScaffoldBody());
  }

  Widget _buildScaffoldBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTopStack(),
          _buildTagLine(),
          _buildInfoLabels(),
          _buildYearAndGenres(),
          Overview(title: Strings.aboutMovie, overview: widget.video.overview),
          _buildCast(),
          _buildDirector(),
          _buildRecommendations(),
          _buildTrailersTab(),
          _buildLinks()
        ],
      ),
    );
  }

  Widget _buildTopStack() {
    return ScopedModelDescendant<MoviesModel>(builder: (context, child, model) {
      return Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildBackdropTabs(),
              _buildButtonsRow(model),
              Divider(height: 0),
              TitleBox(
                  title: widget.video.title,
                  subtitle: widget.video.originalTitle)
            ],
          )
        ],
      );
    });
  }

  Widget _buildBackdropTabs() {
    if (_details == null) return BackdropPlaceholder();

    List<Backdrop> backdrops = _details.images.backdrops
        .take(min(_maxBackdrops, _details.images.backdrops.length))
        .toList();

    return BackdropsTab(backdropsList: backdrops);
  }

  Widget _buildButtonsRow(MoviesModel model) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Utils.getFavouriteIcon(
                  model.favouriteMovies.contains(widget.video as Movie)),
              Padding(
                padding:
                    EdgeInsets.only(left: Utils.screenAwareWidth(8.0, context)),
                child: Text(
                    model.favouriteMovies.contains(widget.video as Movie)
                        ? Strings.inFavourites
                        : Strings.addFavourites),
              )
            ],
          ),
          onPressed: () => _onLikeClick(model),
        ),
        FlatButton(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    right: Utils.screenAwareWidth(8.0, context)),
                child: Text(Strings.wantToWatch),
              ),
              Utils.getToWatchIcon(
                  model.seenMovies.contains(widget.video as Movie)),
            ],
          ),
          onPressed: () => _onToWatchClick(model),
        )
      ],
    );
  }

  Widget _buildInfoLabels() {
    final List<Widget> labels = [
      GradientBox(
          iconData: Icons.star,
          text: widget.video.voteAverage.toStringAsPrecision(2),
          gradientColors: Utils.getRatingColor(widget.video.voteAverage)),
      GradientBox(
        iconData: Icons.access_time,
        text: _details == null ? "---" : "${_details.runtime} мин.",
        gradientColors: [Colors.lightBlue, Colors.indigo],
      ),
      GradientBox(
          iconData: Icons.people_outline,
          text:
              "${widget.video.voteCount.toString()} ${StringUtils.getVotesPlural(widget.video.voteCount)}",
          gradientColors: [Colors.grey, Colors.blueGrey]),
    ];

    return Padding(
        padding: EdgeInsets.symmetric(
            vertical: 0, horizontal: Utils.screenAwareWidth(8.0, context)),
        child: Container(
            height: Utils.screenAwareHeight(52.0, context),
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: labels,
                ))));
  }

  Widget _buildTagLine() {
    if (_details == null) return SizedBox();
    if (_details.tagline == null || _details.tagline.isEmpty) return SizedBox();
    return Center(
      child: Padding(
          padding: EdgeInsets.fromLTRB(
              Utils.screenAwareWidth(8.0, context),
              0,
              Utils.screenAwareWidth(8.0, context),
              Utils.screenAwareWidth(8.0, context)),
          child: Text(_details.tagline, textAlign: TextAlign.center)),
    );
  }

  Widget _buildYearAndGenres() {
    String genres =
        _details == null ? "" : StringUtils.getGenresNames(_details.genres);
    return Padding(
      padding: EdgeInsets.all(Utils.screenAwareWidth(8.0, context)),
      child: Text("${widget.video.releaseDate.substring(0, 4)}, $genres",
          style: TextStyle(fontSize: Utils.screenAwareHeight(14.0, context), color: AppColors.textColor)),
    );
  }

  Widget _buildCast() {
    if (_details == null) return LoadingPlaceholder(title: Strings.cast);

    //отфильтровать актеров без картинки профиля
    return CastList(
        persons:
            _details.credits.cast.where((c) => c.profilePath != null).toList(),
        title: Strings.cast);
  }

  Widget _buildDirector() {
    if (_details == null) return LoadingPlaceholder(title: Strings.director);

    return CastList(
        persons:
            _details.credits.crew.where((c) => c.job == "Director").toList(),
        title: Strings.director);
  }

  Widget _buildRecommendations() {
    if (_details == null)
      return LoadingPlaceholder(title: Strings.recommendations);

    return PosterList(
        title: Strings.recommendations,
        videos: _details.recommendations.results);
  }

  Widget _buildTrailersTab() {
    if (_details == null) return LoadingPlaceholder(title: Strings.trailers);

    return TrailersTab(
      videos: _details.videos.results
          .where((v) =>
              v.site == TmdbParams.youTubeValue &&
              v.type == TmdbParams.typeTrailer)
          .toList(),
    );
  }

  Widget _buildLinks() {
    if (_details == null) return LoadingPlaceholder(title: Strings.links);

    return ExternalLinks(ids: _details.externalIds, title: widget.video.title);
  }

  Widget _buildErrorUI() {
    return Scaffold(
        appBar: AppBar(title: Text(widget.video.title), centerTitle: true),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ConnectionError(onRetry: () {
                setState(() {
                  _isError = false;
                });
              }),
              _buildInfoLabels(),
              _buildYearAndGenres(),
              Overview(
                  title: Strings.aboutMovie, overview: widget.video.overview),
            ],
          ),
        ));
  }

  void _onLikeClick(MoviesModel model) {
    Movie movie = widget.video as Movie;
    if (!model.favouriteMovies.contains(movie))
      model.addToFavourite(movie);
    else
      model.removeFromFavourites(movie);
  }

  void _onToWatchClick(MoviesModel model) {
    Movie movie = widget.video as Movie;
    if (!model.seenMovies.contains(movie))
      model.addToSeen(movie);
    else
      model.removeFromSeen(movie);
  }

  void _loadDetails() {
    Utils.getMovieDetails(Utils.buildMovieDetailsUrl(widget.video.id))
        .then((results) {
      setState(() {
        _details = results;
      });
    }).catchError((e) => _onError(e));
  }

  void _onError(Exception e) {
    setState(() {
      _isError = true;
    });
  }
}
