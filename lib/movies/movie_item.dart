import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/movies_model.dart';
import 'package:filmographer/model/tv_shows_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/tmdb/tmdb_video.dart';
import 'package:filmographer/tmdb/movie.dart';
import 'package:filmographer/tmdb/tv_show.dart';
import 'package:filmographer/movies/movie_info.dart';
import 'package:filmographer/tv_shows/tv_show_info.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/colors.dart';

//Краткая карточка фильма/сериала (изображение, год, жанр, оценка)
class MovieItem extends StatelessWidget {
  MovieItem({Key key, @required this.video}) : super(key: key);

  final TmdbVideo video;
  final String _placeholderMoviePath = "images/poster_placeholder.png";
  final String _placeholderTvPath = "images/3.0x/episode_placeholder.png";

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
        elevation: 2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[_buildPosterStack(), _buildButtons(context)],
        ));
  }

  Widget _buildPosterStack() {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        _buildPoster(),
        _buildInfo(),
        _buildRating()
      ],
    );
  }

  Widget _buildPoster() {
    String imageUrl = video.backdropPath;
    return ClipRRect(
        borderRadius: BorderRadius.circular(2),
        child: CachedNetworkImage(
            width: double.infinity,
            fit: BoxFit.cover,
            placeholder: (context, string) => _buildPlaceholder(),
            errorWidget: (context, url, error) => _buildPlaceholder(),
            imageUrl: imageUrl == null
                ? _getPlaceholderPath()
                : StringUtils.getHighResPicUrl(imageUrl)));
  }

  Widget _buildInfo() {
    return Container(
      padding: const EdgeInsets.all(8),
      width: double.infinity,
      color: AppColors.transparentColor,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            video.title,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.start,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
          ),
          Text(_getYearAndGenres(),
              style: TextStyle(fontSize: 14, color: Colors.white)),
        ],
      ),
    );
  }

  Widget _buildRating() {
    return Positioned(
        top: 4,
        right: -20,
        child: Chip(
          padding: const EdgeInsets.all(8),
          label: Text(
            "${video.voteAverage.toStringAsPrecision(2)}   ", //при поиске по ключевым словам оценки в результатах имеют 2 знака после запятой
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          elevation: 2,
          backgroundColor: Utils.getRatingColor(video.voteAverage)[1],
        ));
  }

  Widget _buildButtons(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _buildLikeButton(),
              SizedBox(width: 8.0),
              _buildToWatchButton(),
            ],
          ),
          FlatButton(
              onPressed: () => _onDetailsClick(context),
              child: _buildButtonText(Strings.detailsCaps)),
        ]);
  }

  Widget _buildLikeButton() {
    if (video is Movie)
      return ScopedModelDescendant<MoviesModel>(
          builder: (context, child, model) {
        return IconButton(
            tooltip: Strings.addFavourites,
            icon: Utils.getFavouriteIcon(
                model.favouriteMovies.contains(video as Movie)),
            onPressed: () => _onLikeMovieClick(model));
      });
    else
      return ScopedModelDescendant<TvShowsModel>(
          builder: (context, child, model) {
        return IconButton(
            tooltip: Strings.addFavourites,
            icon: Utils.getFavouriteIcon(
                model.favouriteTvShows.contains(video as TvShow)),
            onPressed: () => _onLikeTvShowClick(model));
      });
  }

  Widget _buildToWatchButton() {
    if (video is Movie)
      return ScopedModelDescendant<MoviesModel>(
          builder: (context, child, model) {
        return IconButton(
            tooltip: Strings.wantToWatch,
            icon:
                Utils.getToWatchIcon(model.seenMovies.contains(video as Movie)),
            onPressed: () => _onToWatchMovieClick(model));
      });
    else
      return ScopedModelDescendant<TvShowsModel>(
          builder: (context, child, model) {
        return IconButton(
            tooltip: Strings.wantToWatch,
            icon: Utils.getToWatchIcon(
                model.seenTvShows.contains(video as TvShow)),
            onPressed: () => _onToWatchTvShowClick(model));
      });
  }

  void _onDetailsClick(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => video is TvShow
                ? TvShowInfo(video: video)
                : MovieInfo(video: video)));
  }

  void _onLikeMovieClick(MoviesModel model) {
    Movie movie = video as Movie;
    if (!model.favouriteMovies.contains(movie))
      model.addToFavourite(movie);
    else
      model.removeFromFavourites(movie);
  }

  void _onLikeTvShowClick(TvShowsModel model) {
    TvShow tvShow = video as TvShow;
    if (!model.favouriteTvShows.contains(tvShow))
      model.addToFavourite(tvShow);
    else
      model.removeFromFavourites(tvShow);
  }

  void _onToWatchMovieClick(MoviesModel model) {
    Movie movie = video as Movie;
    if (!model.seenMovies.contains(movie))
      model.addToSeen(movie);
    else
      model.removeFromSeen(movie);
  }

  void _onToWatchTvShowClick(TvShowsModel model) {
    TvShow tvShow = video as TvShow;
    if (!model.seenTvShows.contains(tvShow))
      model.addToSeen(tvShow);
    else
      model.removeFromSeen(tvShow);
  }

  Widget _buildButtonText(String text) {
    return Text(text, style: (TextStyle(color: AppColors.accentColor)));
  }

  String _getYearAndGenres() {
    String releaseDate =
        (video.releaseDate == null || video.releaseDate.isEmpty)
            ? Strings.unknown
            : video.releaseDate.substring(0, 4);
    return "$releaseDate, ${StringUtils.getGenresNamesFromIds(video.genreIds, video is Movie)}";
  }

  Widget _buildPlaceholder() {
    return Image.asset(_getPlaceholderPath());
  }

  String _getPlaceholderPath() {
    if (video is Movie)
      return _placeholderMoviePath;
    else
      return _placeholderTvPath;
  }
}
