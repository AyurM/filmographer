import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/movies/favourite_movies_screen.dart';
import 'package:filmographer/movies/seen_movies_screen.dart';
import 'package:filmographer/model/movies_model.dart';
import 'package:filmographer/strings.dart';

//Экран "Фильмы"
class MoviesScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final _movieTabs = <Tab>[
      Tab(text: Strings.favourites),
      Tab(text: Strings.wantToWatch),
    ];

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
            flexibleSpace: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [TabBar(tabs: _movieTabs)],
        )),
        body: ScopedModelDescendant<MoviesModel>(
          builder: (context, child, model){
            return TabBarView(children: <Widget>[
              FavouriteMoviesScreen(),
              SeenMoviesScreen(),
            ]);
          }
        ),
      ),
    );
  }
}
