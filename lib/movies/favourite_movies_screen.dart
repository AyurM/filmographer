import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/movies_model.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/poster_flipper.dart';

class FavouriteMoviesScreen extends StatefulWidget {
  @override
  _FavouriteMoviesScreenState createState() => _FavouriteMoviesScreenState();
}

class _FavouriteMoviesScreenState extends State<FavouriteMoviesScreen> {
  final String _loadingImagePath = "images/loading.gif";
  final String _noElementsImagePath = "images/cinema.png";

  @override
  void initState() {
    super.initState();
    ScopedModel.of<MoviesModel>(context).getFavouriteMovies();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MoviesModel>(builder: (context, child, model) {
      if (model.isLoading) return _buildLoadingUI();

      if (model.favouriteMovies.isEmpty) return _buildNoElementsUI();

//      return PosterCarousel(
//          movies: model.favouriteMovies,
//          height: MediaQuery.of(context).size.height * 0.48,
//          widthPercent: 0.15);

        return PosterFlipper(
          movies: model.favouriteMovies.reversed.toList(), screenPercent: 0.75);
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingImagePath));
  }

  Widget _buildNoElementsUI() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noElementsImagePath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.noElements,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
