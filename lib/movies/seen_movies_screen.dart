import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/movies_model.dart';
import 'package:filmographer/movies/movie_item.dart';
import 'package:filmographer/strings.dart';

class SeenMoviesScreen extends StatefulWidget {
  @override
  _SeenMoviesScreenState createState() => _SeenMoviesScreenState();
}

class _SeenMoviesScreenState extends State<SeenMoviesScreen> {
  final String _loadingImagePath = "images/loading.gif";
  final String _noElementsImagePath = "images/cinema.png";

  @override
  void initState() {
    super.initState();
    ScopedModel.of<MoviesModel>(context).getSeenMovies();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MoviesModel>(builder: (context, child, model) {
      if (model.isLoading) return _buildLoadingUI();

      if (model.seenMovies.isEmpty) return _buildNoElementsUI();

      return ListView.builder(
          itemCount: model.seenMovies.length,
          itemBuilder: (BuildContext context, int i) {
            return MovieItem(video: model.seenMovies[i]);
          });
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingImagePath));
  }

  Widget _buildNoElementsUI() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noElementsImagePath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.noElements,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}