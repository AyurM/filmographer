import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'tmdb/tmdb_parameters.dart';
import 'tmdb/movie_details.dart';
import 'tmdb/tv_show_details.dart';
import 'tmdb/tv_season_details.dart';
import 'tmdb/discover_results.dart';
import 'tmdb/discover_tv_results.dart';
import 'tmdb/discover_people_result.dart';
import 'tmdb/cast_details.dart';
import 'tmdb/image_results.dart';
import 'package:http/http.dart' as http;

class Utils {
  static const double _baseHeight = 640.0;
  static const double _baseWidth = 360.0;
  static const double _baseAspect = 0.5625;


  static double screenAwareHeight(double height, BuildContext context){
    return height * MediaQuery.of(context).size.width / _baseWidth;
  }

  static double screenAwareWidth(double width, BuildContext context){
    return width * MediaQuery.of(context).size.width / _baseWidth;
  }

  static List<Color> getRatingColor(double rating) {
    if (rating == 0)
      return [Colors.black12, Colors.grey];
    else if (rating > 0 && rating < 4.0)
      return [Colors.redAccent, Colors.red];
    else if (rating >= 4.0 && rating < 6.0)
      return [Colors.orangeAccent, Colors.orange];
    else if (rating >= 6.0 && rating < 7.0)
      return [Colors.amber, Colors.lime];
    else
      return [Colors.lightGreen, Colors.green];
  }

  static Widget getFavouriteIcon(bool isFavourite) {
    return isFavourite
        ? Icon(
            Icons.favorite,
            color: Colors.red,
          )
        : Icon(
            Icons.favorite_border,
            color: Colors.grey,
          );
  }

  static Widget getToWatchIcon(bool isInToWatchList) {
    return isInToWatchList
        ? Icon(
            FontAwesomeIcons.eye,
            color: Colors.blue,
          )
        : Icon(
            FontAwesomeIcons.eye,
            color: Colors.grey,
          );
  }

  static String buildDiscoverUrl(String genre, int fromYear, int toYear,
      double minRating, bool isMovie, int page) {
    String releaseDateLte;
    DateTime dateTime = DateTime.now();
    if (toYear == dateTime.year)
      releaseDateLte = toYear.toString() +
          "-" +
          dateTime.month.toString() +
          "-" +
          dateTime.day.toString();
    else
      releaseDateLte = toYear.toString() + "-12-31";

    return TmdbParams.baseUrl +
        (isMovie ? TmdbParams.discoverMovieUrl : TmdbParams.discoverTvUrl) +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.sortBy +
        "=popularity.desc&include_adult=false&include_video=false&page=" +
        page.toString() +
        "&" +
        _getWithGenresValue(genre, isMovie) +
        "&" +
        TmdbParams.voteCountGte +
        "=" +
        (isMovie
            ? TmdbParams.defaultMovieVoteCountGte
            : TmdbParams.defaultTvVoteCountGte) +
        "&" +
        TmdbParams.voteAverageGte +
        "=" +
        minRating.toString() +
        "&" +
        (isMovie
            ? TmdbParams.primaryReleaseDateGte
            : TmdbParams.firstAirDateGte) +
        "=" +
        fromYear.toString() +
        "-01-01&" +
        (isMovie
            ? TmdbParams.primaryReleaseDateLte
            : TmdbParams.firstAirDateLte) +
        "=" +
        releaseDateLte;
  }

  static String buildImagesUrl(int id) {
    return TmdbParams.baseUrl +
        TmdbParams.movieUrl +
        id.toString() +
        TmdbParams.imagesUrl +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU" +
        TmdbParams.imageLanguage;
  }

  static String buildMovieDetailsUrl(int id) {
    return TmdbParams.baseUrl +
        TmdbParams.movieUrl +
        id.toString() +
        "?" +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.appendToResponse +
        TmdbParams.imageLanguage;
  }

  static String buildTvDetailsUrl(int id) {
    return TmdbParams.baseUrl +
        TmdbParams.tvUrl +
        id.toString() +
        "?" +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.appendToResponse +
        TmdbParams.imageLanguage;
  }

  static String buildTvSeasonDetailsUrl(int tvShowId, int seasonNumber) {
    return TmdbParams.baseUrl +
        TmdbParams.tvUrl +
        tvShowId.toString() +
        TmdbParams.seasonUrl +
        seasonNumber.toString() +
        "?" +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU";
  }

  static String buildTvEpisodeStillsUrl(
      int tvShowId, int seasonNumber, int episodeNumber) {
    return TmdbParams.baseUrl +
        TmdbParams.tvUrl +
        tvShowId.toString() +
        TmdbParams.seasonUrl +
        seasonNumber.toString() +
        TmdbParams.episodeUrl +
        episodeNumber.toString() +
        TmdbParams.imagesUrl +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        TmdbParams.imageLanguage;
  }

  static String buildTrendingUrl(String mediaType, String timeWindow) {
    return TmdbParams.baseUrl +
        TmdbParams.trendingUrl +
        "$mediaType/$timeWindow?" +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU";
  }

  static String buildPopularPersonUrl() {
    return TmdbParams.baseUrl +
        TmdbParams.personUrl +
        TmdbParams.popularUrl +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU";
  }

  static String buildPersonDetailsUrl(int id) {
    return TmdbParams.baseUrl +
        TmdbParams.personUrl +
        "${id.toString()}?" +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.appendToCastResponse +
        TmdbParams.imageLanguage;
  }

  static String buildSearchUrl(String query, bool isMovie, int page) {
    return TmdbParams.baseUrl +
        (isMovie ? TmdbParams.searchMovieUrl : TmdbParams.searchTvUrl) +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.query +
        "=" +
        query +
        "&page=" +
        page.toString();
  }

  static String buildSearchPeopleUrl(String query, int page) {
    return TmdbParams.baseUrl +
        TmdbParams.searchPersonUrl +
        TmdbParams.apiKey +
        "=" +
        TmdbParams.apiKeyValue +
        "&" +
        TmdbParams.language +
        "=ru-RU&" +
        TmdbParams.query +
        "=" +
        query +
        "&page=" +
        page.toString() +
        "&include_adult=false";
  }

  static Future<DiscoverResults> getDiscoverResults(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return DiscoverResults.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to discover movies");
  }

  static Future<DiscoverTvResults> getDiscoverTvResults(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return DiscoverTvResults.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to discover TV shows");
  }

  static Future<ImageResults> getImageResults(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return ImageResults.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request images");
  }

  static Future<MovieDetails> getMovieDetails(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return MovieDetails.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request movie details");
  }

  static Future<DiscoverPeopleResults> getDiscoverPeopleResults(
      String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return DiscoverPeopleResults.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request people results");
  }

  static Future<TvShowDetails> getTvShowDetails(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return TvShowDetails.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request TV show details");
  }

  static Future<TvSeasonDetails> getTvSeasonDetails(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return TvSeasonDetails.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request TV season details");
  }

  static Future<CastDetails> getCastDetails(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200)
      return CastDetails.fromJson(json.decode(response.body));
    else
      throw Exception("Failed to request cast details");
  }

  static String _getWithGenresValue(String genre, bool isMovie) {
    int genreId = isMovie
        ? TmdbParams.moviesGenreIds[TmdbParams.movieGenres.indexOf(genre)]
        : TmdbParams.tvShowGenreIds[TmdbParams.tvShowGenres.indexOf(genre)];
    //указан любой жанр
    if (genreId == 0) return "";

    return TmdbParams.withGenres + "=" + genreId.toString();
  }
}
