import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'tmdb/tmdb_video.dart';
import 'tmdb/movie.dart';
import 'utils.dart';
import 'string_utils.dart';

//Постер к фильму/сериалу + название + оценка
class PreviewCard extends StatelessWidget {
  PreviewCard({
    Key key,
    @required this.video,
    this.cardWidth = 140.0,
  }) : super(key: key);

  final double cardWidth;
  final TmdbVideo video;

  final double _aspectRatio = 0.667;
  final String _moviePlaceholderPath = "images/movie_placeholder.png";
  final String _tvPlaceholderPath = "images/tv_placeholder.png";

  @override
  Widget build(BuildContext context) {
    return Container(
        width: Utils.screenAwareWidth(cardWidth + 16, context),
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    right: Utils.screenAwareWidth(12.0, context),
                    top: Utils.screenAwareHeight(10.0, context)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      decoration: _buildShadow(context),
                      child: _buildPosterImage(context),
                    ),
                    _buildTitle(context)
                  ],
                )),
            _buildRatingChip(context)
          ],
        ));
  }

  Widget _buildPosterImage(BuildContext context) {
    String imageUrl = video.posterPath == null
        ? (video is Movie ? _moviePlaceholderPath : _tvPlaceholderPath)
        : StringUtils.getSmallPicUrl(video.posterPath);
    return ClipRRect(
        borderRadius:
            BorderRadius.circular(Utils.screenAwareHeight(4.0, context)),
        child: CachedNetworkImage(
            width: Utils.screenAwareWidth(cardWidth, context),
            height: Utils.screenAwareWidth(cardWidth, context) / _aspectRatio,
            fit: BoxFit.fitHeight,
            placeholder: (context, string) => _buildPlaceholder(context),
            errorWidget: (context, url, error) => _buildPlaceholder(context),
            imageUrl: imageUrl));
  }

  Widget _buildPlaceholder(BuildContext context) {
    return ClipRRect(
        borderRadius:
            BorderRadius.circular(Utils.screenAwareHeight(4.0, context)),
        child: Container(
          width: Utils.screenAwareWidth(cardWidth, context),
          height: Utils.screenAwareWidth(cardWidth, context) / _aspectRatio,
          child: Image.asset(
              video is Movie ? _moviePlaceholderPath : _tvPlaceholderPath),
        ));
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: Utils.screenAwareHeight(4.0, context),
          left: Utils.screenAwareWidth(4.0, context),
          right: Utils.screenAwareWidth(4.0, context)),
      child: Text(video.title,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
          textAlign: TextAlign.center),
    );
  }

  Widget _buildRatingChip(BuildContext context) {
    return Positioned(
        top: Utils.screenAwareHeight(8.0, context),
        right: Utils.screenAwareWidth(8.0, context),
        child: Chip(
          backgroundColor: Utils.getRatingColor(video.voteAverage)[1],
          label: Text(
            video.voteAverage.toStringAsPrecision(2),
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ));
  }

  Decoration _buildShadow(BuildContext context) {
    return BoxDecoration(
        borderRadius:
            BorderRadius.circular(Utils.screenAwareHeight(4.0, context)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              blurRadius: 2,
              offset: Offset(Utils.screenAwareWidth(3.0, context),
                  Utils.screenAwareHeight(3.0, context)))
        ]);
  }
}
