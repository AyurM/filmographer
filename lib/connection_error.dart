import 'package:filmographer/colors.dart';
import 'package:flutter/material.dart';
import 'strings.dart';

class ConnectionError extends StatelessWidget {
  final Function onRetry;

  const ConnectionError({Key key, this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(Strings.errorMessage, style: TextStyle(fontSize: 18)),
            Text(Strings.checkConnection, textAlign: TextAlign.center),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                child: Text(
                  Strings.retry,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                textColor: Colors.white,
                shape: const StadiumBorder(),
                padding: EdgeInsets.all(16),
                color: AppColors.accentColor,
                onPressed: () => onRetry(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
