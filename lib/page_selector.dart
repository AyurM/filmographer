import 'dart:math';
import 'package:flutter/material.dart';
import 'package:filmographer/colors.dart';

class PageSelector extends StatelessWidget {
  final int currentPage;
  final int totalPages;
  final Function onPageSelect;

  const PageSelector(
      {Key key, this.currentPage, this.totalPages, this.onPageSelect})
      : super(key: key);

  final int _maxPages = 5;

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: _buildPageNumbers());
  }

  List<Widget> _buildPageNumbers() {
    List<Widget> result = List<Widget>();
    for (int i = _getStartIndex(); i <= _getEndIndex(); i++) {
      result.add(_buildNumber(i));
    }
    return result;
  }

  int _getStartIndex() {
    if (currentPage > totalPages - 2) return max(totalPages - _maxPages + 1, 1);

    return max(currentPage - 2, 1);
  }

  int _getEndIndex() {
    if (currentPage < _maxPages - 2) return min(totalPages, _maxPages);

    return min(currentPage + 2, totalPages);
  }

  Widget _buildNumber(int i) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: GestureDetector(
        onTap: () => onPageSelect(i),
        child: Container(
          padding: i == currentPage ? const EdgeInsets.all(4.0) : null,
          decoration: i == currentPage
              ? BoxDecoration(
              color: AppColors.primaryDark,
              borderRadius: BorderRadius.circular(4))
              : null,
          child: Text(
            i.toString(),
            style: TextStyle(
                fontSize: 20,
                color: i == currentPage ? Colors.white : AppColors.textColor),
          ),
        ),
      ),
    );
  }
}