import 'tmdb/tmdb_parameters.dart';
import 'tmdb/genre.dart';
import 'strings.dart';

class StringUtils {
  static String getSmallPicUrl(String filePath) {
    if (filePath == null)
      return null;
    else
      return TmdbParams.lowResPosterBaseUrl + filePath;
  }

  static String getHighResPicUrl(String filePath) {
    if (filePath == null)
      return null;
    else
      return TmdbParams.highResPosterBaseUrl + filePath;
  }

  static String getGenresNames(List<Genre> genres) {
    String result = "";
    genres.forEach((genre) => result += genre.name + ", ");
    return result.substring(0, result.length - 2);
  }

  static String getGenreIdsString(List<int> genres) {
    String result = "";
    genres.forEach((genre) => result += (genre.toString() + ', '));
    if (result.isNotEmpty) result = result.substring(0, result.length - 2);
    return result;
  }

  static String getGenresNamesFromIds(List<int> ids, bool isMovie) {
    String result = "";
    List<String> mainGenres =
        isMovie ? TmdbParams.movieGenres : TmdbParams.tvShowGenres;
    List<String> secondaryGenres =
        isMovie ? TmdbParams.tvShowGenres : TmdbParams.movieGenres;
    List<int> mainIdNumbers =
        isMovie ? TmdbParams.moviesGenreIds : TmdbParams.tvShowGenreIds;
    List<int> secondaryIdNumbers =
        isMovie ? TmdbParams.tvShowGenreIds : TmdbParams.moviesGenreIds;

    ids.forEach((id) {
      int index = mainIdNumbers.indexOf(id);
      //id жанров сериалов иногда могут относиться к id жанров для фильмов
      if (index == -1) {
        index = secondaryIdNumbers.indexOf(id);
        result += secondaryGenres[index] + ", ";
      } else
        result += mainGenres[index] + ", ";
    });
    return result.isEmpty ? result : result.substring(0, result.length - 2);
  }

  static String getDateString(DateTime date) {
    String result = "";
    result += date.day.toString() + " ";
    String month;
    switch (date.month) {
      case 1:
        month = "января";
        break;
      case 2:
        month = "февраля";
        break;
      case 3:
        month = "марта";
        break;
      case 4:
        month = "апреля";
        break;
      case 5:
        month = "мая";
        break;
      case 6:
        month = "июня";
        break;
      case 7:
        month = "июля";
        break;
      case 8:
        month = "августа";
        break;
      case 9:
        month = "сентября";
        break;
      case 10:
        month = "октября";
        break;
      case 11:
        month = "ноября";
        break;
      default:
        month = "декабря";
        break;
    }

    result += month + " " + date.year.toString();
    return result;
  }

  static String getYearsPlural(int years) {
    int remainder100 = years % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "лет";

    int remainder = years % 10;
    if (remainder == 1) return "год";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "года";

    return "лет";
  }

  static String getSeasonsPlural(int seasons) {
    int remainder100 = seasons % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "сезонов";

    int remainder = seasons % 10;
    if (remainder == 1) return "сезон";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "сезона";

    return "сезонов";
  }

  static String getEpisodesPlural(int episodes) {
    int remainder100 = episodes % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "серий";

    int remainder = episodes % 10;
    if (remainder == 1) return "серия";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "серии";

    return "серий";
  }

  static String getVotesPlural(int votes) {
    int remainder100 = votes % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "оценок";

    int remainder = votes % 10;
    if (remainder == 1) return "оценка";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "оценки";

    return "оценок";
  }

  static String getMoviesPlural(int totalMovies) {
    int remainder100 = totalMovies % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "фильмов";

    int remainder = totalMovies % 10;
    if (remainder == 1) return "фильм";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "фильма";

    return "фильмов";
  }

  static String getTvShowsPlural(int totalTvShows) {
    int remainder100 = totalTvShows % 100;
    if (remainder100 == 11 ||
        remainder100 == 12 ||
        remainder100 == 13 ||
        remainder100 == 14) return "сериалов";

    int remainder = totalTvShows % 10;
    if (remainder == 1) return "сериал";
    if (remainder == 2 || remainder == 3 || remainder == 4) return "сериала";

    return "сериалов";
  }

  static String getKnownForMoviesTitle(int gender) {
    switch (gender) {
      case 0:
        return Strings.knownForMoviesNeutral;
        break;
      case 1:
        return Strings.knownForMoviesFemale;
        break;
      default:
        return Strings.knownForMoviesMale;
        break;
    }
  }

  static String getKnownForTvShowsTitle(int gender) {
    switch (gender) {
      case 0:
        return Strings.knownForTvShowsNeutral;
        break;
      case 1:
        return Strings.knownForTvShowsFemale;
        break;
      default:
        return Strings.knownForTvShowsMale;
        break;
    }
  }

  static String parseAge(String birthday, String deathday) {
    if (birthday == null) return "";

    String result = "";

    DateTime parsedBirthday = DateTime.parse(birthday);
    result += StringUtils.getDateString(parsedBirthday);
    int age;
    if (deathday == null) {
      age = (DateTime.now().difference(parsedBirthday).inDays / 365).floor();
      result += ", $age ${StringUtils.getYearsPlural(age)}";
    } else {
      DateTime parsedDeathday = DateTime.parse(deathday);
      age = (parsedDeathday.difference(parsedBirthday).inDays / 365).floor();
      result +=
          " - ${StringUtils.getDateString(parsedDeathday)}, $age ${StringUtils.getYearsPlural(age)}";
    }
    return result;
  }
}
