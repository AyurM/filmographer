import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'tmdb/discover_results.dart';
import 'tmdb/discover_tv_results.dart';
import 'package:filmographer/movies/movie_item.dart';
import 'package:filmographer/page_selector.dart';
import 'connection_error.dart';
import 'strings.dart';
import 'colors.dart';
import 'utils.dart';

//Экран с результатами поиска по названию или по жанру
class SearchResults extends StatefulWidget {
  SearchResults(
      {Key key,
      this.fromYear,
      this.toYear,
      this.genre,
      this.minRating,
      this.isMovie,
      this.searchQuery})
      : super(key: key);

  final int fromYear;
  final int toYear;
  final String genre;
  final double minRating;
  final bool isMovie;
  final String searchQuery;

  @override
  _SearchResultsState createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  DiscoverResults _discoverResults;
  DiscoverTvResults _discoverTvResults;
  int _selectedPage = 1;
  bool _isError = false;

  final int _resultsPerPage = 20;
  final String _noResultsPicPath = "images/search.png";
  final String _loadingPicPath = "images/loading.gif";

  @override
  Widget build(BuildContext context) {
    if (!_isError && _isWidgetNull()) _loadSearchResults();

    return Scaffold(
      appBar: AppBar(
          title: Text(
            _getAppBarTitle(),
          ),
          centerTitle: true),
      body: _buildScaffoldBody(),
    );
  }

  Widget _buildScaffoldBody() {
    if (_isError) return _buildErrorUI();

    if (_isWidgetNull()) return _buildLoadingUI();

    if (_isWidgetEmpty()) return _buildEmptyResults();

    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([_buildPageNavigation()]),
        ),
        SliverList(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
          return MovieItem(
              video: widget.isMovie
                  ? _discoverResults.results[index]
                  : _discoverTvResults.results[index]);
        }, childCount: _getResultsCount())),
        SliverList(
          delegate: SliverChildListDelegate([_buildPageNavigation()]),
        )
      ],
    );
  }

  Widget _buildErrorUI() {
    return ConnectionError(onRetry: () {
      setState(() {
        _isError = false;
      });
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingPicPath));
  }

  Widget _buildEmptyResults() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noResultsPicPath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.nothingFound,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPageNavigation() {
    if (_getTotalPages() < 2) return SizedBox();

    return Card(
        margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
        elevation: 2,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _selectedPage == 1
                    ? SizedBox()
                    : GestureDetector(
                        child: Container(
                          width: 48,
                          height: 48,
                          child: Icon(FontAwesomeIcons.angleLeft,
                              color: AppColors.primaryDark, size: 28),
                        ),
                        onTap: () {
                          setState(() {
                            _selectedPage = max(_selectedPage - 1, 1);
                            _discoverResults = null;
                            _discoverTvResults = null;
                          });
                        },
                        onDoubleTap: () {
                          setState(() {
                            _selectedPage = 1;
                            _discoverResults = null;
                            _discoverTvResults = null;
                          });
                        },
                      ),
                Expanded(
                  child: Center(
                    child: PageSelector(
                      currentPage: _selectedPage,
                      totalPages: _getTotalPages(),
                      onPageSelect: (index) => _onPageNumberClick(index),
                    ),
                  ),
                ),
                _selectedPage == _getTotalPages()
                    ? SizedBox()
                    : GestureDetector(
                        child: Container(
                          width: 48,
                          height: 48,
                          child: Icon(FontAwesomeIcons.angleRight,
                              color: AppColors.primaryDark, size: 28),
                        ),
                        onTap: () {
                          setState(() {
                            _selectedPage =
                                min(_selectedPage + 1, _getTotalPages());
                            _discoverResults = null;
                            _discoverTvResults = null;
                          });
                        },
                        onDoubleTap: () {
                          setState(() {
                            _selectedPage = _getTotalPages();
                            _discoverResults = null;
                            _discoverTvResults = null;
                          });
                        },
                      )
              ],
            )));
  }

  void _loadSearchResults() {
    if (widget.isMovie) {
      _loadMovieResults();
    } else {
      _loadTvResults();
    }
  }

  void _loadMovieResults() {
    Utils.getDiscoverResults(_getUrl()).then((results) {
      setState(() {
        _discoverResults = results;
        _discoverResults.results
            .sort((m1, m2) => m2.voteCount.compareTo(m1.voteCount));
      });
    }).catchError((e) => _onError(e));
  }

  void _loadTvResults() {
    Utils.getDiscoverTvResults(_getUrl()).then((results) {
      setState(() {
        _discoverTvResults = results;
        _discoverTvResults.results
            .removeWhere((tvShow) => tvShow.backdropPath == null);
        _discoverTvResults.results
            .sort((t1, t2) => t2.voteCount.compareTo(t1.voteCount));
      });
    }).catchError((e) => _onError(e));
  }

  void _onError(Exception e) {
    setState(() {
      _isError = true;
    });
  }

  void _onPageNumberClick(int page) {
    if (_selectedPage != page) {
      setState(() {
        _selectedPage = page;
        _discoverResults = null;
        _discoverTvResults = null;
      });
    }
  }

  String _getAppBarTitle() {
    if (_isError) return Strings.errorMessage;

    if (_isWidgetNull()) return Strings.loading;

    if (_isWidgetEmpty()) return Strings.searchResults;

    int totalResults = _discoverResults == null
        ? _discoverTvResults.totalResults
        : _discoverResults.totalResults;
    return "${1 + (_selectedPage - 1) * _resultsPerPage}-${min(_selectedPage * _resultsPerPage, totalResults)} из $totalResults";
  }

  int _getResultsCount() {
    return widget.isMovie
        ? _discoverResults.results.length
        : _discoverTvResults.results.length;
  }

  int _getTotalPages() {
    return widget.isMovie
        ? _discoverResults.totalPages
        : _discoverTvResults.totalPages;
  }

  bool _isWidgetNull() {
    return (widget.isMovie && _discoverResults == null) ||
        (!widget.isMovie && _discoverTvResults == null);
  }

  bool _isWidgetEmpty() {
    return (widget.isMovie && _discoverResults.results.isEmpty) ||
        (!widget.isMovie && _discoverTvResults.results.isEmpty);
  }

  String _getUrl() {
    if (widget.searchQuery != null)
      return Utils.buildSearchUrl(
          widget.searchQuery, widget.isMovie, _selectedPage);

    return Utils.buildDiscoverUrl(widget.genre, widget.fromYear, widget.toYear,
        widget.minRating, widget.isMovie, _selectedPage);
  }
}
