import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'tmdb/backdrop.dart';
import 'colors.dart';

class BackdropsTab extends StatelessWidget {
  BackdropsTab(
      {Key key,
      this.backdropsList,
      this.isVideo = true,
      this.isMovie = true,
      this.gender = 0})
      : super(key: key);

  final List<Backdrop> backdropsList;
  final bool isVideo;
  final bool isMovie;
  final int gender;

  final String _placeholderMoviePath = "images/poster_placeholder.png";
  final String _placeholderTvPath = "images/episode_placeholder.png";
  final String _placeholderMalePath = "images/3.0x/male_placeholder.png";
  final String _placeholderFemalePath = "images/3.0x/female_placeholder.png";

  @override
  Widget build(BuildContext context) {
    if (backdropsList == null || backdropsList.isEmpty)
      return _buildNoBackdrops();

    double aspect = isVideo ? 1.777 : 2.0 / 3.0;

    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.width / aspect,
      child: DefaultTabController(
          length: backdropsList.length,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              TabBarView(
                children: backdropsList.map((backdrop) {
                  return CachedNetworkImage(
                      fit: BoxFit.cover,
                      errorWidget: (context, url, error) => _buildPlaceholder(),
                      placeholder: (context, url) => _buildPlaceholder(),
                      imageUrl: backdrop.getBackdropPath());
                }).toList(),
              ),
              backdropsList.length > 1
                  ? TabPageSelector(
                      selectedColor: AppColors.accentColor,
                      color: Colors.white,
                      indicatorSize: 6)
                  : SizedBox()
            ],
          )),
    );
  }

  Widget _buildNoBackdrops() {
    return Container(width: double.infinity, child: _buildPlaceholder());
  }

  Widget _buildPlaceholder() {
    String path;
    if (isVideo)
      path = isMovie ? _placeholderMoviePath : _placeholderTvPath;
    else
      path = gender == 1 ? _placeholderFemalePath : _placeholderMalePath;

    return Image.asset(path, fit: BoxFit.cover);
  }
}
