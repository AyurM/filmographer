import 'dart:async';
import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:filmographer/tmdb/movie.dart';
import 'package:filmographer/tmdb/tv_show.dart';
import 'package:filmographer/tmdb/person_summary.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static const String dbName = "movies.db";
  static const String favouriteMovies = "favourites";
  static const String seenMovies = "seenMovies";
  static const String favouriteTvShows = "favouriteTvShows";
  static const String seenTvShows = "seenTvShows";
  static const String people = "people";

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, dbName);
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await createFavouriteMoviesTable(db);
      await createSeenMoviesTable(db);
      await createFavouriteTvShowsTable(db);
      await createSeenTvShowsTable(db);
      await createPeopleTable(db);
    });
  }

  Future<void> createFavouriteMoviesTable(Database db){
    return db.execute("CREATE TABLE favourites("
        "id INTEGER PRIMARY KEY, "
        "voteCount INTEGER, "
        "voteAverage REAL, "
        "title TEXT, "
        "popularity REAL, "
        "posterPath TEXT, "
        "originalLanguage TEXT, "
        "originalTitle TEXT, "
        "genreIds TEXT, "
        "backdropPath TEXT, "
        "overview TEXT, "
        "releaseDate TEXT)");
  }

  Future<void> createSeenMoviesTable(Database db){
    return db.execute("CREATE TABLE seenMovies("
        "id INTEGER PRIMARY KEY, "
        "voteCount INTEGER, "
        "voteAverage REAL, "
        "title TEXT, "
        "popularity REAL, "
        "posterPath TEXT, "
        "originalLanguage TEXT, "
        "originalTitle TEXT, "
        "genreIds TEXT, "
        "backdropPath TEXT, "
        "overview TEXT, "
        "releaseDate TEXT)");
  }

  Future<void> createFavouriteTvShowsTable(Database db){
    return db.execute("CREATE TABLE favouriteTvShows("
        "id INTEGER PRIMARY KEY, "
        "voteCount INTEGER, "
        "voteAverage REAL, "
        "name TEXT, "
        "popularity REAL, "
        "posterPath TEXT, "
        "originalLanguage TEXT, "
        "originalName TEXT, "
        "genreIds TEXT, "
        "backdropPath TEXT, "
        "overview TEXT, "
        "firstAirDate TEXT)");
  }

  Future<void> createSeenTvShowsTable(Database db){
    return db.execute("CREATE TABLE seenTvShows("
        "id INTEGER PRIMARY KEY, "
        "voteCount INTEGER, "
        "voteAverage REAL, "
        "name TEXT, "
        "popularity REAL, "
        "posterPath TEXT, "
        "originalLanguage TEXT, "
        "originalName TEXT, "
        "genreIds TEXT, "
        "backdropPath TEXT, "
        "overview TEXT, "
        "firstAirDate TEXT)");
  }

  Future<void> createPeopleTable(Database db){
    return db.execute("CREATE TABLE people("
        "id INTEGER PRIMARY KEY, "
        "gender INTEGER, "
        "name TEXT, "
        "knownForDepartment TEXT, "
        "profilePath TEXT, "
        "totalMovies INTEGER, "
        "totalTvShows INTEGER, "
        "totalPhotos INTEGER)");
  }

  //Movies
  insertFavouriteMovie(Movie movie) async {
    final db = await database;
    var res = await db.insert(favouriteMovies, movie.toMap());
    return res;
  }

  insertSeenMovie(Movie movie) async {
    final db = await database;
    var res = await db.insert(seenMovies, movie.toMap());
    return res;
  }

  deleteFavouriteMovie(int id) async {
    final db = await database;
    db.delete(favouriteMovies, where: "id = ?", whereArgs: [id]);
  }

  deleteSeenMovie(int id) async {
    final db = await database;
    db.delete(seenMovies, where: "id = ?", whereArgs: [id]);
  }

  deleteAllFavourite() async {
    final db = await database;
    db.rawDelete("DELETE FROM " + favouriteMovies);
  }

  deleteAllSeen() async {
    final db = await database;
    db.rawDelete("DELETE FROM " + seenMovies);
  }

  Future<List<Movie>> getAllFavouriteMovies() async {
    final db = await database;
    var res = await db.query(favouriteMovies, orderBy: "popularity DESC");
    List<Movie> list =
        res.isNotEmpty ? res.map((c) => Movie.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<Movie>> getAllSeenMovies() async {
    final db = await database;
    var res = await db.query(seenMovies, orderBy: "popularity DESC");
    List<Movie> list =
    res.isNotEmpty ? res.map((c) => Movie.fromMap(c)).toList() : [];
    return list;
  }

  //TvShows
  insertFavouriteTvShow(TvShow tvShow) async {
    final db = await database;
    var res = await db.insert(favouriteTvShows, tvShow.toMap());
    return res;
  }

  insertSeenTvShow(TvShow tvShow) async {
    final db = await database;
    var res = await db.insert(seenTvShows, tvShow.toMap());
    return res;
  }

  deleteFavouriteTvShow(int id) async {
    final db = await database;
    db.delete(favouriteTvShows, where: "id = ?", whereArgs: [id]);
  }

  deleteSeenTvShow(int id) async {
    final db = await database;
    db.delete(seenTvShows, where: "id = ?", whereArgs: [id]);
  }

  deleteAllFavouriteTvShows() async {
    final db = await database;
    db.rawDelete("DELETE FROM " + favouriteTvShows);
  }

  deleteAllSeenTvShows() async {
    final db = await database;
    db.rawDelete("DELETE FROM " + seenTvShows);
  }

  Future<List<TvShow>> getAllFavouriteTvShows() async {
    final db = await database;
    var res = await db.query(favouriteTvShows, orderBy: "popularity DESC");
    List<TvShow> list =
    res.isNotEmpty ? res.map((c) => TvShow.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<TvShow>> getAllSeenTvShows() async {
    final db = await database;
    var res = await db.query(seenTvShows, orderBy: "popularity DESC");
    List<TvShow> list =
    res.isNotEmpty ? res.map((c) => TvShow.fromMap(c)).toList() : [];
    return list;
  }

  //People
  insertPerson(PersonSummary person) async {
    final db = await database;
    var res = await db.insert(people, person.toMap());
    return res;
  }

  deletePerson(int id) async {
    final db = await database;
    db.delete(people, where: "id = ?", whereArgs: [id]);
  }

  deleteAllPeople() async {
    final db = await database;
    db.rawDelete("DELETE FROM " + people);
  }

  Future<List<PersonSummary>> getAllPeople() async {
    final db = await database;
    var res = await db.query(people, orderBy: "name ASC");
    List<PersonSummary> list =
    res.isNotEmpty ? res.map((c) => PersonSummary.fromMap(c)).toList() : [];
    return list;
  }
}
