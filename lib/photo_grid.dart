import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'tmdb/backdrop.dart';
import 'colors.dart';
import 'string_utils.dart';

class PhotoGrid extends StatelessWidget {
  final List<Backdrop> photos;
  final String title;
  final String name;
  final double aspectRatio;
  final double gridHeight;
  final int rows;

  static const double _padding = 8.0;
  final String _placeholderPath = "images/photo_placeholder.png";

  const PhotoGrid(
      {Key key,
      this.photos,
      this.title,
      this.name = "",
      @required this.aspectRatio,
      @required this.gridHeight,
      @required this.rows})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (photos == null || photos.isEmpty) return SizedBox();

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(_padding, _padding, _padding, 0.0),
          child: Text("$title (${photos.length})",
              style: TextStyle(
                  fontSize: 18,
                  color: AppColors.textColor,
                  fontWeight: FontWeight.bold)),
        ),
        Container(
          width: double.infinity,
          height: gridHeight,
          child: GridView.count(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(_padding),
            childAspectRatio: aspectRatio,
            mainAxisSpacing: _padding,
            crossAxisSpacing: _padding,
            crossAxisCount: rows,
            children: photos
                .map((photo) => GestureDetector(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4.0),
                        child: CachedNetworkImage(
                            fit: BoxFit.fitHeight,
                            placeholder: (context, string) =>
                                _buildPlaceholder(),
                            errorWidget: (context, url, error) =>
                                _buildPlaceholder(),
                            imageUrl:
                                StringUtils.getSmallPicUrl(photo.filePath)),
                      ),
                      onTap: () => _onPhotoClick(context, photo, name),
                    ))
                .toList(),
          ),
        )
      ],
    );
  }

  Widget _buildPlaceholder() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      child: Image.asset(_placeholderPath),
    );
  }

  void _onPhotoClick(BuildContext context, Backdrop photo, String name) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(name, style: TextStyle(color: Colors.white)),
                    centerTitle: true,
                  ),
                  backgroundColor: Colors.black,
                  body: Center(
                    child: CachedNetworkImage(
                        width: double.infinity,
                        fit: BoxFit.fitHeight,
//        placeholder: (context, string) => _buildPlaceholder(),
//        errorWidget: (context, url, error) => _buildPlaceholder(),
                        imageUrl: StringUtils.getHighResPicUrl(photo.filePath)),
                  ),
                )));
  }
}
