import 'package:flutter/material.dart';

//Цветной ярлык для оценки фильма/сериала, продолжительности, кол-ва сезонов и т.д.
class GradientBox extends StatelessWidget {
  GradientBox({Key key, this.iconData, this.text, this.gradientColors})
      : super(key: key);
  final IconData iconData;
  final String text;
  final List<Color> gradientColors;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 8),
        child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey, blurRadius: 2, offset: Offset(2, 2))
              ],
              gradient: LinearGradient(
                  colors: gradientColors,
                  begin: FractionalOffset.bottomLeft,
                  end: FractionalOffset.topRight),
            ),
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: Icon(iconData, color: Colors.white)),
                    Text(
                      text,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 16),
                    )
                  ],
                ))));
  }
}
