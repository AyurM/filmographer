import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'colors.dart';

class TitleBox extends StatelessWidget {
  final String title;
  final String subtitle;

  TitleBox({Key key, this.title, this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          Utils.screenAwareWidth(8.0, context),
          Utils.screenAwareHeight(8.0, context),
          Utils.screenAwareWidth(64.0, context),
          Utils.screenAwareHeight(8.0, context)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontSize: Utils.screenAwareHeight(24.0, context),
                fontWeight: FontWeight.bold,
                color: AppColors.textColor),
          ),
          Padding(
              padding:
                  EdgeInsets.only(top: Utils.screenAwareHeight(4.0, context)),
              child: Text(subtitle,
                  style: TextStyle(
                      fontSize: Utils.screenAwareHeight(14.0, context),
                      color: AppColors.textColor)))
        ],
      ),
    );
  }
}
