import 'package:flutter/material.dart';
import 'search_screen.dart';
import 'package:filmographer/movies/movies_screen.dart';
import 'package:filmographer/tv_shows/tv_shows_screen.dart';
import 'package:filmographer/people/people_screen.dart';
import 'strings.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _pageIndex = 0;
  final Color bottomNavColor = Colors.teal;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        color: Colors.white,
        child: Scaffold(
          bottomNavigationBar: _buildBottomNavigation(),
          body: _buildScaffoldBody(),
          backgroundColor: Colors.transparent,
        ));
  }

  Widget _buildScaffoldBody() {
    switch (_pageIndex) {
      case 0:
        return SearchScreen();
        break;
      case 1:
        return MoviesScreen();
        break;
      case 2:
        return TvShowsScreen();
        break;
      case 3:
        return PeopleScreen();
        break;
      default:
        return SearchScreen();
        break;
    }
  }

  Widget _buildBottomNavigation() {
    final navBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          backgroundColor: bottomNavColor,
          icon: Icon(Icons.search),
          title: Text(Strings.search)),
      BottomNavigationBarItem(
          backgroundColor: bottomNavColor,
          icon: Icon(Icons.movie),
          title: Text(Strings.movies)),
      BottomNavigationBarItem(
          backgroundColor: bottomNavColor,
          icon: Icon(Icons.tv),
          title: Text(Strings.tvShows)),
      BottomNavigationBarItem(
          backgroundColor: bottomNavColor,
          icon: Icon(Icons.people),
          title: Text(Strings.people))
    ];

    return BottomNavigationBar(
        items: navBarItems,
        currentIndex: _pageIndex,
        type: BottomNavigationBarType.shifting,
        onTap: (int index) {
          setState(() {
            _pageIndex = index;
          });
        });
  }
}
