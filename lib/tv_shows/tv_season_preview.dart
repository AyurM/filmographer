import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/tmdb/tv_season.dart';
import 'package:filmographer/string_utils.dart';

//Превью к сезону сериала (постер, номер сезона, кол-во серий)
class TvSeasonPreview extends StatelessWidget {
  const TvSeasonPreview({Key key, this.season}) : super(key: key);

  final TvSeason season;

  final String _placeholderPath = "images/tv_placeholder.png";
  final double _width = 140.0;
  final double _aspect = 2.0 / 3.0;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(right: 12),
        width: _width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildPoster(),
            Padding(
              padding: const EdgeInsets.only(top: 4, left: 4, right: 4),
              child: Text(season.name),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: Text(
                "${season.episodeCount} ${StringUtils.getEpisodesPlural(season.episodeCount)}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ));
  }

  Widget _buildPoster() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(color: Colors.grey, blurRadius: 2, offset: Offset(3, 3))
          ]),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: CachedNetworkImage(
              width: _width,
              height: _width / _aspect,
              fit: BoxFit.fill,
              placeholder: (context, string) => _buildPlaceholder(),
              errorWidget: (context, url, error) => _buildPlaceholder(),
              imageUrl: season.posterPath == null
                  ? _placeholderPath
                  : StringUtils.getSmallPicUrl(season.posterPath))),
    );
  }

  Widget _buildPlaceholder() {
    return Image.asset(_placeholderPath);
  }
}
