import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/tv_shows/favourite_tv_shows_screen.dart';
import 'package:filmographer/tv_shows/seen_tv_shows_screen.dart';
import 'package:filmographer/model/tv_shows_model.dart';
import 'package:filmographer/strings.dart';

//Экран "Сериалы"
class TvShowsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _tvShowTabs = <Tab>[
      Tab(text: Strings.favourites),
      Tab(text: Strings.wantToWatch),
    ];

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
            flexibleSpace: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [TabBar(tabs: _tvShowTabs)],
        )),
        body: ScopedModelDescendant<TvShowsModel>(
            builder: (context, child, model) {
          return TabBarView(children: <Widget>[
            FavouriteTvShowsScreen(),
            SeenTvShowsScreen(),
          ]);
        }),
      ),
    );
  }
}
