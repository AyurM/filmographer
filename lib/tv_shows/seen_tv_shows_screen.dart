import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/tv_shows_model.dart';
import 'package:filmographer/movies/movie_item.dart';
import 'package:filmographer/strings.dart';

class SeenTvShowsScreen extends StatefulWidget {
  @override
  _SeenTvShowsScreenState createState() => _SeenTvShowsScreenState();
}

class _SeenTvShowsScreenState extends State<SeenTvShowsScreen> {
  final String _loadingImagePath = "images/loading.gif";
  final String _noElementsImagePath = "images/cinema.png";

  @override
  void initState() {
    super.initState();
    ScopedModel.of<TvShowsModel>(context).getSeenTvShows();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TvShowsModel>(
        builder: (context, child, model) {
      if (model.isLoading) return _buildLoadingUI();

      if (model.seenTvShows.isEmpty) return _buildNoElementsUI();

      return ListView.builder(
          itemCount: model.seenTvShows.length,
          itemBuilder: (BuildContext context, int i) {
            return MovieItem(video: model.seenTvShows[i]);
          });
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingImagePath));
  }

  Widget _buildNoElementsUI() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noElementsImagePath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.noElements,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
