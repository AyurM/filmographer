import 'package:flutter/material.dart';
import 'package:filmographer/tmdb/tv_episode.dart';
import 'package:filmographer/tv_shows/tv_episode_preview.dart';
import 'package:filmographer/tv_shows/tv_episode_info.dart';
import 'package:filmographer/colors.dart';

//Список превью к сериям сезона
class TvEpisodesList extends StatelessWidget {
  const TvEpisodesList(
      {Key key, this.title, this.episodes, @required this.tvShowId})
      : super(key: key);

  final String title;
  final int tvShowId;
  final List<TvEpisode> episodes;

  final double _listHeight = 170.0;

  @override
  Widget build(BuildContext context) {
    if (episodes == null || episodes.isEmpty) return SizedBox();

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_buildTitle(), _buildPosterList()],
      ),
    );
  }

  Widget _buildTitle() {
    return Text("$title (${episodes.length})",
        style: TextStyle(
            fontSize: 18,
            color: AppColors.textColor,
            fontWeight: FontWeight.bold));
  }

  Widget _buildPosterList() {
    return Container(
        padding: const EdgeInsets.only(top: 8.0),
        height: _listHeight,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: episodes.length,
            itemBuilder: (BuildContext context, int i) {
              return GestureDetector(
                  child: TvEpisodePreview(episode: episodes[i]),
                  onTap: () => _onEpisodeClick(context, i));
            }));
  }

  void _onEpisodeClick(BuildContext context, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TvEpisodeInfo(
              episode: episodes[index],
                tvShowId: tvShowId
            )));
  }
}
