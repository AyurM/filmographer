import 'dart:math';

import 'package:filmographer/connection_error.dart';
import 'package:filmographer/external_links.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/tv_shows_model.dart';
import 'package:filmographer/tmdb/tmdb_video.dart';
import 'package:filmographer/tmdb/tv_show.dart';
import 'package:filmographer/tmdb/tv_show_details.dart';
import 'package:filmographer/tmdb/backdrop.dart';
import 'package:filmographer/tmdb/crew.dart';
import 'package:filmographer/tmdb/tmdb_parameters.dart';
import 'package:filmographer/gradient_box.dart';
import 'package:filmographer/backdrop_placeholder.dart';
import 'package:filmographer/tv_shows/tv_seasons_list.dart';
import 'package:filmographer/title_box.dart';
import 'package:filmographer/loading_placeholder.dart';
import 'package:filmographer/trailers_tab.dart';
import 'package:filmographer/overview.dart';
import 'package:filmographer/poster_list.dart';
import 'package:filmographer/backrdops_tab.dart';
import 'package:filmographer/people/cast_list.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/colors.dart';

//Экран с детальными сведениями о сериале
class TvShowInfo extends StatefulWidget {
  final TmdbVideo video;

  TvShowInfo({Key key, @required this.video}) : super(key: key);

  @override
  _TvShowInfoState createState() => _TvShowInfoState();
}

class _TvShowInfoState extends State<TvShowInfo> {
  final int _maxBackdrops = 7;
  TvShowDetails _details;
  bool _isError = false;

  @override
  Widget build(BuildContext context) {
    if (!_isError && _details == null) _loadDetails();

    if (_isError) return _buildErrorUI();

    return Scaffold(
        appBar: AppBar(title: Text(widget.video.title), centerTitle: true),
        body: _buildScaffoldBody());
  }

  Widget _buildScaffoldBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTopStack(),
          _buildInfoLabels(),
          _buildYearAndGenres(),
          Overview(title: Strings.aboutTvShow, overview: widget.video.overview),
          _buildSeasons(),
          _buildCast(),
          _buildDirector(),
          _buildRecommendations(),
          _buildTrailersTab(),
          _buildLinks()
        ],
      ),
    );
  }

  Widget _buildTopStack() {
    return ScopedModelDescendant<TvShowsModel>(
        builder: (context, child, model) {
      return Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildBackdropTabs(),
              _buildButtonsRow(model),
              Divider(height: 0),
              TitleBox(
                  title: widget.video.title,
                  subtitle: widget.video.originalTitle)
            ],
          )
        ],
      );
    });
  }

  Widget _buildBackdropTabs() {
    if (_details == null) return BackdropPlaceholder();

    List<Backdrop> backdrops = _details.images.backdrops
        .take(min(_maxBackdrops, _details.images.backdrops.length))
        .toList();

    return BackdropsTab(
        backdropsList: backdrops, isVideo: true, isMovie: false);
  }

  Widget _buildButtonsRow(TvShowsModel model) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _getFavouriteIcon(
                  model.favouriteTvShows.contains(widget.video as TvShow)),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                    model.favouriteTvShows.contains(widget.video as TvShow)
                        ? Strings.inFavourites
                        : Strings.addFavourites),
              )
            ],
          ),
          onPressed: () => _onFabLikeClick(model),
        ),
        FlatButton(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(Strings.wantToWatch),
              ),
              _getToWatchIcon(
                  model.seenTvShows.contains(widget.video as TvShow)),
            ],
          ),
          onPressed: () => _onFabToWatchClick(model),
        )
      ],
    );
  }

  Widget _buildInfoLabels() {
    final List<Widget> labels = [
      GradientBox(
          iconData: Icons.star,
          text: widget.video.voteAverage.toString(),
          gradientColors: Utils.getRatingColor(widget.video.voteAverage)),
      GradientBox(
          iconData: Icons.access_time,
          text: _details == null ? "---" : "${_details.episodeRunTime[0]} мин.",
          gradientColors: [Colors.lightBlue, Colors.indigo]),
      GradientBox(
        iconData: Icons.calendar_today,
        text: _details == null
            ? "---"
            : "${_details.numberOfSeasons} ${StringUtils.getSeasonsPlural(_details.numberOfSeasons)}",
        gradientColors: [Colors.purple, Colors.deepPurple],
      ),
      GradientBox(
        iconData: Icons.tv,
        text: _details == null
            ? "---"
            : "${_details.numberOfEpisodes} ${StringUtils.getEpisodesPlural(_details.numberOfEpisodes)}",
        gradientColors: [Colors.orange, Colors.deepOrange],
      ),
      GradientBox(
          iconData: Icons.people_outline,
          text:
              "${widget.video.voteCount.toString()} ${StringUtils.getVotesPlural(widget.video.voteCount)}",
          gradientColors: [Colors.grey, Colors.blueGrey]),
      GradientBox(
        iconData: Icons.camera,
        text: _details == null
            ? "---"
            : _getProductionStatus(_details.inProduction),
        gradientColors: [Colors.cyan, Colors.teal],
      )
    ];

    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        child: Container(
            height: 52,
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: labels,
                ))));
  }

  Widget _buildYearAndGenres() {
    String genres =
        _details == null ? "" : StringUtils.getGenresNames(_details.genres);
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Text("${widget.video.releaseDate.substring(0, 4)}, $genres",
          style: TextStyle(fontSize: 14, color: AppColors.textColor)),
    );
  }

  Widget _buildSeasons() {
    if (_details == null) return LoadingPlaceholder(title: Strings.seasons);

    return TvSeasonsList(
        title: Strings.seasons,
        seasons: _details.seasons,
        tvShowId: widget.video.id,
        tvShowName: widget.video.title);
  }

  Widget _buildCast() {
    if (_details == null) return LoadingPlaceholder(title: Strings.cast);

    //отфильтровать актеров без картинки профиля
    return CastList(
        persons:
            _details.credits.cast.where((c) => c.profilePath != null).toList(),
        title: Strings.cast);
  }

  Widget _buildDirector() {
    if (_details == null) return LoadingPlaceholder(title: Strings.director);

    List<Crew> directors =
        _details.credits.crew.where((c) => c.job == "Director").toList();
    if (directors.isNotEmpty)
      return CastList(persons: directors, title: Strings.director);
    else
      return SizedBox();
  }

  Widget _buildRecommendations() {
    if (_details == null)
      return LoadingPlaceholder(title: Strings.recommendations);

    return PosterList(
        title: Strings.recommendations,
        videos: _details.recommendations.results);
  }

  Widget _buildTrailersTab() {
    if (_details == null) return LoadingPlaceholder(title: Strings.trailers);

    return TrailersTab(
      videos: _details.videos.results
          .where((v) =>
              v.site == TmdbParams.youTubeValue &&
              v.type == TmdbParams.typeTrailer)
          .toList(),
    );
  }

  Widget _buildLinks() {
    if (_details == null) return LoadingPlaceholder(title: Strings.links);

    return ExternalLinks(ids: _details.externalIds, title: widget.video.title);
  }

  Widget _buildErrorUI() {
    return Scaffold(
        appBar: AppBar(title: Text(widget.video.title), centerTitle: true),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ConnectionError(onRetry: () {
                setState(() {
                  _isError = false;
                });
              }),
              _buildInfoLabels(),
              _buildYearAndGenres(),
              Overview(
                  title: Strings.aboutMovie, overview: widget.video.overview),
            ],
          ),
        ));
  }

  void _loadDetails() {
    Utils.getTvShowDetails(Utils.buildTvDetailsUrl(widget.video.id))
        .then((results) {
      setState(() {
        _details = results;
      });
    }).catchError((e) => _onError(e));
  }

  void _onFabLikeClick(TvShowsModel model) {
    TvShow tvShow = widget.video as TvShow;
    if (!model.favouriteTvShows.contains(tvShow))
      model.addToFavourite(tvShow);
    else
      model.removeFromFavourites(tvShow);
  }

  void _onFabToWatchClick(TvShowsModel model) {
    TvShow tvShow = widget.video as TvShow;
    if (!model.seenTvShows.contains(tvShow))
      model.addToSeen(tvShow);
    else
      model.removeFromSeen(tvShow);
  }

  String _getProductionStatus(bool inProduction) {
    return inProduction ? Strings.inProduction : Strings.finished;
  }

  Widget _getFavouriteIcon(bool isFavourite) {
    return isFavourite
        ? Icon(
            Icons.favorite,
            color: Colors.red,
          )
        : Icon(
            Icons.favorite_border,
            color: Colors.grey,
          );
  }

  Widget _getToWatchIcon(bool isInToWatchList) {
    return isInToWatchList
        ? Icon(
            Icons.remove_red_eye,
            color: Colors.blue,
          )
        : Icon(
            Icons.remove_red_eye,
            color: Colors.grey,
          );
  }

  void _onError(Exception e) {
    setState(() {
      _isError = true;
    });
  }
}
