import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/tv_shows_model.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/movies/movie_item.dart';

class FavouriteTvShowsScreen extends StatefulWidget {
  @override
  _FavouriteTvShowsScreenState createState() => _FavouriteTvShowsScreenState();
}

class _FavouriteTvShowsScreenState extends State<FavouriteTvShowsScreen> {
  final String _loadingImagePath = "images/loading.gif";
  final String _noElementsImagePath = "images/cinema.png";

  @override
  void initState() {
    super.initState();
    ScopedModel.of<TvShowsModel>(context).getFavouriteTvShows();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<TvShowsModel>(builder: (context, child, model) {
      if (model.isLoading) return _buildLoadingUI();

      if (model.favouriteTvShows.isEmpty) return _buildNoElementsUI();

      return ListView.builder(
          itemCount: model.favouriteTvShows.length,
          itemBuilder: (BuildContext context, int i) {
            return MovieItem(video: model.favouriteTvShows[i]);
          });
//        PosterFlipper(
//          movies: model.favouriteTvShows.reversed.toList(), screenPercent: 0.75);
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingImagePath));
  }

  Widget _buildNoElementsUI() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noElementsImagePath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.noElements,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
