import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/tmdb/tv_season.dart';
import 'package:filmographer/tmdb/tv_season_details.dart';
import 'package:filmographer/loading_placeholder.dart';
import 'package:filmographer/tv_shows/tv_episode_list.dart';
import 'package:filmographer/overview.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/title_box.dart';

//Экран со сведениями о сезоне сериала (постер, описание, список серий)
class TvSeasonInfo extends StatefulWidget {
  final TvSeason season;
  final int tvShowId;
  final String tvShowName;

  const TvSeasonInfo({Key key, this.season, this.tvShowId, this.tvShowName})
      : super(key: key);

  @override
  _TvSeasonInfoState createState() => _TvSeasonInfoState();
}

class _TvSeasonInfoState extends State<TvSeasonInfo> {
  TvSeasonDetails _details;
  final double _posterAspect = 1.777;
  final String _placeholderPath = "images/3.0x/episode_placeholder.png";

  @override
  Widget build(BuildContext context) {
    if (_details == null) {
      _loadTvSeasonDetails();
    }

    return Scaffold(
        appBar: AppBar(title: Text(widget.tvShowName), centerTitle: true),
        body: _buildScaffoldBody());
  }

  Widget _buildScaffoldBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildPoster(),
          _buildTitle(),
          Overview(
              title: Strings.aboutSeason, overview: widget.season.overview),
          _buildEpisodes()
        ],
      ),
    );
  }

  Widget _buildPoster() {
    return CachedNetworkImage(
        width: double.infinity,
        height: MediaQuery.of(context).size.width * _posterAspect / 2,
        fit: BoxFit.cover,
        placeholder: (context, url) => _buildPlaceholder(),
        errorWidget: (context, url, error) => _buildPlaceholder(),
        imageUrl: widget.season.posterPath == null
            ? _placeholderPath
            : StringUtils.getHighResPicUrl(widget.season.posterPath));
  }

  Widget _buildTitle() {
    String airDate = widget.season.airDate == null
        ? Strings.unknown
        : StringUtils.getDateString(DateTime.parse(widget.season.airDate));
    return TitleBox(
        title: widget.season.name, subtitle: "${Strings.airDate}: $airDate");
  }

  Widget _buildEpisodes() {
    if (_details == null) return LoadingPlaceholder(title: Strings.episodes);

    return TvEpisodesList(
        title: Strings.episodes,
        episodes: _details.episodes,
        tvShowId: widget.tvShowId);
  }

  Widget _buildPlaceholder() {
    return Image.asset(_placeholderPath);
  }

  void _loadTvSeasonDetails() {
    Utils.getTvSeasonDetails(Utils.buildTvSeasonDetailsUrl(
            widget.tvShowId, widget.season.seasonNumber))
        .then((results) {
      setState(() {
        _details = results;
      });
    });
  }
}
