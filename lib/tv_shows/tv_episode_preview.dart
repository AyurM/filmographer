import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/tmdb/tv_episode.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/strings.dart';

//Карточка с превью серии (изображение, номер серии, название)
class TvEpisodePreview extends StatelessWidget {
  const TvEpisodePreview({Key key, this.episode}) : super(key: key);

  final TvEpisode episode;

  final String _placeholderPath = "images/episode_placeholder.png";
  final double _width = 160.0;
  final double _aspect = 500.0 / 281.0;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(right: 12),
        width: _width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildPoster(),
            Padding(
              padding: const EdgeInsets.only(top: 4, left: 4, right: 4),
              child: Text("${episode.episodeNumber} ${Strings.episode}"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0),
              child: Text(
                episode.name,
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ));
  }

  Widget _buildPoster() {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(color: Colors.grey, blurRadius: 2, offset: Offset(3, 3))
          ]),
      child: AspectRatio(
        aspectRatio: _aspect,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: CachedNetworkImage(
                width: _width,
                fit: BoxFit.cover,
                placeholder: (context, string) => _buildPlaceholder(),
                errorWidget: (context, url, error) => _buildPlaceholder(),
                imageUrl: episode.stillPath == null
                    ? _placeholderPath
                    : StringUtils.getSmallPicUrl(episode.stillPath))),
      ),
    );
  }

  Widget _buildPlaceholder() {
    return Image.asset(_placeholderPath);
  }
}
