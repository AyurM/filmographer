import 'dart:math';

import 'package:filmographer/gradient_box.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/tmdb/tv_episode.dart';
import 'package:filmographer/tmdb/image_results.dart';
import 'package:filmographer/loading_placeholder.dart';
import 'package:filmographer/photo_grid.dart';
import 'package:filmographer/people/cast_list.dart';
import 'package:filmographer/overview.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/title_box.dart';

//Экран со сведениями о серии (фото, описание, список актеров, оценка, дата выхода,
//кадры)
class TvEpisodeInfo extends StatefulWidget {
  final TvEpisode episode;
  final int tvShowId;

  const TvEpisodeInfo({Key key, this.episode, this.tvShowId}) : super(key: key);

  @override
  _TvEpisodeInfoState createState() => _TvEpisodeInfoState();
}

class _TvEpisodeInfoState extends State<TvEpisodeInfo> {
  ImageResults _stills;
  final double _posterAspect = 500.0 / 281.0;
  final double _photoRowHeight = 140.0;
  final String _placeholderPath = "images/3.0x/episode_placeholder.png";

  @override
  Widget build(BuildContext context) {
    if (_stills == null) {
      _loadTvEpisodeStills();
    }

    return Scaffold(
        appBar: AppBar(title: Text(widget.episode.name), centerTitle: true),
        body: _buildScaffoldBody());
  }

  Widget _buildScaffoldBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildPoster(),
          _buildTitle(),
          _buildInfoLabels(),
          Overview(
              title: Strings.aboutEpisode, overview: widget.episode.overview),
          _buildCast(),
          _buildStills()
        ],
      ),
    );
  }

  Widget _buildPoster() {
    return CachedNetworkImage(
        width: double.infinity,
        height: MediaQuery.of(context).size.width / _posterAspect,
        fit: BoxFit.cover,
        placeholder: (context, url) => _buildPlaceholder(),
        errorWidget: (context, url, error) => _buildPlaceholder(),
        imageUrl: widget.episode.stillPath == null
            ? _placeholderPath
            : StringUtils.getHighResPicUrl(widget.episode.stillPath));
  }

  Widget _buildTitle() {
    String airDate = widget.episode.airDate == null
        ? Strings.unknown
        : StringUtils.getDateString(DateTime.parse(widget.episode.airDate));
    return TitleBox(
        title: widget.episode.name, subtitle: "${Strings.airDate}: $airDate");
  }

  Widget _buildInfoLabels() {
    final List<Widget> labels = [
      GradientBox(
          iconData: Icons.star,
          text: widget.episode.voteAverage.toStringAsPrecision(2),
          gradientColors: Utils.getRatingColor(widget.episode.voteAverage)),
      GradientBox(
        iconData: Icons.calendar_today,
        text: "${widget.episode.seasonNumber} ${Strings.season}",
        gradientColors: [Colors.purple, Colors.deepPurple],
      ),
      GradientBox(
        iconData: Icons.tv,
        text: "${widget.episode.episodeNumber} ${Strings.episode}",
        gradientColors: [Colors.orange, Colors.deepOrange],
      ),
      GradientBox(
          iconData: Icons.people_outline,
          text:
              "${widget.episode.voteCount.toString()} ${StringUtils.getVotesPlural(widget.episode.voteCount)}",
          gradientColors: [Colors.grey, Colors.blueGrey])
    ];

    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        child: Container(
            height: 52,
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: labels,
                ))));
  }

  Widget _buildCast() {
    //отфильтровать актеров без картинки профиля
    return CastList(
        persons: widget.episode.guestStars
            .where((c) => c.profilePath != null)
            .toList(),
        title: Strings.guestStars);
  }

  Widget _buildStills() {
    if (_stills == null) return LoadingPlaceholder(title: Strings.stills);

    //кол-во рядов в сетке от 1 до 3
    int rows = min(3, max(1, (_stills.stills.length / 10).round()));
    return PhotoGrid(
        photos: _stills.stills,
        title: Strings.stills,
        name: widget.episode.name,
        aspectRatio: 1 / _posterAspect,
        gridHeight: rows * _photoRowHeight,
        rows: rows);
  }

  Widget _buildPlaceholder() {
    return Image.asset(_placeholderPath);
  }

  void _loadTvEpisodeStills() {
    Utils.getImageResults(Utils.buildTvEpisodeStillsUrl(widget.tvShowId,
            widget.episode.seasonNumber, widget.episode.episodeNumber))
        .then((results) {
      setState(() {
        _stills = results;
      });
    });
  }
}
