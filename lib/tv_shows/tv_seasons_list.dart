import 'package:flutter/material.dart';
import 'package:filmographer/tmdb/tv_season.dart';
import 'package:filmographer/tv_shows/tv_season_preview.dart';
import 'package:filmographer/tv_shows/tv_season_info.dart';
import 'package:filmographer/colors.dart';

class TvSeasonsList extends StatelessWidget {
  const TvSeasonsList(
      {Key key,
      this.title,
      this.seasons,
      @required this.tvShowId,
      this.tvShowName})
      : super(key: key);

  final String title;
  final String tvShowName;
  final int tvShowId;
  final List<TvSeason> seasons;

  final double _listHeight = 265.0;

  @override
  Widget build(BuildContext context) {
    if (seasons == null || seasons.isEmpty) return SizedBox();

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_buildTitle(), _buildPosterList()],
      ),
    );
  }

  Widget _buildTitle() {
    return Text("$title (${seasons.length})",
        style: TextStyle(
            fontSize: 18,
            color: AppColors.textColor,
            fontWeight: FontWeight.bold));
  }

  Widget _buildPosterList() {
    return Container(
        padding: const EdgeInsets.only(top: 8.0),
        height: _listHeight,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: seasons.length,
            itemBuilder: (BuildContext context, int i) {
              return GestureDetector(
                  child: TvSeasonPreview(season: seasons[i]),
                  onTap: () => _onSeasonClick(context, i));
            }));
  }

  void _onSeasonClick(BuildContext context, int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TvSeasonInfo(
                  season: seasons[index],
                  tvShowId: tvShowId,
                  tvShowName: tvShowName
                )));
  }
}
