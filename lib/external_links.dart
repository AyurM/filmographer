import 'dart:async';

import 'package:flutter/material.dart';
import 'package:filmographer/tmdb/external_ids.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:filmographer/colors.dart';
import 'strings.dart';

//Ссылки на соц.сети
class ExternalLinks extends StatelessWidget {
  final ExternalIds ids;
  final String title;
  final bool isVideo;

  const ExternalLinks(
      {Key key, @required this.ids, this.title, this.isVideo = true})
      : super(key: key);

  final String _instagram = "https://www.instagram.com/";
  final String _facebook = "https://www.facebook.com/";
  final String _twitter = "https://twitter.com/";
  final String _imdbTitle = "https://www.imdb.com/title/";
  final String _imdbName = "https://www.imdb.com/name/";

  @override
  Widget build(BuildContext context) {
    if (ids.isEmpty()) return SizedBox();

    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_buildTitle(), _buildButtonRow(context)],
      ),
    );
  }

  Widget _buildTitle() {
    return Text(Strings.links,
        style: TextStyle(
            fontSize: 18,
            color: AppColors.textColor,
            fontWeight: FontWeight.bold));
  }

  Widget _buildButtonRow(BuildContext context) {
    double iconSize = MediaQuery.of(context).size.width / 6.0;

    List<Widget> buttons = List<Widget>();
    if(ids.instagram != null)
      buttons.add(_buildButton(0, ids.instagram, iconSize, context));

    if(ids.facebook != null)
      buttons.add(_buildButton(1, ids.facebook, iconSize, context));

    if(ids.twitter != null)
      buttons.add(_buildButton(2, ids.twitter, iconSize, context));

    if(ids.imdb != null)
      buttons.add(_buildButton(3, ids.imdb, iconSize, context));

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: buttons,
    );
  }

  Widget _buildButton(
      int linkType, String url, double iconSize, BuildContext context) {
    if (url == null) return SizedBox();

    Icon icon;
    String externalLink;
    String siteName;
    switch (linkType) {
      case 0:
        icon = Icon(FontAwesomeIcons.instagram, color: Colors.purple);
        externalLink = _instagram + url;
        siteName = "Instagram";
        break;
      case 1:
        icon = Icon(FontAwesomeIcons.facebook, color: Colors.blue);
        externalLink = _facebook + url;
        siteName = "Facebook";
        break;
      case 2:
        icon = Icon(FontAwesomeIcons.twitter, color: Colors.lightBlue);
        externalLink = _twitter + url;
        siteName = "Twitter";
        break;
      default:
        icon = Icon(FontAwesomeIcons.imdb, color: Colors.orangeAccent);
        externalLink = isVideo ? _imdbTitle + url : _imdbName + url;
        siteName = "IMDb";
        break;
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
            iconSize: iconSize,
            icon: icon,
            onPressed: () => _onButtonClick(externalLink, context)),
        Text(siteName)
      ],
    );
  }

  void _onButtonClick(String externalLink, BuildContext context) {
    print(externalLink);

    final Completer<WebViewController> _controller =
        Completer<WebViewController>();

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Scaffold(
                  appBar: AppBar(
                    title: Text(title),
                    actions: <Widget>[
                      NavigationControls(_controller.future),
                    ],
                  ),
                  body: WebView(
                    key: UniqueKey(),
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: externalLink,
                    onWebViewCreated: (WebViewController webViewController) {
                      _controller.complete(webViewController);
                    },
                  ),
                )));
  }
}

class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController controller = snapshot.data;
        return Row(
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller.canGoBack()) {
                        controller.goBack();
                      }
                    },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios),
              onPressed: !webViewReady
                  ? null
                  : () async {
                      if (await controller.canGoForward()) {
                        controller.goForward();
                      }
                    },
            ),
            IconButton(
              icon: const Icon(Icons.replay),
              onPressed: !webViewReady
                  ? null
                  : () {
                      controller.reload();
                    },
            ),
          ],
        );
      },
    );
  }
}
