import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'string_utils.dart';
import 'tmdb/movie.dart';

class PosterCarousel extends StatefulWidget {
  final List<Movie> movies;
  final double height;
  final double widthPercent;

  PosterCarousel({Key key, this.movies, this.height, this.widthPercent = 0.2})
      : super(key: key);

  @override
  _PosterCarouselState createState() => _PosterCarouselState();
}

class _PosterCarouselState extends State<PosterCarousel>
    with TickerProviderStateMixin {
  int _currentIndex = 0;
  final double _aspect = 2.0 / 3.0;
  final double _padding = 10.0;
  final double _scrollSpeedMultiplier = 1.5;

  double scrollPercent = 0.0;
  Offset startDrag;
  double startDragPercentScroll;
  double finishScrollStart;
  double finishScrollEnd;
  AnimationController finishScrollController;

  double fullWidth;
  double partialWidth;

  List<double> widths = [];

  @override
  void initState() {
    super.initState();

    fullWidth = widget.height * _aspect;
    partialWidth = fullWidth * widget.widthPercent;
    for (int i = 0; i < widget.movies.length; i++)
      widths.add(i == 0 ? fullWidth : partialWidth);

    finishScrollController = new AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    )
      ..addListener(() {
        setState(() {
          scrollPercent = lerpDouble(
              finishScrollStart, finishScrollEnd, finishScrollController.value);

//          if (widget.onScroll != null) {
//            widget.onScroll(scrollPercent);
//          }
        });
      })
      ..addStatusListener((AnimationStatus status) {});
  }

  @override
  Widget build(BuildContext context) {
    return _buildPosterRow();
  }

  Widget _buildPosterRow() {
    return GestureDetector(
      onHorizontalDragStart: _onPanStart,
      onHorizontalDragUpdate: _onPanUpdate,
      onHorizontalDragEnd: _onPanEnd,
      behavior: HitTestBehavior.translucent,
      child: Stack(
        children: _buildPosters(),
      ),
    );
  }

  List<Widget> _buildPosters() {
    int index = -1;
    return widget.movies.map((movie) {
      index++;
      return _buildPoster(movie, index, widget.movies.length, scrollPercent);
    }).toList();
  }

  Widget _buildPoster(
      Movie movie, int index, int posterCount, double scrollPercent) {
    return Positioned.fromRect(
      rect: Rect.fromLTWH(_getPadding(index) - _getOffset(scrollPercent),
          _padding, _getWidth(index), widget.height),
      child: FractionalTranslation(
        translation: Offset(0.0, 0.0),
        child: Container(
          decoration:
              BoxDecoration(borderRadius: BorderRadius.circular(8), boxShadow: [
            BoxShadow(color: Colors.grey, blurRadius: 2, offset: Offset(3, 3))
          ]),
          width: index == _currentIndex ? fullWidth : partialWidth,
          height: widget.height,
          child: Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: CachedNetworkImage(
                    fit: BoxFit.fitHeight,
//              placeholder: (context, string) => _buildPlaceholder(),
//              errorWidget: (context, url, error) => _buildPlaceholder(),
                    imageUrl:
                        StringUtils.getSmallPicUrl(widget.movies[index].posterPath))),
          ),
        ),
      ),
    );
  }

  double _getPadding(int index) {
    double totalWidth = 0.0;
    if (index > 0) {
      for (int i = 0; i < index; i++) totalWidth += widths[i];
    }

    return totalWidth + _padding * (index + 1);
  }

  double _getOffset(double scrollPercent) {
    return scrollPercent * widget.movies.length * (partialWidth + _padding);
  }

  double _getWidth(int index) {
    double widthScale;
    double diff = scrollPercent * widget.movies.length - index;

    if (!diff.isNegative && diff < 1 - widget.widthPercent)
      widthScale = (1 - diff).clamp(widget.widthPercent, 1);
    else if (diff.isNegative &&
        diff.abs() < 1 - widget.widthPercent &&
        diff.abs() < 1)
      widthScale = 1 - diff.abs();
    else
      widthScale = widget.widthPercent;

    double result = fullWidth * widthScale;
    widths[index] = result;

    return result;
  }

  void _onPanStart(DragStartDetails details) {
    startDrag = details.globalPosition;
    startDragPercentScroll = scrollPercent;
  }

  void _onPanUpdate(DragUpdateDetails details) {
    final currDrag = details.globalPosition;
    final dragDistance = (currDrag.dx - startDrag.dx) * _scrollSpeedMultiplier;
    final singleCardDragPercent = dragDistance / fullWidth;

    setState(() {
      scrollPercent = (startDragPercentScroll +
              (-singleCardDragPercent / widget.movies.length))
          .clamp(0.0, 1.0 - (1 / widget.movies.length));
      _currentIndex = (scrollPercent * widget.movies.length).round();
//      if (widget.onScroll != null) {
//        widget.onScroll(scrollPercent);
//      }
    });
  }

  void _onPanEnd(DragEndDetails details) {
    finishScrollStart = scrollPercent;
    finishScrollEnd =
        (scrollPercent * widget.movies.length).round() / widget.movies.length;
    finishScrollController.forward(from: 0.0);

    setState(() {
      startDrag = null;
      startDragPercentScroll = null;
    });
  }
}
