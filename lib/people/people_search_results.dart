import 'dart:math';

import 'package:filmographer/connection_error.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:filmographer/tmdb/discover_people_result.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/colors.dart';
import 'package:filmographer/page_selector.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/people/person_result_card.dart';

class PeopleSearchResults extends StatefulWidget {
  final String query;

  const PeopleSearchResults({Key key, this.query}) : super(key: key);

  @override
  _PeopleSearchResultsState createState() => _PeopleSearchResultsState();
}

class _PeopleSearchResultsState extends State<PeopleSearchResults> {
  DiscoverPeopleResults _results;
  int _selectedPage = 1;
  bool _isError = false;
  final int _resultsPerPage = 20;
  final String _noResultsPicPath = "images/search.png";
  final String _loadingPicPath = "images/loading.gif";

  @override
  Widget build(BuildContext context) {
    if (!_isError && _results == null) _loadResults();

    return Scaffold(
      appBar: AppBar(
          title: Text(
            _getAppBarTitle(),
          ),
          centerTitle: true),
      body: _buildScaffoldBody(),
    );
  }

  Widget _buildScaffoldBody() {
    if (_isError) return _buildErrorUI();

    if (_results == null) return _buildLoadingUI();

    if (_results.results.isEmpty) return _buildEmptyResults();

    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([_buildPageNavigation()]),
        ),
        SliverList(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
          return PersonResultCard(person: _results.results[index]);
        }, childCount: _results.results.length)),
        SliverList(
          delegate: SliverChildListDelegate([_buildPageNavigation()]),
        )
      ],
    );
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset(_loadingPicPath));
  }

  Widget _buildErrorUI() {
    return ConnectionError(onRetry: () {
      setState(() {
        _isError = false;
      });
    });
  }

  Widget _buildEmptyResults() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(_noResultsPicPath),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.nothingFound,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPageNavigation() {
    if (_results.totalPages < 2) return SizedBox();

    return Card(
        margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
        elevation: 2,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _selectedPage == 1
                    ? SizedBox()
                    : GestureDetector(
                        child: Container(
                          width: 48,
                          height: 48,
                          child: Icon(FontAwesomeIcons.angleLeft,
                              color: AppColors.primaryDark, size: 28),
                        ),
                        onTap: () {
                          setState(() {
                            _selectedPage = max(_selectedPage - 1, 1);
                            _results = null;
                          });
                        },
                        onDoubleTap: () {
                          setState(() {
                            _selectedPage = 1;
                            _results = null;
                          });
                        },
                      ),
                Expanded(
                  child: Center(
                    child: PageSelector(
                      currentPage: _selectedPage,
                      totalPages: _results.totalPages,
                      onPageSelect: (index) => _onPageNumberClick(index),
                    ),
                  ),
                ),
                _selectedPage == _results.totalPages
                    ? SizedBox()
                    : GestureDetector(
                        child: Container(
                          width: 48,
                          height: 48,
                          child: Icon(FontAwesomeIcons.angleRight,
                              color: AppColors.primaryDark, size: 28),
                        ),
                        onTap: () {
                          setState(() {
                            _selectedPage =
                                min(_selectedPage + 1, _results.totalPages);
                            _results = null;
                          });
                        },
                        onDoubleTap: () {
                          setState(() {
                            _selectedPage = _results.totalPages;
                            _results = null;
                          });
                        },
                      )
              ],
            )));
  }

  void _loadResults() {
    Utils.getDiscoverPeopleResults(
            Utils.buildSearchPeopleUrl(widget.query, _selectedPage))
        .then((results) {
      setState(() {
        _results = results;
      });
    }).catchError((e) => _onError(e));
  }

  String _getAppBarTitle() {
    if (_isError) return Strings.errorMessage;

    if (_results == null) return Strings.loading;

    if (_results.results.isEmpty) return Strings.searchResults;

    int totalResults = _results.totalResults;
    return "${1 + (_selectedPage - 1) * _resultsPerPage}-${min(_selectedPage * _resultsPerPage, totalResults)} из $totalResults";
  }

  void _onPageNumberClick(int page) {
    if (_selectedPage != page) {
      setState(() {
        _selectedPage = page;
        _results = null;
      });
    }
  }

  void _onError(Exception e) {
    setState(() {
      _isError = true;
    });
  }
}
