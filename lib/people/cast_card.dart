import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/tmdb/tmdb_person.dart';

//Карточка с фото и именем человека
class CastCard extends StatelessWidget {
  CastCard({Key key, @required this.person}) : super(key: key);

  final TmdbPerson person;

  final double _cardWidth = 128;
  final double _aspectRatio = 2.0 / 3.0;
  final String _malePlaceholderPath = "images/male_placeholder.png";
  final String _femalePlaceholderPath = "images/female_placeholder.png";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: Utils.screenAwareWidth(12.0, context)),
      width: Utils.screenAwareWidth(_cardWidth, context),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: _buildShadow(),
            child: _buildCastImage(context),
          ),
          _buildName()
        ],
      ),
    );
  }

  Widget _buildCastImage(BuildContext context) {
    if (person.profilePath == null) return _buildBlankProfilePic(context);

    double width = Utils.screenAwareWidth(_cardWidth, context);

    return ClipRRect(
        borderRadius:
            BorderRadius.circular(Utils.screenAwareWidth(4.0, context)),
        child: CachedNetworkImage(
            width: width,
            height: width / _aspectRatio,
            fit: BoxFit.cover,
            placeholder: (context, string) => _buildPlaceholder(context),
            errorWidget: (context, url, error) => _buildPlaceholder(context),
            imageUrl: StringUtils.getSmallPicUrl(person.profilePath)));
  }

  Widget _buildBlankProfilePic(BuildContext context) {
    return ClipRRect(
        borderRadius:
            BorderRadius.circular(Utils.screenAwareWidth(4.0, context)),
        child: _buildPlaceholder(context));
  }

  Widget _buildPlaceholder(BuildContext context) {
    double width = Utils.screenAwareWidth(_cardWidth, context);

    return ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          width: width,
          height: width / _aspectRatio,
          child: Image.asset(_getPlaceHolder()),
        ));
  }

  Widget _buildName() {
    return Padding(
      padding: const EdgeInsets.only(top: 4, left: 4, right: 4),
      child: Text(person.name,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center),
    );
  }

  Decoration _buildShadow() {
    return BoxDecoration(borderRadius: BorderRadius.circular(4), boxShadow: [
      BoxShadow(color: Colors.grey, blurRadius: 2, offset: Offset(3, 3))
    ]);
  }

  String _getPlaceHolder() {
    return person.gender == 1 ? _femalePlaceholderPath : _malePlaceholderPath;
  }
}
