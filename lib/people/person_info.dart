import 'dart:math';

import 'package:filmographer/backdrop_placeholder.dart';
import 'package:filmographer/backrdops_tab.dart';
import 'package:filmographer/connection_error.dart';
import 'package:filmographer/model/people_model.dart';
import 'package:filmographer/overview.dart';
import 'package:filmographer/photo_grid.dart';
import 'package:filmographer/poster_list.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/strings.dart';
import 'package:filmographer/title_box.dart';
import 'package:filmographer/tmdb/backdrop.dart';
import 'package:filmographer/tmdb/tmdb_person.dart';
import 'package:filmographer/tmdb/cast_details.dart';
import 'package:filmographer/tmdb/movie.dart';
import 'package:filmographer/tmdb/person_summary.dart';
import 'package:filmographer/tmdb/tv_show.dart';
import 'package:filmographer/utils.dart';
import 'package:filmographer/external_links.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

//Страница с детальными сведениями о человеке (фото, имя, биография,
//фильмы и сериалы с участием)
class PersonInfo extends StatefulWidget {
  PersonInfo({Key key, @required this.person}) : super(key: key);

  final TmdbPerson person;

  @override
  _PersonInfoState createState() => _PersonInfoState();
}

class _PersonInfoState extends State<PersonInfo> {
  final int _maxBackdrops = 8;
  final double _photoRowHeight = 175.0;
  CastDetails _details;
  PersonSummary _summary;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(widget.person.name), centerTitle: true),
        body: FutureBuilder<CastDetails>(
          future: Utils.getCastDetails(
              Utils.buildPersonDetailsUrl(widget.person.id)),
          builder: (context, snapshot) {
            if (snapshot.hasError) return _buildErrorUI();

            if (snapshot.hasData) {
              _details = snapshot.data;
              _summary = _details.getSummary();
              return _buildScaffoldBody();
            } else {
              return _buildLoadingUI();
            }
          },
        ));
  }

  Widget _buildErrorUI() {
    return ConnectionError(onRetry: () {
      setState(() {});
    });
  }

  Widget _buildLoadingUI() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[BackdropPlaceholder(), _buildTitle()],
    );
  }

  Widget _buildScaffoldBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTopStack(),
          _buildBiography(),
          _buildKnownForMovies(),
          _buildKnownForTvShows(),
          _buildCrewInfo("Directing", Strings.director),
          _buildCrewInfo("Writing", Strings.writer),
          _buildCrewInfo("Production", Strings.producer),
          _buildImagesGrid(),
          _buildLinks()
        ],
      ),
    );
  }

  Widget _buildTopStack() {
    return Stack(
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[_buildBackdropTabs(), _buildTitle()],
        ),
        _buildLikeButton()
      ],
    );
  }

  Widget _buildBackdropTabs() {
    _swapBackdrops();
    List<Backdrop> backdrops = _details.images.profiles
        .take(min(_maxBackdrops, _details.images.profiles.length))
        .toList();

    return BackdropsTab(
        backdropsList: backdrops, isVideo: false, gender: _details.gender);
  }

  Widget _buildLikeButton() {
    return Positioned(
        top: MediaQuery.of(context).size.width / 0.66 - 32,
        right: 16,
        child: ScopedModelDescendant<PeopleModel>(
            builder: (context, child, model) {
          return FloatingActionButton(
              backgroundColor: Colors.white,
              heroTag: null,
              tooltip: Strings.addFavourites,
              child: model.contains(_details.id)
                  ? Icon(
                      Icons.favorite,
                      color: Colors.red,
                    )
                  : Icon(
                      Icons.favorite_border,
                      color: Colors.grey,
                    ),
              onPressed: () => _onLikeButtonPress(model));
        }));
  }

  Widget _buildTitle() {
    return TitleBox(title: widget.person.name, subtitle: _parseAge());
  }

  Widget _buildBiography() {
    return Overview(title: Strings.biography, overview: _details.biography);
  }

  Widget _buildKnownForMovies() {
    if (_details.adult) return SizedBox();

    List<Movie> movies = <Movie>[];
    _details.combinedCredits.cast.forEach((c) {
      if (c.isMovie()) movies.add(c.getMovie());
    });

    //убрать фильмы без постеров и отсортировать по убыванию количества оценок
    movies.removeWhere((m) => m.posterPath == null || m.posterPath.isEmpty);
    movies.sort((m1, m2) => m2.voteCount.compareTo(m1.voteCount));
    List<Movie> result = movies.toSet().toList();
    _summary.totalMovies = result.length;

    return PosterList(
        title: StringUtils.getKnownForMoviesTitle(_details.gender),
        videos: result);
  }

  Widget _buildKnownForTvShows() {
    if (_details.adult) return SizedBox();

    List<TvShow> tvShows = <TvShow>[];
    _details.combinedCredits.cast.forEach((c) {
      if (!c.isMovie()) tvShows.add(c.getTvShow());
    });

    //убрать сериалы без постеров и отсортировать по убыванию количества оценок
    tvShows.removeWhere((m) => m.posterPath == null || m.posterPath.isEmpty);
    tvShows.sort((m1, m2) => m2.voteCount.compareTo(m1.voteCount));
    List<TvShow> result = tvShows.toSet().toList();
    _summary.totalTvShows = result.length;

    return PosterList(
      title: StringUtils.getKnownForTvShowsTitle(_details.gender),
      videos: result,
    );
  }

  Widget _buildCrewInfo(String department, String title) {
    List<Movie> movies = <Movie>[];
    _details.combinedCredits.crew.forEach((c) {
      if (c.department == department && c.isMovie()) movies.add(c.getMovie());
    });

    //убрать фильмы без постеров и отсортировать по убыванию количества оценок
    movies.removeWhere((m) => m.posterPath == null || m.posterPath.isEmpty);
    movies.sort((m1, m2) => m2.voteCount.compareTo(m1.voteCount));
    List<Movie> result = movies.toSet().toList();

    return PosterList(title: title, videos: result);
  }

  Widget _buildImagesGrid() {
    if (_details.images.profiles.length > _maxBackdrops) {
      //кол-во рядов в сетке от 1 до 3
      int rows = min(3, max(1, (_details.images.profiles.length / 10).round()));
      return PhotoGrid(
          photos: _details.images.profiles,
          title: Strings.photo,
          name: _details.name,
          aspectRatio: 1.5,
          gridHeight: rows * _photoRowHeight,
          rows: rows);
    } else
      return SizedBox();
  }

  Widget _buildLinks() {
    return ExternalLinks(
        ids: _details.externalIds, title: _details.name, isVideo: false);
  }

  void _swapBackdrops() {
    if (_details.images.profiles.length > 1) {
      //картинка из профиля должна быть первой в списке всех картинок
      int profileBackdropIndex = _details.images.profiles.indexWhere(
          (backdrop) => backdrop.filePath == (widget.person.profilePath));
      if (profileBackdropIndex != 0) {
        Backdrop temp = _details.images.profiles[profileBackdropIndex];
        _details.images.profiles[profileBackdropIndex] =
            _details.images.profiles[0];
        _details.images.profiles[0] = temp;
      }
    }
  }

  void _onLikeButtonPress(PeopleModel model) {
    if (!model.people.contains(_summary))
      model.add(_summary);
    else
      model.remove(_summary);
  }

  String _parseAge() {
    if (_details == null) return "";

    return StringUtils.parseAge(_details.birthday, _details.deathday);
  }
}
