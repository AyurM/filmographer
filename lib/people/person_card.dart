import 'package:filmographer/strings.dart';
import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/people_model.dart';
import 'package:filmographer/tmdb/person_summary.dart';
import 'package:filmographer/people/person_info.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/colors.dart';

//Карточка для отображения кратких сведений о человеке
//в списке избранного
class PersonCard extends StatelessWidget {
  final PersonSummary summary;

  final double _profilePicAspect = 2.0 / 3.0;
  final double _width = 100.0;
  final String _malePlaceholderPath = "images/male_placeholder.png";
  final String _femalePlaceholderPath = "images/female_placeholder.png";

  const PersonCard({Key key, this.summary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PeopleModel>(
      builder: (context, child, model) {
        return Dismissible(
          direction: DismissDirection.startToEnd,
          key: Key(summary.id.toString()),
          onDismissed: (direction) => _onDismiss(model, context),
          background: Container(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Utils.screenAwareWidth(8.0, context)),
              child: Icon(Icons.delete,
                  color: Colors.grey,
                  size: Utils.screenAwareWidth(32.0, context)),
            ),
            alignment: Alignment.centerLeft,
          ),
          child: GestureDetector(
            child: _buildPersonCard(context),
            onTap: () => _onClick(context),
          ),
        );
      },
    );
  }

  Widget _buildPersonCard(BuildContext context) {
    return Card(
        margin: EdgeInsets.symmetric(
            vertical: Utils.screenAwareHeight(6.0, context),
            horizontal: Utils.screenAwareWidth(8.0, context)),
        elevation: 2,
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white, Color(0xFFEDEDED)],
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter)),
            child: _buildPersonInfo(context)));
  }

  Widget _buildPersonInfo(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(Utils.screenAwareWidth(4.0, context)),
          child: ClipRRect(
              borderRadius:
                  BorderRadius.circular(Utils.screenAwareWidth(2.0, context)),
              child: CachedNetworkImage(
                  width: Utils.screenAwareWidth(_width, context),
                  height: Utils.screenAwareWidth(_width, context) /
                      _profilePicAspect,
                  fit: BoxFit.fitHeight,
                  placeholder: (context, string) => _buildPlaceholder(context),
                  errorWidget: (context, url, error) =>
                      _buildPlaceholder(context),
                  imageUrl: StringUtils.getSmallPicUrl(summary.profilePath))),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(
                    Utils.screenAwareWidth(8.0, context),
                    Utils.screenAwareHeight(8.0, context),
                    Utils.screenAwareWidth(8.0, context),
                    Utils.screenAwareHeight(2.0, context)),
                child: Text(
                  summary.name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: Utils.screenAwareHeight(20.0, context),
                      fontWeight: FontWeight.bold,
                      color: AppColors.textColor),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: Utils.screenAwareHeight(12.0, context)),
                child: Text(_getKnownForTitle()),
              ),
              _buildStatsRow(context)
            ],
          ),
        )
      ],
    );
  }

  Widget _buildStatsRow(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _buildStat(StringUtils.getMoviesPlural(summary.totalMovies),
            summary.totalMovies, context),
        _buildStat(StringUtils.getTvShowsPlural(summary.totalTvShows),
            summary.totalTvShows, context),
        _buildStat(Strings.photoUI, summary.totalPhotos, context)
      ],
    );
  }

  Widget _buildStat(String statName, int statValue, BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: Utils.screenAwareHeight(8.0, context),
          horizontal: Utils.screenAwareWidth(4.0, context)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(statValue.toString(),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: Utils.screenAwareHeight(20.0, context),
                  color: AppColors.accentColor)),
          Text(statName)
        ],
      ),
    );
  }

  Widget _buildPlaceholder(BuildContext context) {
    double width = Utils.screenAwareWidth(_width, context);

    return ClipRRect(
        borderRadius: BorderRadius.circular(Utils.screenAwareWidth(4.0, context)),
        child: Container(
          width: width,
          height: width / _profilePicAspect,
          child: Image.asset(_getPlaceHolder()),
        ));
  }

  String _getKnownForTitle() {
    if (summary.knownForDepartment == "Acting")
      return summary.gender == 1 ? Strings.actress : Strings.actor;
    else if (summary.knownForDepartment == "Directing")
      return Strings.director;
    else if (summary.knownForDepartment == "Writing")
      return Strings.writer;
    else
      return "";
  }

  String _getPlaceHolder() {
    return summary.gender == 1 ? _femalePlaceholderPath : _malePlaceholderPath;
  }

  void _onClick(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonInfo(
                person:
                    summary.isCast() ? summary.getCast() : summary.getCrew())));
  }

  void _onDismiss(PeopleModel model, BuildContext context) {
    model.remove(summary);
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
          "${summary.name} ${summary.gender == 1 ? Strings.deletedFemale : Strings.deletedMale}"),
      action: SnackBarAction(
          label: Strings.cancelCaps, onPressed: () => model.add(summary)),
    ));
  }
}
