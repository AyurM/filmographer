import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/people_model.dart';
import 'package:filmographer/tmdb/person_result.dart';
import 'package:filmographer/people/person_info.dart';
import 'package:filmographer/string_utils.dart';
import 'package:filmographer/colors.dart';

//Карточка для отображения кратких сведений о человеке
//в результатах поиска
class PersonResultCard extends StatelessWidget {
  final PersonResult person;

  final double _profilePicAspect = 2.0 / 3.0;
  final double _width = 100.0;
  final String _malePlaceholderPath = "images/male_placeholder.png";

  const PersonResultCard({Key key, this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PeopleModel>(
      builder: (context, child, model) {
        return GestureDetector(
          child: _buildPersonCard(),
          onTap: () => _onClick(context),
        );
      },
    );
  }

  Widget _buildPersonCard() {
    return Card(
        margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
        elevation: 2,
        child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white, Color(0xFFEDEDED)],
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter)),
            child: _buildPersonInfo()));
  }

  Widget _buildPersonInfo() {
    String imageUrl = person.profilePath == null
        ? _malePlaceholderPath
        : StringUtils.getSmallPicUrl(person.profilePath);

    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: CachedNetworkImage(
                  width: _width,
                  height: _width / _profilePicAspect,
                  fit: BoxFit.cover,
                  placeholder: (context, string) => _buildPlaceholder(),
                  errorWidget: (context, url, error) => _buildPlaceholder(),
                  imageUrl: imageUrl)),
        ),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 2.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    person.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: AppColors.textColor),
                  ),
                ),
              ),
              _buildKnownFor()
            ],
          ),
        )
      ],
    );
  }

  Widget _buildKnownFor() {
    List<Widget> items = List<Widget>();
    person.knownFor.forEach((item) => {
          items.add(Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
            child: Text("• ${item.isMovie() ? item.title : item.name}",
                textAlign: TextAlign.start,
                maxLines: 2,
                overflow: TextOverflow.ellipsis),
          ))
        });

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: items,
    );
  }

  Widget _buildPlaceholder() {
    return ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          width: _width,
          height: _width / _profilePicAspect,
          child: Image.asset(_getPlaceHolder()),
        ));
  }

  String _getPlaceHolder() {
    return _malePlaceholderPath;
  }

  void _onClick(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonInfo(person: person.getCast())));
  }
}
