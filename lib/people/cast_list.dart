import 'package:flutter/material.dart';
import 'package:filmographer/tmdb/cast.dart';
import 'package:filmographer/tmdb/tmdb_person.dart';
import 'package:filmographer/people/cast_card.dart';
import 'package:filmographer/colors.dart';
import 'package:filmographer/people/person_info.dart';

//Список с фотографиями и именами актеров/режиссеров
class CastList extends StatelessWidget {
  CastList({Key key, @required this.persons, @required this.title})
      : super(key: key);

  final List<TmdbPerson> persons;
  final String title;
  final double _listHeight = 235;

  @override
  Widget build(BuildContext context) {
    if (persons.isEmpty) return SizedBox();

    if (persons is List<Cast>)
      persons
          .sort((a1, a2) => (a1 as Cast).order.compareTo((a2 as Cast).order));

    return Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[_buildTitle(), _buildCastList()],
        ));
  }

  Widget _buildTitle() {
    return Padding(
      padding: EdgeInsets.only(bottom: 4),
      child: Text("$title (${persons.length})",
          style: TextStyle(
              fontSize: 18,
              color: AppColors.textColor,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget _buildCastList() {
    return Container(
        height: _listHeight,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: persons.length,
            itemBuilder: (BuildContext context, int i) {
              return GestureDetector(
                child: CastCard(person: persons[i]),
                onTap: () => _onProfilePicClick(context, i),
              );
            }));
  }

  void _onProfilePicClick(BuildContext context, int itemIndex) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PersonInfo(person: persons[itemIndex])));
  }
}
