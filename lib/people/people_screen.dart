import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:filmographer/model/people_model.dart';
import 'package:filmographer/people/person_card.dart';
import 'package:filmographer/strings.dart';

//Экран "Люди"
class PeopleScreen extends StatefulWidget {
  @override
  _PeopleScreenState createState() => _PeopleScreenState();
}

class _PeopleScreenState extends State<PeopleScreen> {
  @override
  void initState() {
    super.initState();
    ScopedModel.of<PeopleModel>(context).getPeople();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<PeopleModel>(builder: (context, child, model) {
      if (model.isLoading) return _buildLoadingUI();

      return Scaffold(
        appBar: AppBar(
            title: Text("${Strings.people} (${model.people.length})"),
            centerTitle: true),
        body: _buildScaffoldBody(model),
      );
    });
  }

  Widget _buildLoadingUI() {
    return Center(child: Image.asset("images/loading.gif"));
  }

  Widget _buildScaffoldBody(PeopleModel model){
    if(model.people.isEmpty) return _buildNoElementsUI();

    return ListView.builder(
        itemCount: model.people.length,
        itemBuilder: (BuildContext context, int i) {
          return PersonCard(summary: model.people[i]);
        });
  }

  Widget _buildNoElementsUI() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset("images/cinema.png"),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              Strings.noElements,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
