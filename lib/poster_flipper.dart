import 'dart:math';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'string_utils.dart';
import 'package:filmographer/movies/movie_info.dart';
import 'tmdb/movie.dart';
import 'colors.dart';

//Исходный код взят из https://www.youtube.com/watch?v=5KbiU-93-yU

class PosterFlipper extends StatefulWidget {
  final List<Movie> movies;
  final double screenPercent;

  PosterFlipper({Key key, this.movies, this.screenPercent = 0.8})
      : super(key: key);

  @override
  _PosterFlipperState createState() => _PosterFlipperState();
}

class _PosterFlipperState extends State<PosterFlipper> {
  double _currentPage;
  int _index;

  final double _aspectRatio = 2.0 / 3.0;

  @override
  void initState() {
    _currentPage = widget.movies.length - 1.0;
    _index = 1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PageController controller =
        PageController(initialPage: widget.movies.length - 1);
    controller.addListener(() {
      setState(() {
        _currentPage = controller.page;
      });
    });

    return Stack(
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * widget.screenPercent,
              child: CardScrollWidget(
                currentPage: _currentPage,
                movies: widget.movies,
              ),
            ),
            MovieShortInfo(movie: widget.movies[widget.movies.length - _index])
          ],
        ),
        Positioned(
          top: 56,
          right: 8,
          child: FloatingActionButton(
              heroTag: null,
              backgroundColor: Colors.white,
              onPressed: () {},
              child: Icon(Icons.share, color: AppColors.accentColor)),
        ),
        Positioned(
          top: 128,
          right: 8,
          child: FloatingActionButton(
              heroTag: null,
              backgroundColor: Colors.white,
              onPressed: () {},
              child: Icon(Icons.delete, color: AppColors.accentColor)),
        ),
        Positioned(
          top: 8,
          right: 16,
          child: Text(
            "$_index / ${widget.movies.length}",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        Positioned.fromRect(
          rect: Rect.fromLTWH(
              0,
              0,
              MediaQuery.of(context).size.width * 0.8,
              MediaQuery.of(context).size.width *
                  widget.screenPercent /
                  (_aspectRatio * 1.2)),
          child: GestureDetector(
            onTap: () => _onPosterClick(context, widget.movies.length - _index),
            child: PageView.builder(
              itemCount: widget.movies.length,
              controller: controller,
              onPageChanged: (page) {
                if (_index != widget.movies.length - page)
                  setState(() {
                    _index = widget.movies.length - page;
                  });
              },
              reverse: true,
              itemBuilder: (context, index) {
                return Container();
              },
            ),
          ),
        )
      ],
    );
  }

  void _onPosterClick(BuildContext context, int itemIndex) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MovieInfo(video: widget.movies[itemIndex])));
  }
}

class CardScrollWidget extends StatelessWidget {
  CardScrollWidget({
    Key key,
    this.currentPage,
    this.movies,
  }) : super(key: key);

  final double currentPage;
  final double padding = 20.0;
  final double verticalInset = 20.0;
  final String _placeholderPath = "images/3.0x/movie_placeholder.png";

  final double aspectRatio = 2.0 / 3.0;
  final List<Movie> movies;

  @override
  Widget build(BuildContext context) {
    double widgetAspectRatio = aspectRatio * 1.2;

    return new AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, constraints) {
        var width = constraints.maxWidth;
        var height = constraints.maxHeight;

        var safeWidth = width - 2 * padding;
        var safeHeight = height - 2 * padding;

        var heightOfPrimaryCard = safeHeight;
        var widthOfPrimaryCard = heightOfPrimaryCard * aspectRatio;

        var primaryCardLeft = safeWidth - widthOfPrimaryCard;
        var horizontalInset = primaryCardLeft / 2;

        List<Widget> cardList = new List();

        for (var i = 0; i < movies.length; i++) {
          var delta = i - currentPage;
          bool isOnRight = delta > 0;

          var start = padding +
              max(
                  primaryCardLeft -
                      horizontalInset * -delta * (isOnRight ? 15 : 1),
                  0.0);

          var cardItem = Positioned.directional(
            top: padding + verticalInset * max(-delta, 0.0),
            bottom: padding + verticalInset * max(-delta, 0.0),
            start: start,
            textDirection: TextDirection.rtl,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      offset: Offset(3.0, 6.0),
                      blurRadius: 10.0)
                ]),
                child: AspectRatio(
                  aspectRatio: aspectRatio,
                  child: CachedNetworkImage(
                      width: widthOfPrimaryCard,
                      fit: BoxFit.cover,
                      placeholder: (context, string) =>
                          _buildPlaceholder(width, height),
                      errorWidget: (context, url, error) =>
                          _buildPlaceholder(width, height),
                      imageUrl:
                          StringUtils.getHighResPicUrl(movies[i].posterPath)),
                ),
              ),
            ),
          );
          cardList.add(cardItem);
        }
        return Stack(
          children: cardList,
        );
      }),
    );
  }

  Widget _buildPlaceholder(double width, double height) {
    return Container(
      width: width,
      height: height,
      child: Image.asset(_placeholderPath),
    );
  }
}

class MovieShortInfo extends StatelessWidget {
  final Movie movie;

  const MovieShortInfo({Key key, this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
          child: Text(
            movie.title,
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: AppColors.textColor),
          ),
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(movie.originalTitle,
                style: TextStyle(fontSize: 14, color: AppColors.textColor))),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
          child: Text(
              "${movie.releaseDate.substring(0, 4)}, ${StringUtils.getGenresNamesFromIds(movie.genreIds, true)}"),
        )
      ],
    );
  }
}
