import 'package:flutter/material.dart';
import 'colors.dart';

class LoadingPlaceholder extends StatelessWidget {
  final String title;

  LoadingPlaceholder({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title,
              style: TextStyle(
                  fontSize: 18,
                  color: AppColors.textColor,
                  fontWeight: FontWeight.bold)),
          Center(
            child: Image.asset("images/loading.gif"),
          )
        ],
      ),
    );
  }
}