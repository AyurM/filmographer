import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'tmdb/movie.dart';
import 'preview_card.dart';
import 'tmdb/tmdb_video.dart';
import 'package:filmographer/movies/movie_info.dart';
import 'package:filmographer/tv_shows/tv_show_info.dart';
import 'colors.dart';

//Список постеров к фильмам/сериалам
class PosterList extends StatelessWidget {
  final String title;
  final List<TmdbVideo> videos;

  final double _listHeight = 265;

  PosterList({Key key, this.title, @required this.videos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (videos.isEmpty) return SizedBox();

    return Padding(
      padding: EdgeInsets.all(Utils.screenAwareHeight(8.0, context)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_buildTitle(context), _buildPosterList(context)],
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text("$title (${videos.length})",
        style: TextStyle(
            fontSize: Utils.screenAwareHeight(18, context),
            color: AppColors.textColor,
            fontWeight: FontWeight.bold));
  }

  Widget _buildPosterList(BuildContext context) {
    return Container(
        height: Utils.screenAwareHeight(_listHeight, context),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: videos.length,
            itemBuilder: (BuildContext context, int i) {
              return GestureDetector(
                  child: PreviewCard(video: videos[i]),
                  onTap: () => _onPopularItemClick(context, i));
            }));
  }

  void _onPopularItemClick(BuildContext context, int itemIndex) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => videos is List<Movie>
                ? MovieInfo(video: videos[itemIndex])
                : TvShowInfo(video: videos[itemIndex])));
  }
}
