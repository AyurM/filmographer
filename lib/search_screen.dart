import 'package:flutter/material.dart';
import 'tmdb/movie.dart';
import 'tmdb/tv_show.dart';
import 'tmdb/tmdb_parameters.dart';
import 'tmdb/person_result.dart';
import 'poster_list.dart';
import 'strings.dart';
import 'package:filmographer/people/cast_list.dart';
import 'package:filmographer/people/people_search_results.dart';
import 'connection_error.dart';
import 'utils.dart';
import 'colors.dart';
import 'search_results.dart';
import 'loading_placeholder.dart';
import 'discover_settings.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final double _categoryAreaHeight = 186;
  final String _movieBackgroundPath = "images/movie_background.png";
  final String _tvBackgroundPath = "images/tv.jpg";
  final String _peopleBackgroundPath = "images/people_background.png";

  int _selectedCategory = 0;
  bool _isError = false;
  List<Movie> _movies;
  List<TvShow> _tvShows;
  List<PersonResult> _people;
  String _searchQuery = "";

  @override
  Widget build(BuildContext context) {
    if (!_isError && _isContentEmpty()) _loadTrending();

    return SingleChildScrollView(
        child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTop(),
        _buildPopularContent(),
        _selectedCategory == 2
            ? SizedBox()
            : DiscoverSettings(selectedCategory: _selectedCategory),
      ],
    ));
  }

  Widget _buildTop() {
    return Container(
        width: double.infinity,
        //TODO: доделать масштабирование высоты под различные разрешения экрана
        height: Utils.screenAwareHeight(_categoryAreaHeight, context),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage(_getBackgroundPath(_selectedCategory)),
                fit: BoxFit.fitWidth)),
        child: Column(
          children: <Widget>[_buildCategorySelector(), _buildSearchArea()],
        ));
  }

  Widget _buildCategorySelector() {
    return Align(
        alignment: Alignment.topCenter,
        child: Padding(
            padding: EdgeInsets.fromLTRB(
                Utils.screenAwareWidth(8, context),
                Utils.screenAwareHeight(32, context),
                Utils.screenAwareWidth(8, context),
                0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                      Utils.screenAwareHeight(40.0, context)),
                  border: Border.all(
                      width: Utils.screenAwareWidth(1.5, context),
                      color: Colors.grey)),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(
                    Utils.screenAwareHeight(40.0, context)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: _buildCategoryButton(Strings.movies, 0)),
                    Expanded(child: _buildCategoryButton(Strings.tvShows, 1)),
                    Expanded(child: _buildCategoryButton(Strings.people, 2))
                  ],
                ),
              ),
            )));
  }

  Widget _buildCategoryButton(String buttonText, int category) {
    Border border;
    switch (category) {
      case 0:
        border = Border(
            right: BorderSide(
                width: Utils.screenAwareWidth(1.0, context),
                color: Colors.grey));
        break;
      case 1:
        border = Border(
            right: BorderSide(
                width: Utils.screenAwareWidth(1.0, context),
                color: Colors.grey));
        break;
      default:
        break;
    }
    return FlatButton(
        shape: border,
        padding: EdgeInsets.symmetric(
            horizontal: Utils.screenAwareWidth(14.0, context),
            vertical: Utils.screenAwareHeight(16.0, context)),
        child: Text(buttonText,
            style: TextStyle(
                fontSize: Utils.screenAwareHeight(16, context),
                fontWeight: FontWeight.bold)),
        textColor: _selectedCategory == category ? Colors.white : Colors.grey,
        color: _selectedCategory == category
            ? AppColors.primaryColor
            : Colors.white,
        onPressed: () {
          if (_selectedCategory == category) return;
          setState(() {
            _isError = false;
            _selectedCategory = category;
          });
        });
  }

  Widget _buildSearchArea() {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: Utils.screenAwareHeight(16.0, context),
          horizontal: Utils.screenAwareWidth(24.0, context)),
      child: Center(
        child: TextField(
          onChanged: (value) => _searchQuery = value,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              suffix: IconButton(
                  color: AppColors.accentColor,
                  icon: Icon(Icons.search),
                  onPressed: () =>
                      _onSearchIconClick(_searchQuery, _selectedCategory)),
              contentPadding: EdgeInsets.fromLTRB(
                  Utils.screenAwareWidth(24.0, context),
                  Utils.screenAwareHeight(8.0, context),
                  Utils.screenAwareWidth(8.0, context),
                  Utils.screenAwareHeight(8.0, context)),
              alignLabelWithHint: false,
              labelText: _getSearchHint(_selectedCategory),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(
                      Utils.screenAwareHeight(32.0, context))))),
        ),
      ),
    );
  }

  Widget _buildPopularContent() {
    if (_isError) return _buildErrorUI();

    if (_isContentEmpty()) return LoadingPlaceholder(title: Strings.popular);

    if (_selectedCategory == 2)
      return CastList(
          persons: _people.map((p) => p.getCast()).toList(),
          title: Strings.popular);
    else
      return PosterList(
        title: Strings.popular,
        videos: _selectedCategory == 0 ? _movies : _tvShows,
      );
  }

  Widget _buildErrorUI() {
    return ConnectionError(onRetry: () {
      setState(() {
        _isError = false;
      });
    });
  }

  bool _isContentEmpty() {
    return (_selectedCategory == 0 && _movies == null) ||
        (_selectedCategory == 1 && _tvShows == null) ||
        (_selectedCategory == 2 && _people == null);
  }

  String _getBackgroundPath(int category) {
    switch (category) {
      case 0:
        return _movieBackgroundPath;
        break;
      case 1:
        return _tvBackgroundPath;
        break;
      default:
        return _peopleBackgroundPath;
    }
  }

  String _getSearchHint(int category) {
    switch (category) {
      case 0:
        return Strings.searchMovies;
        break;
      case 1:
        return Strings.searchTvShows;
        break;
      default:
        return Strings.searchPeople;
    }
  }

  void _loadTrending() {
    if (_selectedCategory == 0)
      _loadTrendingMovies();
    else if (_selectedCategory == 1)
      _loadTrendingTvShows();
    else
      _loadTrendingPeople();
  }

  void _loadTrendingMovies() {
    Utils.getDiscoverResults(Utils.buildTrendingUrl("movie", "day"))
        .then((results) {
      setState(() {
        _movies = results.results;
        //отсеять непопулярные или невышедшие результаты
        _movies.removeWhere((movie) =>
            movie.voteCount < int.parse(TmdbParams.defaultMovieVoteCountGte));
      });
    }).catchError((e) => _onError(e));
  }

  void _loadTrendingTvShows() {
    Utils.getDiscoverTvResults(Utils.buildTrendingUrl("tv", "day"))
        .then((results) {
      setState(() {
        _tvShows = results.results;
        //отсеять непопулярные или невышедшие результаты
        _tvShows.removeWhere(
            (tv) => tv.voteCount < int.parse(TmdbParams.defaultTvVoteCountGte));
      });
    }).catchError((e) => _onError(e));
  }

  void _loadTrendingPeople() {
    Utils.getDiscoverPeopleResults(Utils.buildPopularPersonUrl())
        .then((results) {
      setState(() {
        _people = results.results;
        _people.removeWhere((p) => p.adult);
      });
    }).catchError((e) => _onError(e));
  }

  void _onSearchIconClick(String query, int category) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => category == 2
                ? PeopleSearchResults(query: query)
                : SearchResults(
                    minRating: TmdbParams.defaultVoteAverage,
                    isMovie: category == 0,
                    searchQuery: query)));
  }

  void _onError(Exception e) {
    setState(() {
      _isError = true;
    });
  }
}
