import 'package:flutter/material.dart';

class AppColors{
  static const MaterialColor primaryColor = Colors.teal;
  static const MaterialAccentColor accentColor = Colors.indigoAccent;
  static const Color primaryDark = Color(0xFF00675b);
  static const Color transparentColor = Color.fromARGB(180, 0, 150, 136);
  static const Color textColor = Color(0xD2000000);
  static const Color transparentBlack = Color(0x50000000);
}