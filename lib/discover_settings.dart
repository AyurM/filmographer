import 'package:filmographer/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:shared_preferences/shared_preferences.dart';
import 'tmdb/tmdb_parameters.dart';
import 'search_results.dart';
import 'strings.dart';
import 'colors.dart';

class DiscoverSettings extends StatefulWidget {
  DiscoverSettings({Key key, this.selectedCategory}) : super(key: key);

  final int selectedCategory;
  @override
  _DiscoverSettingsState createState() => _DiscoverSettingsState();
}

class _DiscoverSettingsState extends State<DiscoverSettings> {
  double _fromYear = 1970;
  double _toYear = DateTime.now().year.floorToDouble();
  int _selectedGenre = 0;

  SharedPreferences _prefs;
  final double _minYear = 1970;
  final double _maxYear = DateTime.now().year.floorToDouble();
  final String _fromYearTvKey = "fromYearTv";
  final String _fromYearMovieKey = "fromYearMovie";
  final String _toYearTvKey = "toYearTv";
  final String _toYearMovieKey = "toYearMovie";
  final String _genreTvKey = "genreTv";
  final String _genreMovieKey = "genreMovie";

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance()
      ..then((prefs) {
        setState(() => this._prefs = prefs);
        _loadGenrePref();
        _loadFromYearPref();
        _loadToYearPref();
      });
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    //виджет был пересоздан, т.е. сменилась категория Фильм/Сериал
    _selectedGenre = 0;
    _loadGenrePref();
    _loadFromYearPref();
    _loadToYearPref();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<String>> genreMenuItems;

    genreMenuItems = _getGenres()
        .map((String value) => DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            ))
        .toList();

    final genreTile = ListTile(
      title: const Text(Strings.genre),
      trailing: DropdownButton(
          value: _getGenres()[_selectedGenre],
          items: genreMenuItems,
          onChanged: (String newValue) {
            _setGenrePref(_getGenres().indexOf(newValue));
          }),
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(Utils.screenAwareHeight(8.0, context)),
          child: Text(
              widget.selectedCategory == 0
                  ? Strings.discoverMovies
                  : Strings.discoverTvShows,
              style: TextStyle(
                  fontSize: Utils.screenAwareHeight(18, context),
                  fontWeight: FontWeight.bold)),
        ),
        Padding(
            padding: EdgeInsets.all(Utils.screenAwareHeight(8.0, context)),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(_fromYear.floor().toString()),
                Expanded(
                    child: frs.RangeSlider(
                  min: _minYear,
                  max: _maxYear,
                  lowerValue: _fromYear,
                  upperValue: _toYear,
                  showValueIndicator: true,
                  divisions: _maxYear.floor() - _minYear.floor(),
                  valueIndicatorMaxDecimals: 0,
                  onChanged: (double newLowerValue, double newUpperValue) {
                    if (newLowerValue != _fromYear) {
                      _setFromYearPref(newLowerValue.floor());
                    }
                    if (newUpperValue != _toYear) {
                      _setToYearPref(newUpperValue.floor());
                    }
                  },
                )),
                Text(_toYear.floor().toString())
              ],
            )),
        genreTile,
        Center(
            child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: Utils.screenAwareHeight(16.0, context)),
                child: RaisedButton(
                    padding: EdgeInsets.symmetric(
                        horizontal: Utils.screenAwareWidth(24.0, context),
                        vertical: Utils.screenAwareHeight(16.0, context)),
                    child: Text(
                      widget.selectedCategory == 0
                          ? Strings.findMovies
                          : Strings.findTvShows,
                      style: TextStyle(
                          fontSize: Utils.screenAwareHeight(18, context),
                          fontWeight: FontWeight.bold),
                    ),
                    textColor: Colors.white,
                    shape: const StadiumBorder(),
                    color: AppColors.accentColor,
                    onPressed: () => _onFindMoviesButtonClick())))
      ],
    );
  }

  void _onFindMoviesButtonClick() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchResults(
                fromYear: _fromYear.toInt(),
                toYear: _toYear.toInt(),
                genre: _getGenres()[_selectedGenre],
                minRating: TmdbParams.defaultVoteAverage,
                isMovie: widget.selectedCategory == 0)));
  }

  Future<Null> _setGenrePref(int val) async {
    await this._prefs.setInt(
        widget.selectedCategory == 0 ? _genreMovieKey : _genreTvKey, val);
    _loadGenrePref();
  }

  Future<Null> _setFromYearPref(int val) async {
    await this._prefs.setInt(
        widget.selectedCategory == 0 ? _fromYearMovieKey : _fromYearTvKey, val);
    _loadFromYearPref();
  }

  Future<Null> _setToYearPref(int val) async {
    await this._prefs.setInt(
        widget.selectedCategory == 0 ? _toYearMovieKey : _toYearTvKey, val);
    _loadToYearPref();
  }

  void _loadGenrePref() {
    setState(() {
      if (_prefs == null)
        _selectedGenre = 0;
      else
        _selectedGenre = _prefs.getInt(
                widget.selectedCategory == 0 ? _genreMovieKey : _genreTvKey) ??
            0;
    });
  }

  void _loadFromYearPref() {
    setState(() {
      if (_prefs == null)
        _fromYear = _minYear;
      else
        _fromYear = _prefs
                .getInt(widget.selectedCategory == 0
                    ? _fromYearMovieKey
                    : _fromYearTvKey)
                ?.floorToDouble() ??
            _minYear;
    });
  }

  void _loadToYearPref() {
    setState(() {
      if (_prefs == null)
        _toYear = _maxYear;
      else
        _toYear = _prefs
                .getInt(widget.selectedCategory == 0
                    ? _toYearMovieKey
                    : _toYearTvKey)
                ?.floorToDouble() ??
            _maxYear;
    });
  }

  List<String> _getGenres() {
    return widget.selectedCategory == 0
        ? TmdbParams.movieGenres
        : TmdbParams.tvShowGenres;
  }
}
